package com.ruoyi.salesmgt.service.impl;

import java.util.List;
import com.ruoyi.common.utils.DateUtils;
import com.ruoyi.salesmgt.domain.PsiSalesContract;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import java.util.ArrayList;
import com.ruoyi.common.utils.StringUtils;
import org.springframework.transaction.annotation.Transactional;
import com.ruoyi.salesmgt.domain.PsiSalesOrderDetail;
import com.ruoyi.salesmgt.mapper.PsiSalesOrderMapper;
import com.ruoyi.salesmgt.domain.PsiSalesOrder;
import com.ruoyi.salesmgt.service.IPsiSalesOrderService;

/**
 * 销售订单Service业务层处理
 * 
 * @author smes
 * @date 2023-06-12
 */
@Service
public class PsiSalesOrderServiceImpl implements IPsiSalesOrderService 
{
    @Autowired
    private PsiSalesOrderMapper psiSalesOrderMapper;

    /**
     * 查询销售订单
     * 
     * @param salesOrderId 销售订单主键
     * @return 销售订单
     */
    @Override
    public PsiSalesOrder selectPsiSalesOrderBySalesOrderId(Long salesOrderId)
    {
        return psiSalesOrderMapper.selectPsiSalesOrderBySalesOrderId(salesOrderId);
    }

    /**
     * 查询销售订单列表
     * 
     * @param psiSalesOrder 销售订单
     * @return 销售订单
     */
    @Override
    public List<PsiSalesOrder> selectPsiSalesOrderList(PsiSalesOrder psiSalesOrder)
    {
        return psiSalesOrderMapper.selectPsiSalesOrderList(psiSalesOrder);
    }

    /**
     * 新增销售订单
     * 
     * @param psiSalesOrder 销售订单
     * @return 结果
     */
    @Transactional
    @Override
    public int insertPsiSalesOrder(PsiSalesOrder psiSalesOrder)
    {
        psiSalesOrder.setCreateTime(DateUtils.getNowDate());
        int rows = psiSalesOrderMapper.insertPsiSalesOrder(psiSalesOrder);
        insertPsiSalesOrderDetail(psiSalesOrder);
        insertPsiSalesContract(psiSalesOrder);
        return rows;
    }

    /**
     * 修改销售订单
     * 
     * @param psiSalesOrder 销售订单
     * @return 结果
     */
    @Transactional
    @Override
    public int updatePsiSalesOrder(PsiSalesOrder psiSalesOrder)
    {
        psiSalesOrder.setUpdateTime(DateUtils.getNowDate());
        psiSalesOrderMapper.deletePsiSalesOrderDetailBySalesOrderId(psiSalesOrder.getSalesOrderId());
        insertPsiSalesOrderDetail(psiSalesOrder);
        psiSalesOrderMapper.deletePsiSalesContractBySalesOrderId(psiSalesOrder.getSalesOrderId());
        insertPsiSalesContract(psiSalesOrder);
        return psiSalesOrderMapper.updatePsiSalesOrder(psiSalesOrder);
    }

    /**
     * 批量删除销售订单
     * 
     * @param salesOrderIds 需要删除的销售订单主键
     * @return 结果
     */
    @Transactional
    @Override
    public int deletePsiSalesOrderBySalesOrderIds(Long[] salesOrderIds)
    {
        psiSalesOrderMapper.deletePsiSalesOrderDetailBySalesOrderIds(salesOrderIds);
        psiSalesOrderMapper.deletePsiSalesContractBySalesOrderIds(salesOrderIds);
        return psiSalesOrderMapper.deletePsiSalesOrderBySalesOrderIds(salesOrderIds);
    }

    /**
     * 删除销售订单信息
     * 
     * @param salesOrderId 销售订单主键
     * @return 结果
     */
    @Transactional
    @Override
    public int deletePsiSalesOrderBySalesOrderId(Long salesOrderId)
    {
        psiSalesOrderMapper.deletePsiSalesOrderDetailBySalesOrderId(salesOrderId);
        psiSalesOrderMapper.deletePsiSalesContractBySalesOrderId(salesOrderId);
        return psiSalesOrderMapper.deletePsiSalesOrderBySalesOrderId(salesOrderId);
    }

    /**
     * 新增销售订单明细信息
     * 
     * @param psiSalesOrder 销售订单对象
     */
    public void insertPsiSalesOrderDetail(PsiSalesOrder psiSalesOrder)
    {
        List<PsiSalesOrderDetail> psiSalesOrderDetailList = psiSalesOrder.getPsiSalesOrderDetailList();
        Long salesOrderId = psiSalesOrder.getSalesOrderId();
        if (StringUtils.isNotNull(psiSalesOrderDetailList))
        {
            List<PsiSalesOrderDetail> list = new ArrayList<PsiSalesOrderDetail>();
            for (PsiSalesOrderDetail psiSalesOrderDetail : psiSalesOrderDetailList)
            {
                psiSalesOrderDetail.setSalesOrderId(salesOrderId);
                list.add(psiSalesOrderDetail);
            }
            if (list.size() > 0)
            {
                psiSalesOrderMapper.batchPsiSalesOrderDetail(list);
            }
        }
    }

    /**
     * 新增销售合同信息
     *
     * @param psiSalesOrder 销售订单对象
     */
    public void insertPsiSalesContract(PsiSalesOrder psiSalesOrder)
    {
        List<PsiSalesContract> psiSalesContractList = psiSalesOrder.getPsiSalesContractList();
        Long salesOrderId = psiSalesOrder.getSalesOrderId();
        if (StringUtils.isNotNull(psiSalesContractList))
        {
            List<PsiSalesContract> list = new ArrayList<PsiSalesContract>();
            for (PsiSalesContract psiSalesContract : psiSalesContractList)
            {
                psiSalesContract.setSalesOrderId(salesOrderId);
                list.add(psiSalesContract);
            }
            if (list.size() > 0)
            {
                psiSalesOrderMapper.batchPsiSalesContract(list);
            }
        }
    }
}
