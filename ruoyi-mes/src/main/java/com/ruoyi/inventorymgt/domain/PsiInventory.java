package com.ruoyi.inventorymgt.domain;

import java.math.BigDecimal;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import com.ruoyi.common.annotation.Excel;
import com.ruoyi.common.core.domain.BaseEntity;

/**
 * 实时库存对象 psi_inventory
 * 
 * @author smes
 * @date 2023-06-20
 */
public class PsiInventory extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /** 库存ID */
    private Long id;

    /** 仓库ID */
    @Excel(name = "仓库ID")
    private Long warehouseId;

    /** 产品ID */
    @Excel(name = "产品ID")
    private Long goodsId;

    /** 产品名称 */
    @Excel(name = "产品名称")
    private String goodsName;

    /** 产品批次 */
    @Excel(name = "产品批次")
    private String goodsBatch;

    /** 产品类型 */
    @Excel(name = "产品类型")
    private String goodsCategory;

    /** 产品规格 */
    @Excel(name = "产品规格")
    private String spec;

    /** 条码 */
    @Excel(name = "条码")
    private String barcode;

    /** 计量单位 */
    @Excel(name = "计量单位")
    private String unit;

    /** 平均单价 */
    @Excel(name = "平均单价")
    private BigDecimal avgUnitPrice;

    /** 库存数量 */
    @Excel(name = "库存数量")
    private Long qty;

    /** 安全库存 */
    @Excel(name = "安全库存")
    private Long ss;

    /** 合计金额 */
    @Excel(name = "合计金额")
    private BigDecimal totalAmount;

    /** 删除标识 */
    private String delFlag;

    /** 创建者ID */
    @Excel(name = "创建者ID")
    private Long createId;

    /** 更新者ID */
    @Excel(name = "更新者ID")
    private Long updateId;

    public void setId(Long id) 
    {
        this.id = id;
    }

    public Long getId() 
    {
        return id;
    }
    public void setWarehouseId(Long warehouseId) 
    {
        this.warehouseId = warehouseId;
    }

    public Long getWarehouseId() 
    {
        return warehouseId;
    }
    public void setGoodsId(Long goodsId) 
    {
        this.goodsId = goodsId;
    }

    public Long getGoodsId() 
    {
        return goodsId;
    }
    public void setGoodsName(String goodsName) 
    {
        this.goodsName = goodsName;
    }

    public String getGoodsName() 
    {
        return goodsName;
    }
    public void setGoodsBatch(String goodsBatch) 
    {
        this.goodsBatch = goodsBatch;
    }

    public String getGoodsBatch() 
    {
        return goodsBatch;
    }
    public void setGoodsCategory(String goodsCategory) 
    {
        this.goodsCategory = goodsCategory;
    }

    public String getGoodsCategory() 
    {
        return goodsCategory;
    }
    public void setSpec(String spec) 
    {
        this.spec = spec;
    }

    public String getSpec() 
    {
        return spec;
    }
    public void setBarcode(String barcode) 
    {
        this.barcode = barcode;
    }

    public String getBarcode() 
    {
        return barcode;
    }
    public void setUnit(String unit) 
    {
        this.unit = unit;
    }

    public String getUnit() 
    {
        return unit;
    }
    public void setAvgUnitPrice(BigDecimal avgUnitPrice) 
    {
        this.avgUnitPrice = avgUnitPrice;
    }

    public BigDecimal getAvgUnitPrice() 
    {
        return avgUnitPrice;
    }
    public void setQty(Long qty) 
    {
        this.qty = qty;
    }

    public Long getQty() 
    {
        return qty;
    }
    public void setSs(Long ss) 
    {
        this.ss = ss;
    }

    public Long getSs() 
    {
        return ss;
    }
    public void setTotalAmount(BigDecimal totalAmount) 
    {
        this.totalAmount = totalAmount;
    }

    public BigDecimal getTotalAmount() 
    {
        return totalAmount;
    }
    public void setDelFlag(String delFlag) 
    {
        this.delFlag = delFlag;
    }

    public String getDelFlag() 
    {
        return delFlag;
    }
    public void setCreateId(Long createId) 
    {
        this.createId = createId;
    }

    public Long getCreateId() 
    {
        return createId;
    }
    public void setUpdateId(Long updateId) 
    {
        this.updateId = updateId;
    }

    public Long getUpdateId() 
    {
        return updateId;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this,ToStringStyle.MULTI_LINE_STYLE)
            .append("id", getId())
            .append("warehouseId", getWarehouseId())
            .append("goodsId", getGoodsId())
            .append("goodsName", getGoodsName())
            .append("goodsBatch", getGoodsBatch())
            .append("goodsCategory", getGoodsCategory())
            .append("spec", getSpec())
            .append("barcode", getBarcode())
            .append("unit", getUnit())
            .append("avgUnitPrice", getAvgUnitPrice())
            .append("qty", getQty())
            .append("ss", getSs())
            .append("totalAmount", getTotalAmount())
            .append("remark", getRemark())
            .append("delFlag", getDelFlag())
            .append("createId", getCreateId())
            .append("createBy", getCreateBy())
            .append("createTime", getCreateTime())
            .append("updateId", getUpdateId())
            .append("updateBy", getUpdateBy())
            .append("updateTime", getUpdateTime())
            .toString();
    }
}
