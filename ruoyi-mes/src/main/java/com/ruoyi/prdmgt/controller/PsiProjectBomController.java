package com.ruoyi.prdmgt.controller;

import java.util.List;
import javax.servlet.http.HttpServletResponse;

import com.ruoyi.basic.domain.PsiBom;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.ruoyi.common.annotation.Log;
import com.ruoyi.common.core.controller.BaseController;
import com.ruoyi.common.core.domain.AjaxResult;
import com.ruoyi.common.enums.BusinessType;
import com.ruoyi.prdmgt.domain.PsiProjectBom;
import com.ruoyi.prdmgt.service.IPsiProjectBomService;
import com.ruoyi.common.utils.poi.ExcelUtil;
import com.ruoyi.common.core.page.TableDataInfo;

/**
 * 项目BOMController
 * 
 * @author smes
 * @date 2023-06-12
 */
@RestController
@RequestMapping("/prdmgt/bom")
public class PsiProjectBomController extends BaseController
{
    @Autowired
    private IPsiProjectBomService psiProjectBomService;

    /**
     * 查询项目BOM列表
     */
    @PreAuthorize("@ss.hasPermi('prdmgt:bom:list')")
    @GetMapping("/list")
    public TableDataInfo list(PsiProjectBom psiProjectBom)
    {
        startPage();
        List<PsiProjectBom> list = psiProjectBomService.selectPsiProjectBomList(psiProjectBom);
        return getDataTable(list);
    }

    /**
     * 导出项目BOM列表
     */
    @PreAuthorize("@ss.hasPermi('prdmgt:bom:export')")
    @Log(title = "项目BOM", businessType = BusinessType.EXPORT)
    @PostMapping("/export")
    public void export(HttpServletResponse response, PsiProjectBom psiProjectBom)
    {
        List<PsiProjectBom> list = psiProjectBomService.selectPsiProjectBomList(psiProjectBom);
        ExcelUtil<PsiProjectBom> util = new ExcelUtil<PsiProjectBom>(PsiProjectBom.class);
        util.exportExcel(response, list, "项目BOM数据");
    }

    /**
     * 获取项目BOM详细信息
     */
    @PreAuthorize("@ss.hasPermi('prdmgt:bom:query')")
    @GetMapping(value = "/{id}")
    public AjaxResult getInfo(@PathVariable("id") Long id)
    {
        return AjaxResult.success(psiProjectBomService.selectPsiProjectBomById(id));
    }

    /**
     * 新增项目BOM
     */
    /*@PreAuthorize("@ss.hasPermi('prdmgt:bom:add')")
    @Log(title = "项目BOM", businessType = BusinessType.INSERT)
    @PostMapping("/add")
    public AjaxResult add(@RequestBody PsiProjectBom psiProjectBom)
    {
        return toAjax(psiProjectBomService.insertPsiProjectBom(psiProjectBom));
    }*/

    /**
     * 修改项目BOM
     */
    @PreAuthorize("@ss.hasPermi('prdmgt:bom:edit')")
    @Log(title = "项目BOM", businessType = BusinessType.UPDATE)
    @PutMapping
    public AjaxResult edit(@RequestBody PsiProjectBom psiProjectBom)
    {
        return toAjax(psiProjectBomService.updatePsiProjectBom(psiProjectBom));
    }

    /**
     * 删除项目BOM
     */
    @PreAuthorize("@ss.hasPermi('prdmgt:bom:remove')")
    @Log(title = "项目BOM", businessType = BusinessType.DELETE)
	@DeleteMapping("/{ids}")
    public AjaxResult remove(@PathVariable Long[] ids)
    {
        return toAjax(psiProjectBomService.deletePsiProjectBomByIds(ids));
    }

    /**
     * 新增项目BOM
     */
    @PreAuthorize("@ss.hasPermi('prdmgt:bom:add')")
    @Log(title = "项目BOM", businessType = BusinessType.INSERT)
    @PostMapping
    public AjaxResult insert(@RequestBody PsiProjectBom psiProjectBom)
    {
        return AjaxResult.success(psiProjectBomService.addPsiProjectBom(psiProjectBom));
    }

    /**
     * 查询项目管理列表
     */
    @PreAuthorize("@ss.hasPermi('prdmgt:bom:list')")
    @GetMapping("/noPgList/{projectId}")
    public AjaxResult noPgList(@PathVariable("projectId") Long projectId)
    {
        PsiProjectBom psiProjectBom = new PsiProjectBom();
        psiProjectBom.setDelFlag("0");
        psiProjectBom.setProjectId(projectId);
        List<PsiProjectBom> list = psiProjectBomService.selectPsiProjectBomList(psiProjectBom);
        return AjaxResult.success(list);
    }
}
