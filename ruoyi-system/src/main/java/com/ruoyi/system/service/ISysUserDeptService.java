package com.ruoyi.system.service;

import java.util.List;
import com.ruoyi.system.domain.SysUserDept;

/**
 * 用户和部门关联Service接口
 * 
 * @author smes
 * @date 2023-06-14
 */
public interface ISysUserDeptService 
{
    /**
     * 查询用户和部门关联
     * 
     * @param userId 用户和部门关联主键
     * @return 用户和部门关联
     */
    public SysUserDept selectSysUserDeptByUserId(Long userId);

    /**
     * 查询用户和部门关联列表
     * 
     * @param sysUserDept 用户和部门关联
     * @return 用户和部门关联集合
     */
    public List<SysUserDept> selectSysUserDeptList(SysUserDept sysUserDept);

    public List<Long> selectSysDeptIdsByUserId(Long userId);

    /**
     * 新增用户和部门关联
     * 
     * @param sysUserDept 用户和部门关联
     * @return 结果
     */
    public int insertSysUserDept(SysUserDept sysUserDept);

    /**
     * 修改用户和部门关联
     * 
     * @param sysUserDept 用户和部门关联
     * @return 结果
     */
    public int updateSysUserDept(SysUserDept sysUserDept);

    /**
     * 批量删除用户和部门关联
     * 
     * @param userIds 需要删除的用户和部门关联主键集合
     * @return 结果
     */
    public int deleteSysUserDeptByUserIds(Long[] userIds);

    /**
     * 删除用户和部门关联信息
     * 
     * @param userId 用户和部门关联主键
     * @return 结果
     */
    public int deleteSysUserDeptByUserId(Long userId);
}
