package com.ruoyi.basic.domain;

import java.math.BigDecimal;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import com.ruoyi.common.annotation.Excel;
import com.ruoyi.common.core.domain.BaseEntity;

import javax.validation.constraints.Size;

/**
 * 产品物料对象 psi_goods
 * 
 * @author smes
 * @date 2023-06-05
 */
public class PsiGoods extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /** PK */
    private Long id;

    /** 产品名称 */
    @Excel(name = "产品名称")
    @Size(min = 0, max = 30, message = "产品名称长度不能超过30个字符")
    private String name;

    /** 产品编码 */
    @Excel(name = "产品编码")
    @Size(min = 0, max = 20, message = "产品编码长度不能超过20个字符")
    private String code;

    /** 助记名 */
    @Excel(name = "助记名")
    @Size(min = 0, max = 10, message = "助记名长度不能超过10个字符")
    private String nikeName="";

    /** 产品类型 */
    @Excel(name = "产品类型")
    private String goodsCategory;

    /** 产品批次 */
    @Excel(name = "产品批次")
    @Size(min = 0, max = 15, message = "产品批次长度不能超过15个字符")
    private String goodsBatch="";

    /** 产品规格 */
    @Excel(name = "产品规格")
    @Size(min = 0, max = 15, message = "产品规格长度不能超过15个字符")
    private String spec="";

    /** 条码 */
    @Excel(name = "条码")
    @Size(min = 0, max = 15, message = "条码长度不能超过15个字符")
    private String barcode="";

    /** 计量单位 */
    @Excel(name = "计量单位")
    @Size(min = 0, max = 15, message = "计量单位长度不能超过15个字符")
    private String unit="";

    /** 预设采购价 */
    @Excel(name = "预设采购价")
    private BigDecimal prePurchasePrice=null;

    /** 定额成本 */
    @Excel(name = "定额成本")
    private BigDecimal ratedCost=null;

    /** 建议售价 */
    @Excel(name = "建议售价")
    private BigDecimal salingPrice=null;

    /** 销项税率 */
    @Excel(name = "销项税率")
    private Long outputTaxRate=null;

    /** 进项税率 */
    @Excel(name = "进项税率")
    private Long inputTaxRate=null;

    /** 产品图片 */
    @Excel(name = "产品图片")
    private String goodsPicurl="";

    /** 默认供应商ID */
    @Excel(name = "默认供应商ID")
    private Long defSupplier=null;

    /** 默认仓库ID */
    @Excel(name = "默认仓库ID")
    private Long defWarehouse=null;

    /** 默认车间ID */
    @Excel(name = "默认车间ID")
    private Long defWorkshop=null;

    /** 状态 Y 启用，N未启用 */
    @Excel(name = "状态 Y 启用，N未启用")
    private String goodsStatus="";

    /** 删除标识0，1 */
    private String delFlag="";

    /** 创建者ID */
    @Excel(name = "创建者ID")
    private Long createId=null;

    /** 更新者ID */
    @Excel(name = "更新者ID")
    private Long updateId=null;

    public void setId(Long id) 
    {
        this.id = id;
    }

    public Long getId() 
    {
        return id;
    }
    public void setName(String name) 
    {
        this.name = name;
    }

    public String getName() 
    {
        return name;
    }
    public void setCode(String code) 
    {
        this.code = code;
    }

    public String getCode() 
    {
        return code;
    }
    public void setNikeName(String nikeName) 
    {
        this.nikeName = nikeName;
    }

    public String getNikeName() 
    {
        return nikeName;
    }
    public void setGoodsCategory(String goodsCategory) 
    {
        this.goodsCategory = goodsCategory;
    }

    public String getGoodsCategory() 
    {
        return goodsCategory;
    }
    public void setGoodsBatch(String goodsBatch) 
    {
        this.goodsBatch = goodsBatch;
    }

    public String getGoodsBatch() 
    {
        return goodsBatch;
    }
    public void setSpec(String spec) 
    {
        this.spec = spec;
    }

    public String getSpec() 
    {
        return spec;
    }
    public void setBarcode(String barcode) 
    {
        this.barcode = barcode;
    }

    public String getBarcode() 
    {
        return barcode;
    }
    public void setUnit(String unit) 
    {
        this.unit = unit;
    }

    public String getUnit() 
    {
        return unit;
    }
    public void setPrePurchasePrice(BigDecimal prePurchasePrice) 
    {
        this.prePurchasePrice = prePurchasePrice;
    }

    public BigDecimal getPrePurchasePrice() 
    {
        return prePurchasePrice;
    }
    public void setRatedCost(BigDecimal ratedCost) 
    {
        this.ratedCost = ratedCost;
    }

    public BigDecimal getRatedCost() 
    {
        return ratedCost;
    }
    public void setSalingPrice(BigDecimal salingPrice) 
    {
        this.salingPrice = salingPrice;
    }

    public BigDecimal getSalingPrice() 
    {
        return salingPrice;
    }
    public void setOutputTaxRate(Long outputTaxRate) 
    {
        this.outputTaxRate = outputTaxRate;
    }

    public Long getOutputTaxRate() 
    {
        return outputTaxRate;
    }
    public void setInputTaxRate(Long inputTaxRate) 
    {
        this.inputTaxRate = inputTaxRate;
    }

    public Long getInputTaxRate() 
    {
        return inputTaxRate;
    }
    public void setGoodsPicurl(String goodsPicurl) 
    {
        this.goodsPicurl = goodsPicurl;
    }

    public String getGoodsPicurl() 
    {
        return goodsPicurl;
    }
    public void setDefSupplier(Long defSupplier) 
    {
        this.defSupplier = defSupplier;
    }

    public Long getDefSupplier() 
    {
        return defSupplier;
    }
    public void setDefWarehouse(Long defWarehouse) 
    {
        this.defWarehouse = defWarehouse;
    }

    public Long getDefWarehouse() 
    {
        return defWarehouse;
    }
    public void setDefWorkshop(Long defWorkshop) 
    {
        this.defWorkshop = defWorkshop;
    }

    public Long getDefWorkshop() 
    {
        return defWorkshop;
    }
    public void setGoodsStatus(String goodsStatus) 
    {
        this.goodsStatus = goodsStatus;
    }

    public String getGoodsStatus() 
    {
        return goodsStatus;
    }
    public void setDelFlag(String delFlag) 
    {
        this.delFlag = delFlag;
    }

    public String getDelFlag() 
    {
        return delFlag;
    }
    public void setCreateId(Long createId) 
    {
        this.createId = createId;
    }

    public Long getCreateId() 
    {
        return createId;
    }
    public void setUpdateId(Long updateId) 
    {
        this.updateId = updateId;
    }

    public Long getUpdateId() 
    {
        return updateId;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this,ToStringStyle.MULTI_LINE_STYLE)
            .append("id", getId())
            .append("name", getName())
            .append("code", getCode())
            .append("nikeName", getNikeName())
            .append("goodsCategory", getGoodsCategory())
            .append("goodsBatch", getGoodsBatch())
            .append("spec", getSpec())
            .append("barcode", getBarcode())
            .append("unit", getUnit())
            .append("prePurchasePrice", getPrePurchasePrice())
            .append("ratedCost", getRatedCost())
            .append("salingPrice", getSalingPrice())
            .append("outputTaxRate", getOutputTaxRate())
            .append("inputTaxRate", getInputTaxRate())
            .append("goodsPicurl", getGoodsPicurl())
            .append("defSupplier", getDefSupplier())
            .append("defWarehouse", getDefWarehouse())
            .append("defWorkshop", getDefWorkshop())
            .append("remark", getRemark())
            .append("goodsStatus", getGoodsStatus())
            .append("delFlag", getDelFlag())
            .append("createId", getCreateId())
            .append("createBy", getCreateBy())
            .append("createTime", getCreateTime())
            .append("updateId", getUpdateId())
            .append("updateBy", getUpdateBy())
            .append("updateTime", getUpdateTime())
            .toString();
    }
}
