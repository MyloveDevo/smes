package com.ruoyi.basic.service.impl;

import java.util.List;

import com.ruoyi.basic.domain.PsiWarehouse;
import com.ruoyi.common.utils.DateUtils;
import com.ruoyi.common.utils.SecurityUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.ruoyi.basic.mapper.PsiSupplierCategoryMapper;
import com.ruoyi.basic.domain.PsiSupplierCategory;
import com.ruoyi.basic.service.IPsiSupplierCategoryService;

/**
 * 供应商分类Service业务层处理
 * 
 * @author smes
 * @date 2023-06-04
 */
@Service
public class PsiSupplierCategoryServiceImpl implements IPsiSupplierCategoryService 
{
    @Autowired
    private PsiSupplierCategoryMapper psiSupplierCategoryMapper;

    /**
     * 查询供应商分类
     * 
     * @param id 供应商分类主键
     * @return 供应商分类
     */
    @Override
    public PsiSupplierCategory selectPsiSupplierCategoryById(Long id)
    {
        return psiSupplierCategoryMapper.selectPsiSupplierCategoryById(id);
    }

    /**
     * 查询供应商分类列表
     * 
     * @param psiSupplierCategory 供应商分类
     * @return 供应商分类
     */
    @Override
    public List<PsiSupplierCategory> selectPsiSupplierCategoryList(PsiSupplierCategory psiSupplierCategory)
    {
        return psiSupplierCategoryMapper.selectPsiSupplierCategoryList(psiSupplierCategory);
    }

    /**
     * 新增供应商分类
     * 
     * @param psiSupplierCategory 供应商分类
     * @return 结果
     */
    @Override
    public int insertPsiSupplierCategory(PsiSupplierCategory psiSupplierCategory)
    {
        psiSupplierCategory.setCreateTime(DateUtils.getNowDate());
        psiSupplierCategory.setCreateBy(SecurityUtils.getUsername());
        psiSupplierCategory.setDelFlag("0");
        return psiSupplierCategoryMapper.insertPsiSupplierCategory(psiSupplierCategory);
    }

    /**
     * 修改供应商分类
     * 
     * @param psiSupplierCategory 供应商分类
     * @return 结果
     */
    @Override
    public int updatePsiSupplierCategory(PsiSupplierCategory psiSupplierCategory)
    {
        psiSupplierCategory.setUpdateTime(DateUtils.getNowDate());
        psiSupplierCategory.setUpdateBy(SecurityUtils.getUsername());
        return psiSupplierCategoryMapper.updatePsiSupplierCategory(psiSupplierCategory);
    }

    /**
     * 批量删除供应商分类
     * 
     * @param ids 需要删除的供应商分类主键
     * @return 结果
     */
    @Override
    public int deletePsiSupplierCategoryByIds(Long[] ids)
    {
        return psiSupplierCategoryMapper.deletePsiSupplierCategoryByIds(ids);
    }

    /**
     * 删除供应商分类信息
     * 
     * @param id 供应商分类主键
     * @return 结果
     */
    @Override
    public int deletePsiSupplierCategoryById(Long id)
    {
        return psiSupplierCategoryMapper.deletePsiSupplierCategoryById(id);
    }

    /**
     * 查重
     */
    @Override
    public boolean isDuplicate(PsiSupplierCategory psiSupplierCategory) {
        List<PsiWarehouse> lst = psiSupplierCategoryMapper.findOneByCondition(psiSupplierCategory);
        return lst.size()>0;
    }
}
