import request from '@/utils/request'

// 查询客户分类列表
export function listCustomerctg(query) {
  return request({
    url: '/basic/customerctg/list',
    method: 'get',
    params: query
  })
}

// 查询客户分类详细
export function getCustomerctg(id) {
  return request({
    url: '/basic/customerctg/' + id,
    method: 'get'
  })
}

// 新增客户分类
export function addCustomerctg(data) {
  return request({
    url: '/basic/customerctg',
    method: 'post',
    data: data
  })
}

// 修改客户分类
export function updateCustomerctg(data) {
  return request({
    url: '/basic/customerctg',
    method: 'put',
    data: data
  })
}

// 删除客户分类
export function delCustomerctg(id) {
  return request({
    url: '/basic/customerctg/' + id,
    method: 'delete'
  })
}

// 客户类型列表DropdownList
export function getCustomerCtgDropdownList(query) {
  return request({
    url: '/basic/customerctg/dropdownList',
    method: 'get',
    params: query
  })
}
