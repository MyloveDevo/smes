package com.ruoyi.purmgt.domain;

import java.util.List;
import java.util.Date;
import com.fasterxml.jackson.annotation.JsonFormat;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import com.ruoyi.common.annotation.Excel;
import com.ruoyi.common.core.domain.BaseEntity;

/**
 * 请购单 对象 psi_purchase_req
 * 
 * @author smes
 * @date 2023-06-19
 */
public class PsiPurchaseReq extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /** PK */
    private Long id;

    /** 单号 */
    @Excel(name = "单号")
    private String code;

    /** 主题 */
    @Excel(name = "主题")
    private String prTitle;

    /** 制单人ID */
    @Excel(name = "制单人ID")
    private Long operator;

    /** 请购日期 */
    @JsonFormat(pattern = "yyyy-MM-dd")
    @Excel(name = "请购日期", width = 30, dateFormat = "yyyy-MM-dd")
    private Date prDate;

    /** 请购人ID */
    @Excel(name = "请购人ID")
    private Long prMan;

    /** 项目ID */
    @Excel(name = "项目ID")
    private Long projectId;

    /** 采购单状态（0，未采购；1，部分采购；2，全采购） */
    @Excel(name = "采购单状态", readConverterExp = "0=，未采购；1，部分采购；2，全采购")
    private String purStatus;

    /** 单据状态 */
    @Excel(name = "单据状态")
    private String docStatus;

    /** 附件 */
    @Excel(name = "附件")
    private String attachmentFile;

    /** 删除标识 */
    private String delFlag;

    /** 创建者ID */
    @Excel(name = "创建者ID")
    private Long createId;

    /** 更新者ID */
    @Excel(name = "更新者ID")
    private Long updateId;

    /** 请购单明细 信息 */
    private List<PsiPurchaseReqDetail> psiPurchaseReqDetailList;

    public void setId(Long id) 
    {
        this.id = id;
    }

    public Long getId() 
    {
        return id;
    }
    public void setCode(String code) 
    {
        this.code = code;
    }

    public String getCode() 
    {
        return code;
    }
    public void setPrTitle(String prTitle) 
    {
        this.prTitle = prTitle;
    }

    public String getPrTitle() 
    {
        return prTitle;
    }
    public void setOperator(Long operator) 
    {
        this.operator = operator;
    }

    public Long getOperator() 
    {
        return operator;
    }
    public void setPrDate(Date prDate) 
    {
        this.prDate = prDate;
    }

    public Date getPrDate() 
    {
        return prDate;
    }
    public void setPrMan(Long prMan) 
    {
        this.prMan = prMan;
    }

    public Long getPrMan() 
    {
        return prMan;
    }
    public void setProjectId(Long projectId) 
    {
        this.projectId = projectId;
    }

    public Long getProjectId() 
    {
        return projectId;
    }
    public void setPurStatus(String purStatus) 
    {
        this.purStatus = purStatus;
    }

    public String getPurStatus() 
    {
        return purStatus;
    }
    public void setDocStatus(String docStatus) 
    {
        this.docStatus = docStatus;
    }

    public String getDocStatus() 
    {
        return docStatus;
    }
    public void setAttachmentFile(String attachmentFile) 
    {
        this.attachmentFile = attachmentFile;
    }

    public String getAttachmentFile() 
    {
        return attachmentFile;
    }
    public void setDelFlag(String delFlag) 
    {
        this.delFlag = delFlag;
    }

    public String getDelFlag() 
    {
        return delFlag;
    }
    public void setCreateId(Long createId) 
    {
        this.createId = createId;
    }

    public Long getCreateId() 
    {
        return createId;
    }
    public void setUpdateId(Long updateId) 
    {
        this.updateId = updateId;
    }

    public Long getUpdateId() 
    {
        return updateId;
    }

    public List<PsiPurchaseReqDetail> getPsiPurchaseReqDetailList()
    {
        return psiPurchaseReqDetailList;
    }

    public void setPsiPurchaseReqDetailList(List<PsiPurchaseReqDetail> psiPurchaseReqDetailList)
    {
        this.psiPurchaseReqDetailList = psiPurchaseReqDetailList;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this,ToStringStyle.MULTI_LINE_STYLE)
            .append("id", getId())
            .append("code", getCode())
            .append("prTitle", getPrTitle())
            .append("operator", getOperator())
            .append("prDate", getPrDate())
            .append("prMan", getPrMan())
            .append("projectId", getProjectId())
            .append("purStatus", getPurStatus())
            .append("docStatus", getDocStatus())
            .append("attachmentFile", getAttachmentFile())
            .append("remark", getRemark())
            .append("delFlag", getDelFlag())
            .append("createId", getCreateId())
            .append("createBy", getCreateBy())
            .append("createTime", getCreateTime())
            .append("updateId", getUpdateId())
            .append("updateBy", getUpdateBy())
            .append("updateTime", getUpdateTime())
            .append("psiPurchaseReqDetailList", getPsiPurchaseReqDetailList())
            .toString();
    }
}
