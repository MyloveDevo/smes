package com.ruoyi.basic.controller;

import java.util.List;
import javax.servlet.http.HttpServletResponse;

import com.ruoyi.common.annotation.Anonymous;
import com.ruoyi.prdmgt.domain.PsiProject;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.ruoyi.common.annotation.Log;
import com.ruoyi.common.core.controller.BaseController;
import com.ruoyi.common.core.domain.AjaxResult;
import com.ruoyi.common.enums.BusinessType;
import com.ruoyi.basic.domain.PsiBom;
import com.ruoyi.basic.service.IPsiBomService;
import com.ruoyi.common.utils.poi.ExcelUtil;
import com.ruoyi.common.core.page.TableDataInfo;

/**
 * BOMController
 *
 * @author smes
 * @date 2023-06-08
 */
@RestController
@RequestMapping("/basic/bom")
public class PsiBomController extends BaseController {
    @Autowired
    private IPsiBomService psiBomService;

    /**
     * 查询BOM根列表
     */
    @PreAuthorize("@ss.hasPermi('basic:bom:list')")
    @GetMapping("/list")
    public TableDataInfo list(PsiBom psiBom) {
        startPage();
        List<PsiBom> list = psiBomService.selectPsiRootBomList(psiBom);//.selectPsiBomList(psiBom);
        return getDataTable(list);
    }

    /**
     * 导出BOM列表
     */
    @PreAuthorize("@ss.hasPermi('basic:bom:export')")
    @Log(title = "BOM", businessType = BusinessType.EXPORT)
    @PostMapping("/export")
    public void export(HttpServletResponse response, PsiBom psiBom) {
        List<PsiBom> list = psiBomService.selectPsiBomList(psiBom);
        ExcelUtil<PsiBom> util = new ExcelUtil<PsiBom>(PsiBom.class);
        util.exportExcel(response, list, "BOM数据");
    }

    /**
     * 获取BOM详细信息
     */
    @PreAuthorize("@ss.hasPermi('basic:bom:query')")
    @GetMapping(value = "/{id}")
    public AjaxResult getInfo(@PathVariable("id") Long id) {
        return AjaxResult.success(psiBomService.selectPsiBomById(id));
    }

    /**
     * 新增BOM
     */
    @PreAuthorize("@ss.hasPermi('basic:bom:add')")
    @Log(title = "BOM", businessType = BusinessType.INSERT)
    @PostMapping("/add")
    public AjaxResult add(@RequestBody PsiBom psiBom) {

        if (psiBomService.isDuplicate(psiBom)== false){
            return toAjax(psiBomService.insertPsiBom(psiBom));
        }else{
            return AjaxResult.error("名称和代码重复。");
        }
    }

    /**
     * 新增BOM
     * 返回 PK
     */
    @PreAuthorize("@ss.hasPermi('basic:bom:add')")
    @Log(title = "BOM", businessType = BusinessType.INSERT)
    @PostMapping("/insert")
    public AjaxResult insert(@RequestBody PsiBom psiBom) {

        if (psiBomService.isDuplicate(psiBom)== false){
            return AjaxResult.success(psiBomService.addPsiBom(psiBom));
        }else{
            return AjaxResult.error("名称和代码重复。");
        }
    }

    /**
     * 修改BOM
     */
    @PreAuthorize("@ss.hasPermi('basic:bom:edit')")
    @Log(title = "BOM", businessType = BusinessType.UPDATE)
    @PutMapping
    public AjaxResult edit(@RequestBody PsiBom psiBom) {

        if (psiBomService.isDuplicate(psiBom)== false){
            return toAjax(psiBomService.updatePsiBom(psiBom));
        }else{
            return AjaxResult.error("名称和代码重复。");
        }
    }

    /**
     * 删除BOM
     */
    @PreAuthorize("@ss.hasPermi('basic:bom:remove')")
    @Log(title = "BOM", businessType = BusinessType.DELETE)
    @DeleteMapping("/{ids}")
    public AjaxResult remove(@PathVariable Long[] ids) {
        return toAjax(psiBomService.deletePsiBomByIds(ids));
    }

    /**
     * 查询BOM所有产品物料列表
     */
    @PreAuthorize("@ss.hasPermi('basic:bom:listAll')")
    @GetMapping("/listAll")
    public TableDataInfo listAll(PsiBom psiBom) {
        startPage();
        List<PsiBom> list = psiBomService.selectPsiBomList(psiBom);
        return getDataTable(list);
    }

    /**
     * 获取root id 下BOM tree 信息
     */
    @PreAuthorize("@ss.hasPermi('basic:bom:query')")
    @GetMapping(value = "/tree/{bomId}")
    public AjaxResult getTreeInfo(@PathVariable("bomId") Long id) {
        return AjaxResult.success(psiBomService.selectPsiBomTreeListById(id));
    }

}
