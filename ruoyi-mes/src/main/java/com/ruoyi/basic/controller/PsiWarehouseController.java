package com.ruoyi.basic.controller;

import java.util.List;
import javax.servlet.http.HttpServletResponse;

import com.ruoyi.basic.domain.PsiSupplierCategory;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.ruoyi.common.annotation.Log;
import com.ruoyi.common.core.controller.BaseController;
import com.ruoyi.common.core.domain.AjaxResult;
import com.ruoyi.common.enums.BusinessType;
import com.ruoyi.basic.domain.PsiWarehouse;
import com.ruoyi.basic.service.IPsiWarehouseService;
import com.ruoyi.common.utils.poi.ExcelUtil;
import com.ruoyi.common.core.page.TableDataInfo;

/**
 * 仓库Controller
 *
 * @author smes
 * @date 2023-06-05
 */
@RestController
@RequestMapping("/basic/warehouse")
public class PsiWarehouseController extends BaseController {
    @Autowired
    private IPsiWarehouseService psiWarehouseService;

    /**
     * 查询仓库列表
     */
    @PreAuthorize("@ss.hasPermi('basic:warehouse:list')")
    @GetMapping("/list")
    public TableDataInfo list(PsiWarehouse psiWarehouse) {
        startPage();
        psiWarehouse.setDelFlag("0");
        List<PsiWarehouse> list = psiWarehouseService.selectPsiWarehouseList(psiWarehouse);
        return getDataTable(list);
    }

    /**
     * 导出仓库列表
     */
    @PreAuthorize("@ss.hasPermi('basic:warehouse:export')")
    @Log(title = "仓库", businessType = BusinessType.EXPORT)
    @PostMapping("/export")
    public void export(HttpServletResponse response, PsiWarehouse psiWarehouse) {
        psiWarehouse.setDelFlag("0");
        List<PsiWarehouse> list = psiWarehouseService.selectPsiWarehouseList(psiWarehouse);
        ExcelUtil<PsiWarehouse> util = new ExcelUtil<PsiWarehouse>(PsiWarehouse.class);
        util.exportExcel(response, list, "仓库数据");
    }

    /**
     * 获取仓库详细信息
     */
    @PreAuthorize("@ss.hasPermi('basic:warehouse:query')")
    @GetMapping(value = "/{id}")
    public AjaxResult getInfo(@PathVariable("id") Long id) {
        return AjaxResult.success(psiWarehouseService.selectPsiWarehouseById(id));
    }

    /**
     * 新增仓库
     */
    @PreAuthorize("@ss.hasPermi('basic:warehouse:add')")
    @Log(title = "仓库", businessType = BusinessType.INSERT)
    @PostMapping
    public AjaxResult add(@Validated @RequestBody PsiWarehouse psiWarehouse) {
        if (psiWarehouseService.isDuplicate(psiWarehouse)== false){
            return toAjax(psiWarehouseService.insertPsiWarehouse(psiWarehouse));
        }else{
            return AjaxResult.error("仓库名和代码重复。");
        }

    }

    /**
     * 修改仓库
     */
    @PreAuthorize("@ss.hasPermi('basic:warehouse:edit')")
    @Log(title = "仓库", businessType = BusinessType.UPDATE)
    @PutMapping
    public AjaxResult edit(@Validated @RequestBody PsiWarehouse psiWarehouse) {
        if (psiWarehouseService.isDuplicate(psiWarehouse)== false){
            return toAjax(psiWarehouseService.updatePsiWarehouse(psiWarehouse));
        }else{
            return AjaxResult.error("仓库名和代码重复。");
        }

    }

    /**
     * 删除仓库
     */
    @PreAuthorize("@ss.hasPermi('basic:warehouse:remove')")
    @Log(title = "仓库", businessType = BusinessType.DELETE)
    @DeleteMapping("/{ids}")
    public AjaxResult remove(@PathVariable Long[] ids) {
        return toAjax(psiWarehouseService.deletePsiWarehouseByIds(ids));
    }

    /**
     * 查询仓库列表
     * 不分页
     */
    @GetMapping("/dropdownList")
    public AjaxResult dropdownList()
    {
        PsiWarehouse entity = new PsiWarehouse();
        entity.setDelFlag("0");
        List<PsiWarehouse> list = psiWarehouseService.selectPsiWarehouseList(entity);
        return AjaxResult.success(list);
    }

}
