import request from '@/utils/request'

// 查询产品物料分类列表
export function listGoodsctg(query) {
  return request({
    url: '/basic/goodsctg/list',
    method: 'get',
    params: query
  })
}

// 查询产品物料分类详细
export function getGoodsctg(id) {
  return request({
    url: '/basic/goodsctg/' + id,
    method: 'get'
  })
}

// 新增产品物料分类
export function addGoodsctg(data) {
  return request({
    url: '/basic/goodsctg',
    method: 'post',
    data: data
  })
}

// 修改产品物料分类
export function updateGoodsctg(data) {
  return request({
    url: '/basic/goodsctg',
    method: 'put',
    data: data
  })
}

// 删除产品物料分类
export function delGoodsctg(id) {
  return request({
    url: '/basic/goodsctg/' + id,
    method: 'delete'
  })
}

// 产品物料分类列表DropdownList
export function goodsCtgListAll(query) {
  return request({
    url: '/basic/goodsctg/dropdownList',
    method: 'get',
    params: query
  })
}
