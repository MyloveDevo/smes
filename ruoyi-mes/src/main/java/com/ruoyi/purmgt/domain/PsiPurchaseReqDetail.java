package com.ruoyi.purmgt.domain;

import java.util.Date;
import com.fasterxml.jackson.annotation.JsonFormat;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import com.ruoyi.common.annotation.Excel;
import com.ruoyi.common.core.domain.BaseEntity;

/**
 * 请购单明细 对象 psi_purchase_req_detail
 * 
 * @author smes
 * @date 2023-06-19
 */
public class PsiPurchaseReqDetail extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /** PK */
    private Long id;

    /** 请购单ID */
    @Excel(name = "请购单ID")
    private Long prId;

    /** 物料ID */
    @Excel(name = "物料ID")
    private Long goodsId;

    /** 批次 */
    @Excel(name = "批次")
    private String goodsBatch;

    /** 规格 */
    @Excel(name = "规格")
    private String spec;

    /** 请购数量 */
    @Excel(name = "请购数量")
    private Long prNum;

    /** 已采购数量 */
    @Excel(name = "已采购数量")
    private Long qpNum;

    /** 需要日期 */
    @JsonFormat(pattern = "yyyy-MM-dd")
    @Excel(name = "需要日期", width = 30, dateFormat = "yyyy-MM-dd")
    private Date requestDate;

    /** 删除标识 */
    private String delFlag;

    /** 创建者ID */
    @Excel(name = "创建者ID")
    private Long createId;

    /** 更新者ID */
    @Excel(name = "更新者ID")
    private Long updateId;

    public void setId(Long id) 
    {
        this.id = id;
    }

    public Long getId() 
    {
        return id;
    }
    public void setPrId(Long prId) 
    {
        this.prId = prId;
    }

    public Long getPrId() 
    {
        return prId;
    }
    public void setGoodsId(Long goodsId) 
    {
        this.goodsId = goodsId;
    }

    public Long getGoodsId() 
    {
        return goodsId;
    }
    public void setGoodsBatch(String goodsBatch) 
    {
        this.goodsBatch = goodsBatch;
    }

    public String getGoodsBatch() 
    {
        return goodsBatch;
    }
    public void setSpec(String spec) 
    {
        this.spec = spec;
    }

    public String getSpec() 
    {
        return spec;
    }
    public void setPrNum(Long prNum) 
    {
        this.prNum = prNum;
    }

    public Long getPrNum() 
    {
        return prNum;
    }
    public void setQpNum(Long qpNum) 
    {
        this.qpNum = qpNum;
    }

    public Long getQpNum() 
    {
        return qpNum;
    }
    public void setRequestDate(Date requestDate) 
    {
        this.requestDate = requestDate;
    }

    public Date getRequestDate() 
    {
        return requestDate;
    }
    public void setDelFlag(String delFlag) 
    {
        this.delFlag = delFlag;
    }

    public String getDelFlag() 
    {
        return delFlag;
    }
    public void setCreateId(Long createId) 
    {
        this.createId = createId;
    }

    public Long getCreateId() 
    {
        return createId;
    }
    public void setUpdateId(Long updateId) 
    {
        this.updateId = updateId;
    }

    public Long getUpdateId() 
    {
        return updateId;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this,ToStringStyle.MULTI_LINE_STYLE)
            .append("id", getId())
            .append("prId", getPrId())
            .append("goodsId", getGoodsId())
            .append("goodsBatch", getGoodsBatch())
            .append("spec", getSpec())
            .append("prNum", getPrNum())
            .append("qpNum", getQpNum())
            .append("requestDate", getRequestDate())
            .append("remark", getRemark())
            .append("delFlag", getDelFlag())
            .append("createId", getCreateId())
            .append("createBy", getCreateBy())
            .append("createTime", getCreateTime())
            .append("updateId", getUpdateId())
            .append("updateBy", getUpdateBy())
            .append("updateTime", getUpdateTime())
            .toString();
    }
}
