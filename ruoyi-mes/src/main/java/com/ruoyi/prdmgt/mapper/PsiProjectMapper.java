package com.ruoyi.prdmgt.mapper;

import java.util.List;
import com.ruoyi.prdmgt.domain.PsiProject;

/**
 * 项目管理Mapper接口
 * 
 * @author smes
 * @date 2023-06-11
 */
public interface PsiProjectMapper 
{
    /**
     * 查询项目管理
     * 
     * @param id 项目管理主键
     * @return 项目管理
     */
    public PsiProject selectPsiProjectById(String id);

    /**
     * 查询项目管理列表
     * 
     * @param psiProject 项目管理
     * @return 项目管理集合
     */
    public List<PsiProject> selectPsiProjectList(PsiProject psiProject);

    /**
     * 新增项目管理
     * 
     * @param psiProject 项目管理
     * @return 结果
     */
    public int insertPsiProject(PsiProject psiProject);

    /**
     * 修改项目管理
     * 
     * @param psiProject 项目管理
     * @return 结果
     */
    public int updatePsiProject(PsiProject psiProject);

    /**
     * 删除项目管理
     * 
     * @param id 项目管理主键
     * @return 结果
     */
    public int deletePsiProjectById(String id);

    /**
     * 批量删除项目管理
     * 
     * @param ids 需要删除的数据主键集合
     * @return 结果
     */
    public int deletePsiProjectByIds(String[] ids);
}
