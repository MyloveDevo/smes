package com.ruoyi.purmgt.service.impl;

import java.util.List;
import com.ruoyi.common.utils.DateUtils;
import com.ruoyi.purmgt.domain.PsiPurchaseContract;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import java.util.ArrayList;
import com.ruoyi.common.utils.StringUtils;
import org.springframework.transaction.annotation.Transactional;
import com.ruoyi.purmgt.domain.PsiPurchaseOrderDetail;
import com.ruoyi.purmgt.mapper.PsiPurchaseOrderMapper;
import com.ruoyi.purmgt.domain.PsiPurchaseOrder;
import com.ruoyi.purmgt.service.IPsiPurchaseOrderService;

/**
 * 采购订单Service业务层处理
 * 
 * @author smes
 * @date 2023-06-17
 */
@Service
public class PsiPurchaseOrderServiceImpl implements IPsiPurchaseOrderService 
{
    @Autowired
    private PsiPurchaseOrderMapper psiPurchaseOrderMapper;

    /**
     * 查询采购订单
     * 
     * @param purchaseOrderId 采购订单主键
     * @return 采购订单
     */
    @Override
    public PsiPurchaseOrder selectPsiPurchaseOrderByPurchaseOrderId(Long purchaseOrderId)
    {
        return psiPurchaseOrderMapper.selectPsiPurchaseOrderByPurchaseOrderId(purchaseOrderId);
    }

    /**
     * 查询采购订单列表
     * 
     * @param psiPurchaseOrder 采购订单
     * @return 采购订单
     */
    @Override
    public List<PsiPurchaseOrder> selectPsiPurchaseOrderList(PsiPurchaseOrder psiPurchaseOrder)
    {
        return psiPurchaseOrderMapper.selectPsiPurchaseOrderList(psiPurchaseOrder);
    }

    /**
     * 新增采购订单
     * 
     * @param psiPurchaseOrder 采购订单
     * @return 结果
     */
    @Transactional
    @Override
    public int insertPsiPurchaseOrder(PsiPurchaseOrder psiPurchaseOrder)
    {
        psiPurchaseOrder.setCreateTime(DateUtils.getNowDate());
        int rows = psiPurchaseOrderMapper.insertPsiPurchaseOrder(psiPurchaseOrder);
        insertPsiPurchaseOrderDetail(psiPurchaseOrder);
        insertPsiPurchaseContract(psiPurchaseOrder);
        return rows;
    }

    /**
     * 修改采购订单
     * 
     * @param psiPurchaseOrder 采购订单
     * @return 结果
     */
    @Transactional
    @Override
    public int updatePsiPurchaseOrder(PsiPurchaseOrder psiPurchaseOrder)
    {
        psiPurchaseOrder.setUpdateTime(DateUtils.getNowDate());
        psiPurchaseOrderMapper.deletePsiPurchaseOrderDetailByPurchaseOrderId(psiPurchaseOrder.getPurchaseOrderId());
        insertPsiPurchaseOrderDetail(psiPurchaseOrder);
        psiPurchaseOrderMapper.deletePsiPurchaseContractByPurchaseOrderId(psiPurchaseOrder.getPurchaseOrderId());
        insertPsiPurchaseContract(psiPurchaseOrder);
        return psiPurchaseOrderMapper.updatePsiPurchaseOrder(psiPurchaseOrder);
    }

    /**
     * 批量删除采购订单
     * 
     * @param purchaseOrderIds 需要删除的采购订单主键
     * @return 结果
     */
    @Transactional
    @Override
    public int deletePsiPurchaseOrderByPurchaseOrderIds(Long[] purchaseOrderIds)
    {
        psiPurchaseOrderMapper.deletePsiPurchaseOrderDetailByPurchaseOrderIds(purchaseOrderIds);
        psiPurchaseOrderMapper.deletePsiPurchaseContractByPurchaseOrderIds(purchaseOrderIds);
        return psiPurchaseOrderMapper.deletePsiPurchaseOrderByPurchaseOrderIds(purchaseOrderIds);
    }

    /**
     * 删除采购订单信息
     * 
     * @param purchaseOrderId 采购订单主键
     * @return 结果
     */
    @Transactional
    @Override
    public int deletePsiPurchaseOrderByPurchaseOrderId(Long purchaseOrderId)
    {
        psiPurchaseOrderMapper.deletePsiPurchaseOrderDetailByPurchaseOrderId(purchaseOrderId);
        psiPurchaseOrderMapper.deletePsiPurchaseContractByPurchaseOrderId(purchaseOrderId);
        return psiPurchaseOrderMapper.deletePsiPurchaseOrderByPurchaseOrderId(purchaseOrderId);
    }

    /**
     * 新增采购订单明细信息
     * 
     * @param psiPurchaseOrder 采购订单对象
     */
    public void insertPsiPurchaseOrderDetail(PsiPurchaseOrder psiPurchaseOrder)
    {
        List<PsiPurchaseOrderDetail> psiPurchaseOrderDetailList = psiPurchaseOrder.getPsiPurchaseOrderDetailList();
        Long purchaseOrderId = psiPurchaseOrder.getPurchaseOrderId();
        if (StringUtils.isNotNull(psiPurchaseOrderDetailList))
        {
            List<PsiPurchaseOrderDetail> list = new ArrayList<PsiPurchaseOrderDetail>();
            for (PsiPurchaseOrderDetail psiPurchaseOrderDetail : psiPurchaseOrderDetailList)
            {
                psiPurchaseOrderDetail.setPurchaseOrderId(purchaseOrderId);
                list.add(psiPurchaseOrderDetail);
            }
            if (list.size() > 0)
            {
                psiPurchaseOrderMapper.batchPsiPurchaseOrderDetail(list);
            }
        }
    }

    /**
     * 新增采购合同信息
     *
     * @param psiPurchaseOrder 采购订单对象
     */
    public void insertPsiPurchaseContract(PsiPurchaseOrder psiPurchaseOrder)
    {
        List<PsiPurchaseContract> psiPurchaseContractList = psiPurchaseOrder.getPsiPurchaseContractList();
        Long purchaseOrderId = psiPurchaseOrder.getPurchaseOrderId();
        if (StringUtils.isNotNull(psiPurchaseContractList))
        {
            List<PsiPurchaseContract> list = new ArrayList<PsiPurchaseContract>();
            for (PsiPurchaseContract psiPurchaseContract : psiPurchaseContractList)
            {
                psiPurchaseContract.setPurchaseOrderId(purchaseOrderId);
                list.add(psiPurchaseContract);
            }
            if (list.size() > 0)
            {
                psiPurchaseOrderMapper.batchPsiPurchaseContract(list);
            }
        }
    }
}
