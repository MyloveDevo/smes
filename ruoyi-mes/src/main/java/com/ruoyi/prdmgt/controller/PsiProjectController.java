package com.ruoyi.prdmgt.controller;

import java.util.List;
import javax.servlet.http.HttpServletResponse;

import com.ruoyi.basic.domain.PsiGoods;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.ruoyi.common.annotation.Log;
import com.ruoyi.common.core.controller.BaseController;
import com.ruoyi.common.core.domain.AjaxResult;
import com.ruoyi.common.enums.BusinessType;
import com.ruoyi.prdmgt.domain.PsiProject;
import com.ruoyi.prdmgt.service.IPsiProjectService;
import com.ruoyi.common.utils.poi.ExcelUtil;
import com.ruoyi.common.core.page.TableDataInfo;

/**
 * 项目管理Controller
 * 
 * @author smes
 * @date 2023-06-11
 */
@RestController
@RequestMapping("/prdmgt/project")
public class PsiProjectController extends BaseController
{
    @Autowired
    private IPsiProjectService psiProjectService;

    /**
     * 查询项目管理列表
     */
    @PreAuthorize("@ss.hasPermi('prdmgt:project:list')")
    @GetMapping("/list")
    public TableDataInfo list(PsiProject psiProject)
    {
        startPage();
        List<PsiProject> list = psiProjectService.selectPsiProjectList(psiProject);
        return getDataTable(list);
    }

    /**
     * 导出项目管理列表
     */
    @PreAuthorize("@ss.hasPermi('prdmgt:project:export')")
    @Log(title = "项目管理", businessType = BusinessType.EXPORT)
    @PostMapping("/export")
    public void export(HttpServletResponse response, PsiProject psiProject)
    {
        List<PsiProject> list = psiProjectService.selectPsiProjectList(psiProject);
        ExcelUtil<PsiProject> util = new ExcelUtil<PsiProject>(PsiProject.class);
        util.exportExcel(response, list, "项目管理数据");
    }

    /**
     * 获取项目管理详细信息
     */
    @PreAuthorize("@ss.hasPermi('prdmgt:project:query')")
    @GetMapping(value = "/{id}")
    public AjaxResult getInfo(@PathVariable("id") String id)
    {
        return AjaxResult.success(psiProjectService.selectPsiProjectById(id));
    }

    /**
     * 新增项目管理
     */
    @PreAuthorize("@ss.hasPermi('prdmgt:project:add')")
    @Log(title = "项目管理", businessType = BusinessType.INSERT)
    @PostMapping
    public AjaxResult add(@RequestBody PsiProject psiProject)
    {
        return AjaxResult.success(psiProjectService.insertPsiProject(psiProject));
    }

    /**
     * 修改项目管理
     */
    @PreAuthorize("@ss.hasPermi('prdmgt:project:edit')")
    @Log(title = "项目管理", businessType = BusinessType.UPDATE)
    @PutMapping
    public AjaxResult edit(@RequestBody PsiProject psiProject)
    {
        return toAjax(psiProjectService.updatePsiProject(psiProject));
    }

    /**
     * 删除项目管理
     */
    @PreAuthorize("@ss.hasPermi('prdmgt:project:remove')")
    @Log(title = "项目管理", businessType = BusinessType.DELETE)
	@DeleteMapping("/{ids}")
    public AjaxResult remove(@PathVariable String[] ids)
    {
        return toAjax(psiProjectService.deletePsiProjectByIds(ids));
    }

    /**
     * 查询项目列表
     * 不分页
     */
    @GetMapping("/dropdownList")
    public AjaxResult dropdownList()
    {
        PsiProject entity = new PsiProject();
        entity.setDelFlag("0");
        List<PsiProject> list = psiProjectService.selectPsiProjectList(entity);
        return AjaxResult.success(list);
    }

}
