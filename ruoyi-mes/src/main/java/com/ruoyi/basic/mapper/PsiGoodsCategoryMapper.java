package com.ruoyi.basic.mapper;

import java.util.List;
import com.ruoyi.basic.domain.PsiGoodsCategory;
import com.ruoyi.basic.domain.PsiSupplierCategory;
import com.ruoyi.basic.domain.PsiWarehouse;

/**
 * 产品物料分类Mapper接口
 * 
 * @author smes
 * @date 2023-06-04
 */
public interface PsiGoodsCategoryMapper 
{
    /**
     * 查询产品物料分类
     * 
     * @param id 产品物料分类主键
     * @return 产品物料分类
     */
    public PsiGoodsCategory selectPsiGoodsCategoryById(Long id);

    /**
     * 查询产品物料分类列表
     * 
     * @param psiGoodsCategory 产品物料分类
     * @return 产品物料分类集合
     */
    public List<PsiGoodsCategory> selectPsiGoodsCategoryList(PsiGoodsCategory psiGoodsCategory);

    /**
     * 新增产品物料分类
     * 
     * @param psiGoodsCategory 产品物料分类
     * @return 结果
     */
    public int insertPsiGoodsCategory(PsiGoodsCategory psiGoodsCategory);

    /**
     * 修改产品物料分类
     * 
     * @param psiGoodsCategory 产品物料分类
     * @return 结果
     */
    public int updatePsiGoodsCategory(PsiGoodsCategory psiGoodsCategory);

    /**
     * 删除产品物料分类
     * 
     * @param id 产品物料分类主键
     * @return 结果
     */
    public int deletePsiGoodsCategoryById(Long id);

    /**
     * 批量删除产品物料分类
     * 
     * @param ids 需要删除的数据主键集合
     * @return 结果
     */
    public int deletePsiGoodsCategoryByIds(Long[] ids);

    List<PsiGoodsCategory> findOneByCondition(PsiGoodsCategory entiry);
}
