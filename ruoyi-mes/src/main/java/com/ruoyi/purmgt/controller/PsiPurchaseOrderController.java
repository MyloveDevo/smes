package com.ruoyi.purmgt.controller;

import java.util.List;
import javax.servlet.http.HttpServletResponse;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.ruoyi.common.annotation.Log;
import com.ruoyi.common.core.controller.BaseController;
import com.ruoyi.common.core.domain.AjaxResult;
import com.ruoyi.common.enums.BusinessType;
import com.ruoyi.purmgt.domain.PsiPurchaseOrder;
import com.ruoyi.purmgt.service.IPsiPurchaseOrderService;
import com.ruoyi.common.utils.poi.ExcelUtil;
import com.ruoyi.common.core.page.TableDataInfo;

/**
 * 采购订单Controller
 * 
 * @author smes
 * @date 2023-06-17
 */
@RestController
@RequestMapping("/purmgt/order")
public class PsiPurchaseOrderController extends BaseController
{
    @Autowired
    private IPsiPurchaseOrderService psiPurchaseOrderService;

    /**
     * 查询采购订单列表
     */
    @PreAuthorize("@ss.hasPermi('purmgt:order:list')")
    @GetMapping("/list")
    public TableDataInfo list(PsiPurchaseOrder psiPurchaseOrder)
    {
        startPage();
        List<PsiPurchaseOrder> list = psiPurchaseOrderService.selectPsiPurchaseOrderList(psiPurchaseOrder);
        return getDataTable(list);
    }

    /**
     * 导出采购订单列表
     */
    @PreAuthorize("@ss.hasPermi('purmgt:order:export')")
    @Log(title = "采购订单", businessType = BusinessType.EXPORT)
    @PostMapping("/export")
    public void export(HttpServletResponse response, PsiPurchaseOrder psiPurchaseOrder)
    {
        List<PsiPurchaseOrder> list = psiPurchaseOrderService.selectPsiPurchaseOrderList(psiPurchaseOrder);
        ExcelUtil<PsiPurchaseOrder> util = new ExcelUtil<PsiPurchaseOrder>(PsiPurchaseOrder.class);
        util.exportExcel(response, list, "采购订单数据");
    }

    /**
     * 获取采购订单详细信息
     */
    @PreAuthorize("@ss.hasPermi('purmgt:order:query')")
    @GetMapping(value = "/{purchaseOrderId}")
    public AjaxResult getInfo(@PathVariable("purchaseOrderId") Long purchaseOrderId)
    {
        return AjaxResult.success(psiPurchaseOrderService.selectPsiPurchaseOrderByPurchaseOrderId(purchaseOrderId));
    }

    /**
     * 新增采购订单
     */
    @PreAuthorize("@ss.hasPermi('purmgt:order:add')")
    @Log(title = "采购订单", businessType = BusinessType.INSERT)
    @PostMapping
    public AjaxResult add(@RequestBody PsiPurchaseOrder psiPurchaseOrder)
    {
        return toAjax(psiPurchaseOrderService.insertPsiPurchaseOrder(psiPurchaseOrder));
    }

    /**
     * 修改采购订单
     */
    @PreAuthorize("@ss.hasPermi('purmgt:order:edit')")
    @Log(title = "采购订单", businessType = BusinessType.UPDATE)
    @PutMapping
    public AjaxResult edit(@RequestBody PsiPurchaseOrder psiPurchaseOrder)
    {
        return toAjax(psiPurchaseOrderService.updatePsiPurchaseOrder(psiPurchaseOrder));
    }

    /**
     * 删除采购订单
     */
    @PreAuthorize("@ss.hasPermi('purmgt:order:remove')")
    @Log(title = "采购订单", businessType = BusinessType.DELETE)
	@DeleteMapping("/{purchaseOrderIds}")
    public AjaxResult remove(@PathVariable Long[] purchaseOrderIds)
    {
        return toAjax(psiPurchaseOrderService.deletePsiPurchaseOrderByPurchaseOrderIds(purchaseOrderIds));
    }
}
