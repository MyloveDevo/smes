package com.ruoyi.prdmgt.domain;

import java.math.BigDecimal;
import java.util.Date;
import com.fasterxml.jackson.annotation.JsonFormat;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import com.ruoyi.common.annotation.Excel;
import com.ruoyi.common.core.domain.BaseEntity;

/**
 * 项目BOM对象 psi_project_bom
 * 
 * @author smes
 * @date 2023-06-12
 */
public class PsiProjectBom extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /** ID */
    private Long id;

    /** 项目ID */
    @Excel(name = "项目ID")
    private Long projectId;

    /** ParentID */
    @Excel(name = "ParentID")
    private Long parentId;

    /** 产品ID */
    @Excel(name = "产品ID")
    private Long goodsId;

    /** 产品名称 */
    @Excel(name = "产品名称")
    private String goodsName;

    /** 产品编码 */
    @Excel(name = "产品编码")
    private String goodsCode;

    /** 产品批次 */
    @Excel(name = "产品批次")
    private String goodsBatch;

    /** 产品类型 */
    @Excel(name = "产品类型")
    private String goodsCategory;

    /** 产品规格 */
    @Excel(name = "产品规格")
    private String spec;

    /** 条码 */
    @Excel(name = "条码")
    private String barcode;

    /** 预设采购单价 */
    @Excel(name = "预设采购单价")
    private BigDecimal prePurchasePrice;

    /** 计量单位 */
    @Excel(name = "计量单位")
    private String unit;

    /** 单位额定成本 */
    @Excel(name = "单位额定成本")
    private BigDecimal unitRatedCost;

    /** 需要数量 */
    @Excel(name = "需要数量")
    private Long bomQty;

    /** 合计成本 */
    @Excel(name = "合计成本")
    private Long totalCost;

    /** 需要时间 */
    @JsonFormat(pattern = "yyyy-MM-dd")
    @Excel(name = "需要时间", width = 30, dateFormat = "yyyy-MM-dd")
    private Date reqTime;

    /** 0, 未领料; 1, 已领料 */
    @Excel(name = "0, 未领料; 1, 已领料")
    private String isPicked;

    /** 开始时间 */
    @JsonFormat(pattern = "yyyy-MM-dd")
    @Excel(name = "开始时间", width = 30, dateFormat = "yyyy-MM-dd")
    private Date startTime;

    /** 结束时间 */
    @JsonFormat(pattern = "yyyy-MM-dd")
    @Excel(name = "结束时间", width = 30, dateFormat = "yyyy-MM-dd")
    private Date endTime;

    /** 删除标识 */
    private String delFlag;

    /** 创建者ID */
    @Excel(name = "创建者ID")
    private Long createId;

    /** 更新者ID */
    @Excel(name = "更新者ID")
    private Long updateId;

    public void setId(Long id) 
    {
        this.id = id;
    }

    public Long getId() 
    {
        return id;
    }
    public void setProjectId(Long projectId) 
    {
        this.projectId = projectId;
    }

    public Long getProjectId() 
    {
        return projectId;
    }
    public void setParentId(Long parentId) 
    {
        this.parentId = parentId;
    }

    public Long getParentId() 
    {
        return parentId;
    }
    public void setGoodsId(Long goodsId) 
    {
        this.goodsId = goodsId;
    }

    public Long getGoodsId() 
    {
        return goodsId;
    }
    public void setGoodsName(String goodsName) 
    {
        this.goodsName = goodsName;
    }

    public String getGoodsName() 
    {
        return goodsName;
    }
    public void setGoodsCode(String goodsCode) 
    {
        this.goodsCode = goodsCode;
    }

    public String getGoodsCode() 
    {
        return goodsCode;
    }
    public void setGoodsBatch(String goodsBatch) 
    {
        this.goodsBatch = goodsBatch;
    }

    public String getGoodsBatch() 
    {
        return goodsBatch;
    }
    public void setGoodsCategory(String goodsCategory) 
    {
        this.goodsCategory = goodsCategory;
    }

    public String getGoodsCategory() 
    {
        return goodsCategory;
    }
    public void setSpec(String spec) 
    {
        this.spec = spec;
    }

    public String getSpec() 
    {
        return spec;
    }
    public void setBarcode(String barcode) 
    {
        this.barcode = barcode;
    }

    public String getBarcode() 
    {
        return barcode;
    }
    public void setPrePurchasePrice(BigDecimal prePurchasePrice) 
    {
        this.prePurchasePrice = prePurchasePrice;
    }

    public BigDecimal getPrePurchasePrice() 
    {
        return prePurchasePrice;
    }
    public void setUnit(String unit) 
    {
        this.unit = unit;
    }

    public String getUnit() 
    {
        return unit;
    }
    public void setUnitRatedCost(BigDecimal unitRatedCost) 
    {
        this.unitRatedCost = unitRatedCost;
    }

    public BigDecimal getUnitRatedCost() 
    {
        return unitRatedCost;
    }
    public void setBomQty(Long bomQty) 
    {
        this.bomQty = bomQty;
    }

    public Long getBomQty() 
    {
        return bomQty;
    }
    public void setTotalCost(Long totalCost) 
    {
        this.totalCost = totalCost;
    }

    public Long getTotalCost() 
    {
        return totalCost;
    }
    public void setReqTime(Date reqTime) 
    {
        this.reqTime = reqTime;
    }

    public Date getReqTime() 
    {
        return reqTime;
    }
    public void setIsPicked(String isPicked) 
    {
        this.isPicked = isPicked;
    }

    public String getIsPicked() 
    {
        return isPicked;
    }
    public void setStartTime(Date startTime) 
    {
        this.startTime = startTime;
    }

    public Date getStartTime() 
    {
        return startTime;
    }
    public void setEndTime(Date endTime) 
    {
        this.endTime = endTime;
    }

    public Date getEndTime() 
    {
        return endTime;
    }
    public void setDelFlag(String delFlag) 
    {
        this.delFlag = delFlag;
    }

    public String getDelFlag() 
    {
        return delFlag;
    }
    public void setCreateId(Long createId) 
    {
        this.createId = createId;
    }

    public Long getCreateId() 
    {
        return createId;
    }
    public void setUpdateId(Long updateId) 
    {
        this.updateId = updateId;
    }

    public Long getUpdateId() 
    {
        return updateId;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this,ToStringStyle.MULTI_LINE_STYLE)
            .append("id", getId())
            .append("projectId", getProjectId())
            .append("parentId", getParentId())
            .append("goodsId", getGoodsId())
            .append("goodsName", getGoodsName())
            .append("goodsCode", getGoodsCode())
            .append("goodsBatch", getGoodsBatch())
            .append("goodsCategory", getGoodsCategory())
            .append("spec", getSpec())
            .append("barcode", getBarcode())
            .append("prePurchasePrice", getPrePurchasePrice())
            .append("unit", getUnit())
            .append("unitRatedCost", getUnitRatedCost())
            .append("bomQty", getBomQty())
            .append("totalCost", getTotalCost())
            .append("reqTime", getReqTime())
            .append("isPicked", getIsPicked())
            .append("startTime", getStartTime())
            .append("endTime", getEndTime())
            .append("remark", getRemark())
            .append("delFlag", getDelFlag())
            .append("createId", getCreateId())
            .append("createBy", getCreateBy())
            .append("createTime", getCreateTime())
            .append("updateId", getUpdateId())
            .append("updateBy", getUpdateBy())
            .append("updateTime", getUpdateTime())
            .toString();
    }
}
