import request from '@/utils/request'

// 查询供应商分类列表
export function listSupplierctg(query) {
  return request({
    url: '/basic/supplierctg/list',
    method: 'get',
    params: query
  })
}

// 供应商分类列表DropdownList
export function supplierListCtgAll(query) {
  return request({
    url: '/basic/supplierctg/dropdownList',
    method: 'get',
    params: query
  })
}

// 查询供应商分类详细
export function getSupplierctg(id) {
  return request({
    url: '/basic/supplierctg/' + id,
    method: 'get'
  })
}

// 新增供应商分类
export function addSupplierctg(data) {
  return request({
    url: '/basic/supplierctg',
    method: 'post',
    data: data
  })
}

// 修改供应商分类
export function updateSupplierctg(data) {
  return request({
    url: '/basic/supplierctg',
    method: 'put',
    data: data
  })
}

// 删除供应商分类
export function delSupplierctg(id) {
  return request({
    url: '/basic/supplierctg/' + id,
    method: 'delete'
  })
}
