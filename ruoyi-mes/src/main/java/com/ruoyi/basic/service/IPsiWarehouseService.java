package com.ruoyi.basic.service;

import java.util.List;
import com.ruoyi.basic.domain.PsiWarehouse;

/**
 * 仓库Service接口
 * 
 * @author smes
 * @date 2023-06-05
 */
public interface IPsiWarehouseService 
{
    /**
     * 查询仓库
     * 
     * @param id 仓库主键
     * @return 仓库
     */
    public PsiWarehouse selectPsiWarehouseById(Long id);

    /**
     * 查询仓库列表
     * 
     * @param psiWarehouse 仓库
     * @return 仓库集合
     */
    public List<PsiWarehouse> selectPsiWarehouseList(PsiWarehouse psiWarehouse);

    /**
     * 新增仓库
     * 
     * @param psiWarehouse 仓库
     * @return 结果
     */
    public int insertPsiWarehouse(PsiWarehouse psiWarehouse);

    /**
     * 修改仓库
     * 
     * @param psiWarehouse 仓库
     * @return 结果
     */
    public int updatePsiWarehouse(PsiWarehouse psiWarehouse);

    /**
     * 批量删除仓库
     * 
     * @param ids 需要删除的仓库主键集合
     * @return 结果
     */
    public int deletePsiWarehouseByIds(Long[] ids);

    /**
     * 删除仓库信息
     * 
     * @param id 仓库主键
     * @return 结果
     */
    public int deletePsiWarehouseById(Long id);

    /**
     * 查重
     */
    public boolean isDuplicate(PsiWarehouse psiWarehouse);
}