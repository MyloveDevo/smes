import request from '@/utils/request'

// 查询产品物料列表
export function listGoods(query) {
  return request({
    url: '/basic/goods/list',
    method: 'get',
    params: query
  })
}

// 查询产品物料详细
export function getGoods(id) {
  return request({
    url: '/basic/goods/' + id,
    method: 'get'
  })
}

// 新增产品物料
export function addGoods(data) {
  return request({
    url: '/basic/goods',
    method: 'post',
    data: data
  })
}

// 修改产品物料
export function updateGoods(data) {
  return request({
    url: '/basic/goods',
    method: 'put',
    data: data
  })
}

// 删除产品物料
export function delGoods(id) {
  return request({
    url: '/basic/goods/' + id,
    method: 'delete'
  })
}

// 产品物料列表DropdownList
export function goodsListAll(query) {
  return request({
    url: '/basic/goods/dropdownList',
    method: 'get',
    params: query
  })
}
