package com.ruoyi.basic.domain;

import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import com.ruoyi.common.annotation.Excel;
import com.ruoyi.common.core.domain.BaseEntity;

import java.math.BigDecimal;

/**
 * BOM对象 psi_bom
 * 
 * @author smes
 * @date 2023-06-08
 */
public class PsiBom extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /** BOMID */
    private Long id;

    /** 名称 */
    @Excel(name = "名称")
    private String name;

    /** 编码 */
    @Excel(name = "编码")
    private String code;

    /** 父ID */
    @Excel(name = "父ID")
    private Long parentId;

    /** 产品ID */
    @Excel(name = "产品ID")
    private Long goodsId;

    /** 产品名称 */
    @Excel(name = "产品名称")
    private String goodsName;

    /** 产品编码 */
    @Excel(name = "产品编码")
    private String goodsCode;

    /** 产品批次 */
    @Excel(name = "产品批次")
    private String goodsBatch;

    /** 产品类型 */
    @Excel(name = "产品类型")
    private String goodsCategory;

    /** 产品规格 */
    @Excel(name = "产品规格")
    private String spec;

    /** 条码 */
    @Excel(name = "条码")
    private String barcode;

    /** 预设采购成本 */
    @Excel(name = "预设采购成本")
    private Long prePurchasePrice;

    /** 计量单位 */
    @Excel(name = "计量单位")
    private String unit;

    /** 数量 */
    @Excel(name = "数量")
    private BigDecimal bomQty;

    /** 删除标识 */
    @Excel(name = "删除标识")
    private String deleteFlag;

    /** 创建者ID */
    @Excel(name = "创建者ID")
    private Long createUserId;

    /** 更新者ID */
    @Excel(name = "更新者ID")
    private Long updateUserId;

    public void setId(Long id) 
    {
        this.id = id;
    }

    public Long getId() 
    {
        return id;
    }
    public void setName(String name) 
    {
        this.name = name;
    }

    public String getName() 
    {
        return name;
    }
    public void setCode(String code) 
    {
        this.code = code;
    }

    public String getCode() 
    {
        return code;
    }
    public void setParentId(Long parentId) 
    {
        this.parentId = parentId;
    }

    public Long getParentId() 
    {
        return parentId;
    }
    public void setGoodsId(Long goodsId) 
    {
        this.goodsId = goodsId;
    }

    public Long getGoodsId() 
    {
        return goodsId;
    }
    public void setGoodsName(String goodsName) 
    {
        this.goodsName = goodsName;
    }

    public String getGoodsName() 
    {
        return goodsName;
    }
    public void setGoodsCode(String goodsCode) 
    {
        this.goodsCode = goodsCode;
    }

    public String getGoodsCode() 
    {
        return goodsCode;
    }
    public void setGoodsBatch(String goodsBatch) 
    {
        this.goodsBatch = goodsBatch;
    }

    public String getGoodsBatch() 
    {
        return goodsBatch;
    }
    public void setGoodsCategory(String goodsCategory) 
    {
        this.goodsCategory = goodsCategory;
    }

    public String getGoodsCategory() 
    {
        return goodsCategory;
    }
    public void setSpec(String spec) 
    {
        this.spec = spec;
    }

    public String getSpec() 
    {
        return spec;
    }
    public void setBarcode(String barcode) 
    {
        this.barcode = barcode;
    }

    public String getBarcode() 
    {
        return barcode;
    }
    public void setPrePurchasePrice(Long prePurchasePrice) 
    {
        this.prePurchasePrice = prePurchasePrice;
    }

    public Long getPrePurchasePrice() 
    {
        return prePurchasePrice;
    }
    public void setUnit(String unit) 
    {
        this.unit = unit;
    }

    public String getUnit()
    {
        return unit;
    }

    public BigDecimal getBomQty() {
        return bomQty;
    }

    public void setBomQty(BigDecimal bomQty) {
        this.bomQty = bomQty;
    }

    public void setDeleteFlag(String deleteFlag)
    {
        this.deleteFlag = deleteFlag;
    }

    public String getDeleteFlag() 
    {
        return deleteFlag;
    }
    public void setCreateUserId(Long createUserId) 
    {
        this.createUserId = createUserId;
    }

    public Long getCreateUserId() 
    {
        return createUserId;
    }
    public void setUpdateUserId(Long updateUserId) 
    {
        this.updateUserId = updateUserId;
    }

    public Long getUpdateUserId() 
    {
        return updateUserId;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this,ToStringStyle.MULTI_LINE_STYLE)
            .append("id", getId())
            .append("name", getName())
            .append("code", getCode())
            .append("parentId", getParentId())
            .append("goodsId", getGoodsId())
            .append("goodsName", getGoodsName())
            .append("goodsCode", getGoodsCode())
            .append("goodsBatch", getGoodsBatch())
            .append("goodsCategory", getGoodsCategory())
            .append("spec", getSpec())
            .append("barcode", getBarcode())
            .append("prePurchasePrice", getPrePurchasePrice())
            .append("unit", getUnit())
            .append("bomQty",getBomQty())
            .append("remark", getRemark())
            .append("deleteFlag", getDeleteFlag())
            .append("createUserId", getCreateUserId())
            .append("createBy", getCreateBy())
            .append("createTime", getCreateTime())
            .append("updateUserId", getUpdateUserId())
            .append("updateBy", getUpdateBy())
            .append("updateTime", getUpdateTime())
            .toString();
    }
}
