-- 菜单 SQL
insert into sys_menu (menu_name, parent_id, order_num, path, component, is_frame, is_cache, menu_type, visible, status, perms, icon, create_by, create_time, update_by, update_time, remark)
values('客户分类', '2013', '1', 'customerctg', 'basic/customerctg/index', 1, 0, 'C', '0', '0', 'basic:customerctg:list', '#', 'admin', sysdate(), '', null, '客户分类菜单');

-- 按钮父菜单ID
SELECT @parentId := LAST_INSERT_ID();

-- 按钮 SQL
insert into sys_menu (menu_name, parent_id, order_num, path, component, is_frame, is_cache, menu_type, visible, status, perms, icon, create_by, create_time, update_by, update_time, remark)
values('客户分类查询', @parentId, '1',  '#', '', 1, 0, 'F', '0', '0', 'basic:customerctg:query',        '#', 'admin', sysdate(), '', null, '');

insert into sys_menu (menu_name, parent_id, order_num, path, component, is_frame, is_cache, menu_type, visible, status, perms, icon, create_by, create_time, update_by, update_time, remark)
values('客户分类新增', @parentId, '2',  '#', '', 1, 0, 'F', '0', '0', 'basic:customerctg:add',          '#', 'admin', sysdate(), '', null, '');

insert into sys_menu (menu_name, parent_id, order_num, path, component, is_frame, is_cache, menu_type, visible, status, perms, icon, create_by, create_time, update_by, update_time, remark)
values('客户分类修改', @parentId, '3',  '#', '', 1, 0, 'F', '0', '0', 'basic:customerctg:edit',         '#', 'admin', sysdate(), '', null, '');

insert into sys_menu (menu_name, parent_id, order_num, path, component, is_frame, is_cache, menu_type, visible, status, perms, icon, create_by, create_time, update_by, update_time, remark)
values('客户分类删除', @parentId, '4',  '#', '', 1, 0, 'F', '0', '0', 'basic:customerctg:remove',       '#', 'admin', sysdate(), '', null, '');

insert into sys_menu (menu_name, parent_id, order_num, path, component, is_frame, is_cache, menu_type, visible, status, perms, icon, create_by, create_time, update_by, update_time, remark)
values('客户分类导出', @parentId, '5',  '#', '', 1, 0, 'F', '0', '0', 'basic:customerctg:export',       '#', 'admin', sysdate(), '', null, '');

INSERT INTO `sys_menu` VALUES (2068, '标准BOM', 2013, 1, 'index', 'basic/bom/index', NULL, 1, 0, 'C', '0', '0', 'basic:bom:list', '#', 'admin', '2023-06-07 17:07:51', 'admin', '2023-06-09 16:53:11', '标准BOM菜单');
INSERT INTO `sys_menu` VALUES (2069, '标准BOM查询', 2068, 2, '#', '', NULL, 1, 0, 'F', '0', '0', 'basic:bom:query', '#', 'admin', '2023-06-07 17:07:51', 'admin', '2023-06-08 10:37:18', '');
INSERT INTO `sys_menu` VALUES (2070, '标准BOM新增', 2068, 3, '#', '', NULL, 1, 0, 'F', '0', '0', 'basic:bom:add', '#', 'admin', '2023-06-07 17:07:51', 'admin', '2023-06-08 10:37:25', '');
INSERT INTO `sys_menu` VALUES (2071, '标准BOM修改', 2068, 4, '#', '', NULL, 1, 0, 'F', '0', '0', 'basic:bom:edit', '#', 'admin', '2023-06-07 17:07:51', 'admin', '2023-06-08 10:37:29', '');
INSERT INTO `sys_menu` VALUES (2072, '标准BOM删除', 2068, 5, '#', '', NULL, 1, 0, 'F', '0', '0', 'basic:bom:remove', '#', 'admin', '2023-06-07 17:07:51', 'admin', '2023-06-08 10:37:34', '');
INSERT INTO `sys_menu` VALUES (2073, '标准BOM导出', 2068, 6, '#', '', NULL, 1, 0, 'F', '0', '0', 'basic:bom:export', '#', 'admin', '2023-06-07 17:07:51', 'admin', '2023-06-08 10:37:40', '');
INSERT INTO `sys_menu` VALUES (2074, '新增或修改BOM', 2013, 1, 'createOrEditBom/:id', 'basic/bom/createOrEditBom', '', 1, 0, 'C', '1', '0', '', 'edit', 'admin', '2023-06-08 10:24:02', 'admin', '2023-06-09 16:52:52', '');
