package com.ruoyi.inventorymgt.service;

import java.util.List;
import com.ruoyi.inventorymgt.domain.PsiInventory;

/**
 * 实时库存Service接口
 * 
 * @author smes
 * @date 2023-06-20
 */
public interface IPsiInventoryService 
{
    /**
     * 查询实时库存
     * 
     * @param id 实时库存主键
     * @return 实时库存
     */
    public PsiInventory selectPsiInventoryById(Long id);

    /**
     * 查询实时库存列表
     * 
     * @param psiInventory 实时库存
     * @return 实时库存集合
     */
    public List<PsiInventory> selectPsiInventoryList(PsiInventory psiInventory);

    /**
     * 新增实时库存
     * 
     * @param psiInventory 实时库存
     * @return 结果
     */
    public int insertPsiInventory(PsiInventory psiInventory);

    /**
     * 修改实时库存
     * 
     * @param psiInventory 实时库存
     * @return 结果
     */
    public int updatePsiInventory(PsiInventory psiInventory);

    /**
     * 批量删除实时库存
     * 
     * @param ids 需要删除的实时库存主键集合
     * @return 结果
     */
    public int deletePsiInventoryByIds(Long[] ids);

    /**
     * 删除实时库存信息
     * 
     * @param id 实时库存主键
     * @return 结果
     */
    public int deletePsiInventoryById(Long id);
}
