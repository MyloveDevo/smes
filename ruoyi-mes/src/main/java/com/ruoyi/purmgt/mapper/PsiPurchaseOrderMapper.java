package com.ruoyi.purmgt.mapper;

import java.util.List;

import com.ruoyi.purmgt.domain.PsiPurchaseContract;
import com.ruoyi.purmgt.domain.PsiPurchaseOrder;
import com.ruoyi.purmgt.domain.PsiPurchaseOrderDetail;

/**
 * 采购订单Mapper接口
 * 
 * @author smes
 * @date 2023-06-17
 */
public interface PsiPurchaseOrderMapper 
{
    /**
     * 查询采购订单
     * 
     * @param purchaseOrderId 采购订单主键
     * @return 采购订单
     */
    public PsiPurchaseOrder selectPsiPurchaseOrderByPurchaseOrderId(Long purchaseOrderId);

    /**
     * 查询采购订单列表
     * 
     * @param psiPurchaseOrder 采购订单
     * @return 采购订单集合
     */
    public List<PsiPurchaseOrder> selectPsiPurchaseOrderList(PsiPurchaseOrder psiPurchaseOrder);

    /**
     * 新增采购订单
     * 
     * @param psiPurchaseOrder 采购订单
     * @return 结果
     */
    public int insertPsiPurchaseOrder(PsiPurchaseOrder psiPurchaseOrder);

    /**
     * 修改采购订单
     * 
     * @param psiPurchaseOrder 采购订单
     * @return 结果
     */
    public int updatePsiPurchaseOrder(PsiPurchaseOrder psiPurchaseOrder);

    /**
     * 删除采购订单
     * 
     * @param purchaseOrderId 采购订单主键
     * @return 结果
     */
    public int deletePsiPurchaseOrderByPurchaseOrderId(Long purchaseOrderId);

    /**
     * 批量删除采购订单
     * 
     * @param purchaseOrderIds 需要删除的数据主键集合
     * @return 结果
     */
    public int deletePsiPurchaseOrderByPurchaseOrderIds(Long[] purchaseOrderIds);

    /**
     * 批量删除采购订单明细
     * 
     * @param purchaseOrderIds 需要删除的数据主键集合
     * @return 结果
     */
    public int deletePsiPurchaseOrderDetailByPurchaseOrderIds(Long[] purchaseOrderIds);
    
    /**
     * 批量新增采购订单明细
     * 
     * @param psiPurchaseOrderDetailList 采购订单明细列表
     * @return 结果
     */
    public int batchPsiPurchaseOrderDetail(List<PsiPurchaseOrderDetail> psiPurchaseOrderDetailList);
    

    /**
     * 通过采购订单主键删除采购订单明细信息
     * 
     * @param purchaseOrderId 采购订单ID
     * @return 结果
     */
    public int deletePsiPurchaseOrderDetailByPurchaseOrderId(Long purchaseOrderId);

    /**
     * 批量删除采购合同
     *
     * @param purchaseOrderIds 需要删除的数据主键集合
     * @return 结果
     */
    public int deletePsiPurchaseContractByPurchaseOrderIds(Long[] purchaseOrderIds);

    /**
     * 批量新增采购合同
     *
     * @param psiPurchaseContractList 采购合同列表
     * @return 结果
     */
    public int batchPsiPurchaseContract(List<PsiPurchaseContract> psiPurchaseContractList);


    /**
     * 通过采购订单主键删除采购合同信息
     *
     * @param purchaseOrderId 采购订单ID
     * @return 结果
     */
    public int deletePsiPurchaseContractByPurchaseOrderId(Long purchaseOrderId);
}
