package com.ruoyi.inventorymgt.service.impl;

import java.util.List;
import com.ruoyi.common.utils.DateUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.ruoyi.inventorymgt.mapper.PsiInventoryMapper;
import com.ruoyi.inventorymgt.domain.PsiInventory;
import com.ruoyi.inventorymgt.service.IPsiInventoryService;

/**
 * 实时库存Service业务层处理
 * 
 * @author smes
 * @date 2023-06-20
 */
@Service
public class PsiInventoryServiceImpl implements IPsiInventoryService 
{
    @Autowired
    private PsiInventoryMapper psiInventoryMapper;

    /**
     * 查询实时库存
     * 
     * @param id 实时库存主键
     * @return 实时库存
     */
    @Override
    public PsiInventory selectPsiInventoryById(Long id)
    {
        return psiInventoryMapper.selectPsiInventoryById(id);
    }

    /**
     * 查询实时库存列表
     * 
     * @param psiInventory 实时库存
     * @return 实时库存
     */
    @Override
    public List<PsiInventory> selectPsiInventoryList(PsiInventory psiInventory)
    {
        return psiInventoryMapper.selectPsiInventoryList(psiInventory);
    }

    /**
     * 新增实时库存
     * 
     * @param psiInventory 实时库存
     * @return 结果
     */
    @Override
    public int insertPsiInventory(PsiInventory psiInventory)
    {
        psiInventory.setCreateTime(DateUtils.getNowDate());
        return psiInventoryMapper.insertPsiInventory(psiInventory);
    }

    /**
     * 修改实时库存
     * 
     * @param psiInventory 实时库存
     * @return 结果
     */
    @Override
    public int updatePsiInventory(PsiInventory psiInventory)
    {
        psiInventory.setUpdateTime(DateUtils.getNowDate());
        return psiInventoryMapper.updatePsiInventory(psiInventory);
    }

    /**
     * 批量删除实时库存
     * 
     * @param ids 需要删除的实时库存主键
     * @return 结果
     */
    @Override
    public int deletePsiInventoryByIds(Long[] ids)
    {
        return psiInventoryMapper.deletePsiInventoryByIds(ids);
    }

    /**
     * 删除实时库存信息
     * 
     * @param id 实时库存主键
     * @return 结果
     */
    @Override
    public int deletePsiInventoryById(Long id)
    {
        return psiInventoryMapper.deletePsiInventoryById(id);
    }
}
