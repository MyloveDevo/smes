package com.ruoyi.purmgt.mapper;

import java.util.List;
import com.ruoyi.purmgt.domain.PsiPurchaseReq;
import com.ruoyi.purmgt.domain.PsiPurchaseReqDetail;

/**
 * 请购单 Mapper接口
 * 
 * @author smes
 * @date 2023-06-19
 */
public interface PsiPurchaseReqMapper 
{
    /**
     * 查询请购单 
     * 
     * @param id 请购单 主键
     * @return 请购单 
     */
    public PsiPurchaseReq selectPsiPurchaseReqById(Long id);

    /**
     * 查询请购单 列表
     * 
     * @param psiPurchaseReq 请购单 
     * @return 请购单 集合
     */
    public List<PsiPurchaseReq> selectPsiPurchaseReqList(PsiPurchaseReq psiPurchaseReq);

    /**
     * 新增请购单 
     * 
     * @param psiPurchaseReq 请购单 
     * @return 结果
     */
    public int insertPsiPurchaseReq(PsiPurchaseReq psiPurchaseReq);

    /**
     * 修改请购单 
     * 
     * @param psiPurchaseReq 请购单 
     * @return 结果
     */
    public int updatePsiPurchaseReq(PsiPurchaseReq psiPurchaseReq);

    /**
     * 删除请购单 
     * 
     * @param id 请购单 主键
     * @return 结果
     */
    public int deletePsiPurchaseReqById(Long id);

    /**
     * 批量删除请购单 
     * 
     * @param ids 需要删除的数据主键集合
     * @return 结果
     */
    public int deletePsiPurchaseReqByIds(Long[] ids);

    /**
     * 批量删除请购单明细 
     * 
     * @param ids 需要删除的数据主键集合
     * @return 结果
     */
    public int deletePsiPurchaseReqDetailByPrIds(Long[] ids);
    
    /**
     * 批量新增请购单明细 
     * 
     * @param psiPurchaseReqDetailList 请购单明细 列表
     * @return 结果
     */
    public int batchPsiPurchaseReqDetail(List<PsiPurchaseReqDetail> psiPurchaseReqDetailList);
    

    /**
     * 通过请购单 主键删除请购单明细 信息
     * 
     * @param id 请购单 ID
     * @return 结果
     */
    public int deletePsiPurchaseReqDetailByPrId(Long id);
}
