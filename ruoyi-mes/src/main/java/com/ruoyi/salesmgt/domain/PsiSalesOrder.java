package com.ruoyi.salesmgt.domain;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;
import java.util.Date;

import com.fasterxml.jackson.annotation.JsonFormat;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import com.ruoyi.common.annotation.Excel;
import com.ruoyi.common.core.domain.BaseEntity;

/**
 * 销售订单对象 psi_sales_order
 *
 * @author smes
 * @date 2023-06-12
 */
public class PsiSalesOrder extends BaseEntity {
    private static final long serialVersionUID = 1L;

    /**
     * 销售订单ID
     */
    private Long salesOrderId;

    /**
     * 单据编号
     */
    @Excel(name = "单据编号")
    private String salesOrderNum;

    /**
     * 单据日期
     */
    @JsonFormat(pattern = "yyyy-MM-dd")
    @Excel(name = "单据日期", width = 30, dateFormat = "yyyy-MM-dd")
    private Date docDate;

    /**
     * 单据主题
     */
    @Excel(name = "单据主题")
    private String docSubject;

    /**
     * 业务员ID
     */
    @Excel(name = "业务员ID")
    private Long salesmenId;

    /**
     * 业务部门ID
     */
    @Excel(name = "业务部门ID")
    private Long departmentId;

    /**
     * 合同编号
     */
    @Excel(name = "合同编号")
    private String contractNum;

    /**
     * 客户ID
     */
    @Excel(name = "客户ID")
    private Long customerId;

    /**
     * 联系人
     */
    @Excel(name = "联系人")
    private String contact;

    /**
     * 联系电话
     */
    @Excel(name = "联系电话")
    private String phone;

    /**
     * 传真
     */
    @Excel(name = "传真")
    private String fax;

    /**
     * 手机
     */
    @Excel(name = "手机")
    private String mobile;

    /**
     * 地址
     */
    @Excel(name = "地址")
    private String address;

    /**
     * E-mail
     */
    @Excel(name = "E-mail")
    private String email;

    /**
     * 区域
     */
    @Excel(name = "区域")
    private String area;

    /**
     * 交货日期
     */
    @JsonFormat(pattern = "yyyy-MM-dd")
    @Excel(name = "交货日期", width = 30, dateFormat = "yyyy-MM-dd")
    private Date deliveryDate;

    /**
     * 交货地点
     */
    @Excel(name = "交货地点")
    private String deliveryPoint;

    /**
     * 运输方式
     */
    @Excel(name = "运输方式")
    private String transportModes;

    /**
     * 付款方式
     */
    @Excel(name = "付款方式")
    private String paymentMethod;

    /**
     * 结算方式
     */
    @Excel(name = "结算方式")
    private String settlementMethod;

    /**
     * 开票方式
     */
    @Excel(name = "开票方式")
    private String invoicingMethod;

    /**
     * 发票类型
     */
    @Excel(name = "发票类型")
    private String invoiceType;

    /**
     * 币种
     */
    @Excel(name = "币种")
    private String currency;

    /**
     * 汇率
     */
    @Excel(name = "汇率")
    private BigDecimal exchangeRate;

    /**
     * 合计销售数量
     */
    @Excel(name = "合计销售数量")
    private Long sumSalesNum;

    /**
     * 合计销售金额
     */
    @Excel(name = "合计销售金额")
    private BigDecimal sumTaxIncluded;

    /**
     * 单据阶段
     */
    @Excel(name = "单据阶段")
    private String docStage;

    /**
     * 单据状态
     */
    @Excel(name = "单据状态")
    private String docState;

    /**
     * 删除标志
     */
    private String delFlag;

    /** 附件 */
    @Excel(name = "附件")
    private String attachmentFile;

    /**
     * 销售合同信息
     */
    private List<PsiSalesContract> psiSalesContractList = new ArrayList<PsiSalesContract>();

    /**
     * 销售订单明细信息
     */
    private List<PsiSalesOrderDetail> psiSalesOrderDetailList = new ArrayList<PsiSalesOrderDetail>();

    public void setSalesOrderId(Long salesOrderId) {
        this.salesOrderId = salesOrderId;
    }

    public Long getSalesOrderId() {
        return salesOrderId;
    }

    public void setSalesOrderNum(String salesOrderNum) {
        this.salesOrderNum = salesOrderNum;
    }

    public String getSalesOrderNum() {
        return salesOrderNum;
    }

    public void setDocDate(Date docDate) {
        this.docDate = docDate;
    }

    public Date getDocDate() {
        return docDate;
    }

    public void setDocSubject(String docSubject) {
        this.docSubject = docSubject;
    }

    public String getDocSubject() {
        return docSubject;
    }

    public void setSalesmenId(Long salesmenId) {
        this.salesmenId = salesmenId;
    }

    public Long getSalesmenId() {
        return salesmenId;
    }

    public void setDepartmentId(Long departmentId) {
        this.departmentId = departmentId;
    }

    public Long getDepartmentId() {
        return departmentId;
    }

    public void setContractNum(String contractNum) {
        this.contractNum = contractNum;
    }

    public String getContractNum() {
        return contractNum;
    }

    public void setCustomerId(Long customerId) {
        this.customerId = customerId;
    }

    public Long getCustomerId() {
        return customerId;
    }

    public void setContact(String contact) {
        this.contact = contact;
    }

    public String getContact() {
        return contact;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getPhone() {
        return phone;
    }

    public void setFax(String fax) {
        this.fax = fax;
    }

    public String getFax() {
        return fax;
    }

    public void setMobile(String mobile) {
        this.mobile = mobile;
    }

    public String getMobile() {
        return mobile;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getAddress() {
        return address;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getEmail() {
        return email;
    }

    public void setArea(String area) {
        this.area = area;
    }

    public String getArea() {
        return area;
    }

    public void setDeliveryDate(Date deliveryDate) {
        this.deliveryDate = deliveryDate;
    }

    public Date getDeliveryDate() {
        return deliveryDate;
    }

    public void setDeliveryPoint(String deliveryPoint) {
        this.deliveryPoint = deliveryPoint;
    }

    public String getDeliveryPoint() {
        return deliveryPoint;
    }

    public void setTransportModes(String transportModes) {
        this.transportModes = transportModes;
    }

    public String getTransportModes() {
        return transportModes;
    }

    public void setPaymentMethod(String paymentMethod) {
        this.paymentMethod = paymentMethod;
    }

    public String getPaymentMethod() {
        return paymentMethod;
    }

    public void setSettlementMethod(String settlementMethod) {
        this.settlementMethod = settlementMethod;
    }

    public String getSettlementMethod() {
        return settlementMethod;
    }

    public void setInvoicingMethod(String invoicingMethod) {
        this.invoicingMethod = invoicingMethod;
    }

    public String getInvoicingMethod() {
        return invoicingMethod;
    }

    public void setInvoiceType(String invoiceType) {
        this.invoiceType = invoiceType;
    }

    public String getInvoiceType() {
        return invoiceType;
    }

    public void setCurrency(String currency) {
        this.currency = currency;
    }

    public String getCurrency() {
        return currency;
    }

    public void setExchangeRate(BigDecimal  exchangeRate) {
        this.exchangeRate = exchangeRate;
    }

    public BigDecimal getExchangeRate() {
        return exchangeRate;
    }

    public void setSumSalesNum(Long sumSalesNum) {
        this.sumSalesNum = sumSalesNum;
    }

    public Long getSumSalesNum() {
        return sumSalesNum;
    }

    public void setSumTaxIncluded(BigDecimal sumTaxIncluded) {
        this.sumTaxIncluded = sumTaxIncluded;
    }

    public BigDecimal getSumTaxIncluded() {
        return sumTaxIncluded;
    }

    public void setDocStage(String docStage) {
        this.docStage = docStage;
    }

    public String getDocStage() {
        return docStage;
    }

    public void setDocState(String docState) {
        this.docState = docState;
    }

    public String getDocState() {
        return docState;
    }

    public void setDelFlag(String delFlag) {
        this.delFlag = delFlag;
    }

    public String getDelFlag() {
        return delFlag;
    }

    public String getAttachmentFile() {
        return attachmentFile;
    }

    public void setAttachmentFile(String attachmentFile) {
        this.attachmentFile = attachmentFile;
    }

    public List<PsiSalesOrderDetail> getPsiSalesOrderDetailList() {
        return psiSalesOrderDetailList;
    }

    public void setPsiSalesOrderDetailList(List<PsiSalesOrderDetail> psiSalesOrderDetailList) {
        this.psiSalesOrderDetailList = psiSalesOrderDetailList;
    }

    public List<PsiSalesContract> getPsiSalesContractList() {
        return psiSalesContractList;
    }

    public void setPsiSalesContractList(List<PsiSalesContract> psiSalesContractList) {
        this.psiSalesContractList = psiSalesContractList;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this, ToStringStyle.MULTI_LINE_STYLE)
                .append("salesOrderId", getSalesOrderId())
                .append("salesOrderNum", getSalesOrderNum())
                .append("docDate", getDocDate())
                .append("docSubject", getDocSubject())
                .append("salesmenId", getSalesmenId())
                .append("departmentId", getDepartmentId())
                .append("contractNum", getContractNum())
                .append("customerId", getCustomerId())
                .append("contact", getContact())
                .append("phone", getPhone())
                .append("fax", getFax())
                .append("mobile", getMobile())
                .append("address", getAddress())
                .append("email", getEmail())
                .append("area", getArea())
                .append("deliveryDate", getDeliveryDate())
                .append("deliveryPoint", getDeliveryPoint())
                .append("transportModes", getTransportModes())
                .append("paymentMethod", getPaymentMethod())
                .append("settlementMethod", getSettlementMethod())
                .append("invoicingMethod", getInvoicingMethod())
                .append("invoiceType", getInvoiceType())
                .append("currency", getCurrency())
                .append("exchangeRate", getExchangeRate())
                .append("sumSalesNum", getSumSalesNum())
                .append("sumTaxIncluded", getSumTaxIncluded())
                .append("docStage", getDocStage())
                .append("docState", getDocState())
                .append("delFlag", getDelFlag())
                .append("attachmentFile", getAttachmentFile())
                .append("remark", getRemark())
                .append("createBy", getCreateBy())
                .append("createTime", getCreateTime())
                .append("updateBy", getUpdateBy())
                .append("updateTime", getUpdateTime())
                .append("psiSalesOrderDetailList", getPsiSalesOrderDetailList())
                .append("psiSalesContractList", getPsiSalesContractList())
                .toString();
    }
}
