package com.ruoyi.purmgt.domain;

import java.math.BigDecimal;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import com.ruoyi.common.annotation.Excel;
import com.ruoyi.common.core.domain.BaseEntity;

/**
 * 采购订单明细对象 psi_purchase_order_detail
 * 
 * @author smes
 * @date 2023-06-17
 */
public class PsiPurchaseOrderDetail extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /** 采购订单明细ID */
    private Long orderDetailId;

    /** 采购订单ID */
    @Excel(name = "采购订单ID")
    private Long purchaseOrderId;

    /** 物料ID */
    @Excel(name = "物料ID")
    private Long materialId;

    /** 规格型号 */
    @Excel(name = "规格型号")
    private String spec;

    /** 库存单位 */
    @Excel(name = "库存单位")
    private String sku;

    /** 出库数量 */
    @Excel(name = "出库数量")
    private Long outboundQuantity;

    /** 销售单位 */
    @Excel(name = "销售单位")
    private String salesUnit;

    /** 销售数量 */
    @Excel(name = "销售数量")
    private Long salesNum;

    /** 税率% */
    @Excel(name = "税率%")
    private BigDecimal taxRate;

    /** 含税单价 */
    @Excel(name = "含税单价")
    private BigDecimal unitPrice;

    /** 折扣率% */
    @Excel(name = "折扣率%")
    private BigDecimal discountRate;

    /** 税额 */
    @Excel(name = "税额")
    private BigDecimal tax;

    /** 含税金额 */
    @Excel(name = "含税金额")
    private BigDecimal taxIncluded;

    /** 已入库数量 */
    @Excel(name = "已入库数量")
    private BigDecimal inWh;

    /** 未入库数量 */
    @Excel(name = "未入库数量")
    private BigDecimal notInWh;

    /** 删除标志（0代表存在 1代表删除） */
    private String delFlag;

    public void setOrderDetailId(Long orderDetailId) 
    {
        this.orderDetailId = orderDetailId;
    }

    public Long getOrderDetailId() 
    {
        return orderDetailId;
    }
    public void setPurchaseOrderId(Long purchaseOrderId) 
    {
        this.purchaseOrderId = purchaseOrderId;
    }

    public Long getPurchaseOrderId() 
    {
        return purchaseOrderId;
    }
    public void setMaterialId(Long materialId) 
    {
        this.materialId = materialId;
    }

    public Long getMaterialId() 
    {
        return materialId;
    }
    public void setSpec(String spec) 
    {
        this.spec = spec;
    }

    public String getSpec() 
    {
        return spec;
    }
    public void setSku(String sku) 
    {
        this.sku = sku;
    }

    public String getSku() 
    {
        return sku;
    }
    public void setOutboundQuantity(Long outboundQuantity) 
    {
        this.outboundQuantity = outboundQuantity;
    }

    public Long getOutboundQuantity() 
    {
        return outboundQuantity;
    }
    public void setSalesUnit(String salesUnit) 
    {
        this.salesUnit = salesUnit;
    }

    public String getSalesUnit() 
    {
        return salesUnit;
    }
    public void setSalesNum(Long salesNum) 
    {
        this.salesNum = salesNum;
    }

    public Long getSalesNum() 
    {
        return salesNum;
    }
    public void setTaxRate(BigDecimal taxRate) 
    {
        this.taxRate = taxRate;
    }

    public BigDecimal getTaxRate() 
    {
        return taxRate;
    }
    public void setUnitPrice(BigDecimal unitPrice) 
    {
        this.unitPrice = unitPrice;
    }

    public BigDecimal getUnitPrice() 
    {
        return unitPrice;
    }
    public void setDiscountRate(BigDecimal discountRate) 
    {
        this.discountRate = discountRate;
    }

    public BigDecimal getDiscountRate() 
    {
        return discountRate;
    }
    public void setTax(BigDecimal tax) 
    {
        this.tax = tax;
    }

    public BigDecimal getTax() 
    {
        return tax;
    }
    public void setTaxIncluded(BigDecimal taxIncluded) 
    {
        this.taxIncluded = taxIncluded;
    }

    public BigDecimal getTaxIncluded() 
    {
        return taxIncluded;
    }
    public void setInWh(BigDecimal inWh) 
    {
        this.inWh = inWh;
    }

    public BigDecimal getInWh() 
    {
        return inWh;
    }
    public void setNotInWh(BigDecimal notInWh) 
    {
        this.notInWh = notInWh;
    }

    public BigDecimal getNotInWh() 
    {
        return notInWh;
    }
    public void setDelFlag(String delFlag) 
    {
        this.delFlag = delFlag;
    }

    public String getDelFlag() 
    {
        return delFlag;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this,ToStringStyle.MULTI_LINE_STYLE)
            .append("orderDetailId", getOrderDetailId())
            .append("purchaseOrderId", getPurchaseOrderId())
            .append("materialId", getMaterialId())
            .append("spec", getSpec())
            .append("sku", getSku())
            .append("outboundQuantity", getOutboundQuantity())
            .append("salesUnit", getSalesUnit())
            .append("salesNum", getSalesNum())
            .append("taxRate", getTaxRate())
            .append("unitPrice", getUnitPrice())
            .append("discountRate", getDiscountRate())
            .append("tax", getTax())
            .append("taxIncluded", getTaxIncluded())
            .append("inWh", getInWh())
            .append("notInWh", getNotInWh())
            .append("delFlag", getDelFlag())
            .append("remark", getRemark())
            .append("createBy", getCreateBy())
            .append("createTime", getCreateTime())
            .append("updateBy", getUpdateBy())
            .append("updateTime", getUpdateTime())
            .toString();
    }
}
