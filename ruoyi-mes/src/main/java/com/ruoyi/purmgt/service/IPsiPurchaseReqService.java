package com.ruoyi.purmgt.service;

import java.util.List;
import com.ruoyi.purmgt.domain.PsiPurchaseReq;

/**
 * 请购单 Service接口
 * 
 * @author smes
 * @date 2023-06-19
 */
public interface IPsiPurchaseReqService 
{
    /**
     * 查询请购单 
     * 
     * @param id 请购单 主键
     * @return 请购单 
     */
    public PsiPurchaseReq selectPsiPurchaseReqById(Long id);

    /**
     * 查询请购单 列表
     * 
     * @param psiPurchaseReq 请购单 
     * @return 请购单 集合
     */
    public List<PsiPurchaseReq> selectPsiPurchaseReqList(PsiPurchaseReq psiPurchaseReq);

    /**
     * 新增请购单 
     * 
     * @param psiPurchaseReq 请购单 
     * @return 结果
     */
    public int insertPsiPurchaseReq(PsiPurchaseReq psiPurchaseReq);

    /**
     * 修改请购单 
     * 
     * @param psiPurchaseReq 请购单 
     * @return 结果
     */
    public int updatePsiPurchaseReq(PsiPurchaseReq psiPurchaseReq);

    /**
     * 批量删除请购单 
     * 
     * @param ids 需要删除的请购单 主键集合
     * @return 结果
     */
    public int deletePsiPurchaseReqByIds(Long[] ids);

    /**
     * 删除请购单 信息
     * 
     * @param id 请购单 主键
     * @return 结果
     */
    public int deletePsiPurchaseReqById(Long id);
}
