package com.ruoyi.basic.service;

import java.util.List;
import com.ruoyi.basic.domain.PsiSupplier;
import com.ruoyi.basic.domain.PsiSupplierCategory;

/**
 * 供应商Service接口
 * 
 * @author smes
 * @date 2023-06-06
 */
public interface IPsiSupplierService 
{
    /**
     * 查询供应商
     * 
     * @param id 供应商主键
     * @return 供应商
     */
    public PsiSupplier selectPsiSupplierById(Long id);

    /**
     * 查询供应商列表
     * 
     * @param psiSupplier 供应商
     * @return 供应商集合
     */
    public List<PsiSupplier> selectPsiSupplierList(PsiSupplier psiSupplier);

    /**
     * 新增供应商
     * 
     * @param psiSupplier 供应商
     * @return 结果
     */
    public int insertPsiSupplier(PsiSupplier psiSupplier);

    /**
     * 修改供应商
     * 
     * @param psiSupplier 供应商
     * @return 结果
     */
    public int updatePsiSupplier(PsiSupplier psiSupplier);

    /**
     * 批量删除供应商
     * 
     * @param ids 需要删除的供应商主键集合
     * @return 结果
     */
    public int deletePsiSupplierByIds(Long[] ids);

    /**
     * 删除供应商信息
     * 
     * @param id 供应商主键
     * @return 结果
     */
    public int deletePsiSupplierById(Long id);

    /**
     * 查重
     */
    public boolean isDuplicate(PsiSupplier entity);
}
