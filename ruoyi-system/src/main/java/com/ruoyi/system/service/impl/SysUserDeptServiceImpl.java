package com.ruoyi.system.service.impl;

import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.ruoyi.system.mapper.SysUserDeptMapper;
import com.ruoyi.system.domain.SysUserDept;
import com.ruoyi.system.service.ISysUserDeptService;

/**
 * 用户和部门关联Service业务层处理
 * 
 * @author smes
 * @date 2023-06-14
 */
@Service
public class SysUserDeptServiceImpl implements ISysUserDeptService 
{
    @Autowired
    private SysUserDeptMapper sysUserDeptMapper;

    /**
     * 查询用户和部门关联
     * 
     * @param userId 用户和部门关联主键
     * @return 用户和部门关联
     */
    @Override
    public SysUserDept selectSysUserDeptByUserId(Long userId)
    {
        return sysUserDeptMapper.selectSysUserDeptByUserId(userId);
    }

    @Override
    public List<Long> selectSysDeptIdsByUserId(Long userId)
    {

        return sysUserDeptMapper.selectSysDeptsByUserId(userId);
    }

    /**
     * 查询用户和部门关联列表
     * 
     * @param sysUserDept 用户和部门关联
     * @return 用户和部门关联
     */
    @Override
    public List<SysUserDept> selectSysUserDeptList(SysUserDept sysUserDept)
    {
        return sysUserDeptMapper.selectSysUserDeptList(sysUserDept);
    }

    /**
     * 新增用户和部门关联
     * 
     * @param sysUserDept 用户和部门关联
     * @return 结果
     */
    @Override
    public int insertSysUserDept(SysUserDept sysUserDept)
    {
        return sysUserDeptMapper.insertSysUserDept(sysUserDept);
    }

    /**
     * 修改用户和部门关联
     * 
     * @param sysUserDept 用户和部门关联
     * @return 结果
     */
    @Override
    public int updateSysUserDept(SysUserDept sysUserDept)
    {
        return sysUserDeptMapper.updateSysUserDept(sysUserDept);
    }

    /**
     * 批量删除用户和部门关联
     * 
     * @param userIds 需要删除的用户和部门关联主键
     * @return 结果
     */
    @Override
    public int deleteSysUserDeptByUserIds(Long[] userIds)
    {
        return sysUserDeptMapper.deleteSysUserDeptByUserIds(userIds);
    }

    /**
     * 删除用户和部门关联信息
     * 
     * @param userId 用户和部门关联主键
     * @return 结果
     */
    @Override
    public int deleteSysUserDeptByUserId(Long userId)
    {
        return sysUserDeptMapper.deleteSysUserDeptByUserId(userId);
    }
}
