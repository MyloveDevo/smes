import request from '@/utils/request'

// 查询请购单 列表
export function listPr(query) {
  return request({
    url: '/purmgt/pr/list',
    method: 'get',
    params: query
  })
}

// 查询请购单 详细
export function getPr(id) {
  return request({
    url: '/purmgt/pr/' + id,
    method: 'get'
  })
}

// 新增请购单 
export function addPr(data) {
  return request({
    url: '/purmgt/pr',
    method: 'post',
    data: data
  })
}

// 修改请购单 
export function updatePr(data) {
  return request({
    url: '/purmgt/pr',
    method: 'put',
    data: data
  })
}

// 删除请购单 
export function delPr(id) {
  return request({
    url: '/purmgt/pr/' + id,
    method: 'delete'
  })
}
