package com.ruoyi.basic.mapper;

import java.util.List;
import com.ruoyi.basic.domain.PsiGoods;
import com.ruoyi.basic.domain.PsiGoodsCategory;

/**
 * 产品物料Mapper接口
 * 
 * @author smes
 * @date 2023-06-05
 */
public interface PsiGoodsMapper 
{
    /**
     * 查询产品物料
     * 
     * @param id 产品物料主键
     * @return 产品物料
     */
    public PsiGoods selectPsiGoodsById(Long id);

    /**
     * 查询产品物料列表
     * 
     * @param psiGoods 产品物料
     * @return 产品物料集合
     */
    public List<PsiGoods> selectPsiGoodsList(PsiGoods psiGoods);

    /**
     * 新增产品物料
     * 
     * @param psiGoods 产品物料
     * @return 结果
     */
    public int insertPsiGoods(PsiGoods psiGoods);

    /**
     * 修改产品物料
     * 
     * @param psiGoods 产品物料
     * @return 结果
     */
    public int updatePsiGoods(PsiGoods psiGoods);

    /**
     * 删除产品物料
     * 
     * @param id 产品物料主键
     * @return 结果
     */
    public int deletePsiGoodsById(Long id);

    /**
     * 批量删除产品物料
     * 
     * @param ids 需要删除的数据主键集合
     * @return 结果
     */
    public int deletePsiGoodsByIds(Long[] ids);

    List<PsiGoods> findOneByCondition(PsiGoods entiry);
}
