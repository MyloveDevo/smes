package com.ruoyi.web.controller.system;

import java.util.List;
import javax.servlet.http.HttpServletResponse;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.ruoyi.common.annotation.Log;
import com.ruoyi.common.core.controller.BaseController;
import com.ruoyi.common.core.domain.AjaxResult;
import com.ruoyi.common.enums.BusinessType;
import com.ruoyi.system.domain.SysUserDept;
import com.ruoyi.system.service.ISysUserDeptService;
import com.ruoyi.common.utils.poi.ExcelUtil;
import com.ruoyi.common.core.page.TableDataInfo;

/**
 * 用户和部门关联Controller
 * 
 * @author smes
 * @date 2023-06-14
 */
@RestController
@RequestMapping("/system/userdept")
public class SysUserDeptController extends BaseController
{
    @Autowired
    private ISysUserDeptService sysUserDeptService;

    /**
     * 查询用户和部门关联列表
     */
    @PreAuthorize("@ss.hasPermi('system:userdept:list')")
    @GetMapping("/list")
    public TableDataInfo list(SysUserDept sysUserDept)
    {
        startPage();
        List<SysUserDept> list = sysUserDeptService.selectSysUserDeptList(sysUserDept);
        return getDataTable(list);
    }

    /**
     * 导出用户和部门关联列表
     */
    @PreAuthorize("@ss.hasPermi('system:userdept:export')")
    @Log(title = "用户和部门关联", businessType = BusinessType.EXPORT)
    @PostMapping("/export")
    public void export(HttpServletResponse response, SysUserDept sysUserDept)
    {
        List<SysUserDept> list = sysUserDeptService.selectSysUserDeptList(sysUserDept);
        ExcelUtil<SysUserDept> util = new ExcelUtil<SysUserDept>(SysUserDept.class);
        util.exportExcel(response, list, "用户和部门关联数据");
    }

    /**
     * 获取用户和部门关联详细信息
     */
    @PreAuthorize("@ss.hasPermi('system:userdept:query')")
    @GetMapping(value = "/{userId}")
    public AjaxResult getInfo(@PathVariable("userId") Long userId)
    {
        return AjaxResult.success(sysUserDeptService.selectSysUserDeptByUserId(userId));
    }

    /**
     * 新增用户和部门关联
     */
    @PreAuthorize("@ss.hasPermi('system:userdept:add')")
    @Log(title = "用户和部门关联", businessType = BusinessType.INSERT)
    @PostMapping
    public AjaxResult add(@RequestBody SysUserDept sysUserDept)
    {
        return toAjax(sysUserDeptService.insertSysUserDept(sysUserDept));
    }

    /**
     * 修改用户和部门关联
     */
    @PreAuthorize("@ss.hasPermi('system:userdept:edit')")
    @Log(title = "用户和部门关联", businessType = BusinessType.UPDATE)
    @PutMapping
    public AjaxResult edit(@RequestBody SysUserDept sysUserDept)
    {
        return toAjax(sysUserDeptService.updateSysUserDept(sysUserDept));
    }

    /**
     * 删除用户和部门关联
     */
    @PreAuthorize("@ss.hasPermi('system:userdept:remove')")
    @Log(title = "用户和部门关联", businessType = BusinessType.DELETE)
	@DeleteMapping("/{userIds}")
    public AjaxResult remove(@PathVariable Long[] userIds)
    {
        return toAjax(sysUserDeptService.deleteSysUserDeptByUserIds(userIds));
    }
}
