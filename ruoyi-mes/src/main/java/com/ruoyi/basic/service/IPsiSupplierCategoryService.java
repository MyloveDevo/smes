package com.ruoyi.basic.service;

import java.util.List;
import com.ruoyi.basic.domain.PsiSupplierCategory;
import com.ruoyi.basic.domain.PsiWarehouse;

/**
 * 供应商分类Service接口
 * 
 * @author smes
 * @date 2023-06-04
 */
public interface IPsiSupplierCategoryService 
{
    /**
     * 查询供应商分类
     * 
     * @param id 供应商分类主键
     * @return 供应商分类
     */
    public PsiSupplierCategory selectPsiSupplierCategoryById(Long id);

    /**
     * 查询供应商分类列表
     * 
     * @param psiSupplierCategory 供应商分类
     * @return 供应商分类集合
     */
    public List<PsiSupplierCategory> selectPsiSupplierCategoryList(PsiSupplierCategory psiSupplierCategory);

    /**
     * 新增供应商分类
     * 
     * @param psiSupplierCategory 供应商分类
     * @return 结果
     */
    public int insertPsiSupplierCategory(PsiSupplierCategory psiSupplierCategory);

    /**
     * 修改供应商分类
     * 
     * @param psiSupplierCategory 供应商分类
     * @return 结果
     */
    public int updatePsiSupplierCategory(PsiSupplierCategory psiSupplierCategory);

    /**
     * 批量删除供应商分类
     * 
     * @param ids 需要删除的供应商分类主键集合
     * @return 结果
     */
    public int deletePsiSupplierCategoryByIds(Long[] ids);

    /**
     * 删除供应商分类信息
     * 
     * @param id 供应商分类主键
     * @return 结果
     */
    public int deletePsiSupplierCategoryById(Long id);

    /**
     * 查重
     */
    public boolean isDuplicate(PsiSupplierCategory psiSupplierCategory);
}
