package com.ruoyi.prdmgt.service.impl;

import java.util.List;
import com.ruoyi.common.utils.DateUtils;
import com.ruoyi.common.utils.SecurityUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.ruoyi.prdmgt.mapper.PsiProjectMapper;
import com.ruoyi.prdmgt.domain.PsiProject;
import com.ruoyi.prdmgt.service.IPsiProjectService;

/**
 * 项目管理Service业务层处理
 * 
 * @author smes
 * @date 2023-06-11
 */
@Service
public class PsiProjectServiceImpl implements IPsiProjectService 
{
    @Autowired
    private PsiProjectMapper psiProjectMapper;

    /**
     * 查询项目管理
     * 
     * @param id 项目管理主键
     * @return 项目管理
     */
    @Override
    public PsiProject selectPsiProjectById(String id)
    {
        return psiProjectMapper.selectPsiProjectById(id);
    }

    /**
     * 查询项目管理列表
     * 
     * @param psiProject 项目管理
     * @return 项目管理
     */
    @Override
    public List<PsiProject> selectPsiProjectList(PsiProject psiProject)
    {
        psiProject.setDelFlag("0");
        return psiProjectMapper.selectPsiProjectList(psiProject);
    }

    /**
     * 新增项目管理
     * 
     * @param psiProject 项目管理
     * @return 结果
     */
    @Override
    public PsiProject insertPsiProject(PsiProject psiProject)
    {
        psiProject.setDelFlag("0");
        psiProject.setCreateBy(SecurityUtils.getUsername());
        psiProject.setCreateId(SecurityUtils.getUserId());
        psiProject.setCreateTime(DateUtils.getNowDate());
        psiProjectMapper.insertPsiProject(psiProject);
        return psiProject;
    }

    /**
     * 修改项目管理
     * 
     * @param psiProject 项目管理
     * @return 结果
     */
    @Override
    public int updatePsiProject(PsiProject psiProject)
    {
        psiProject.setUpdateTime(DateUtils.getNowDate());
        return psiProjectMapper.updatePsiProject(psiProject);
    }

    /**
     * 批量删除项目管理
     * 
     * @param ids 需要删除的项目管理主键
     * @return 结果
     */
    @Override
    public int deletePsiProjectByIds(String[] ids)
    {
        return psiProjectMapper.deletePsiProjectByIds(ids);
    }

    /**
     * 删除项目管理信息
     * 
     * @param id 项目管理主键
     * @return 结果
     */
    @Override
    public int deletePsiProjectById(String id)
    {
        return psiProjectMapper.deletePsiProjectById(id);
    }
}
