package com.ruoyi.basic.service.impl;

import java.util.List;

import com.ruoyi.basic.domain.PsiGoodsCategory;
import com.ruoyi.common.utils.DateUtils;
import com.ruoyi.common.utils.SecurityUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.ruoyi.basic.mapper.PsiCustomerCategoryMapper;
import com.ruoyi.basic.domain.PsiCustomerCategory;
import com.ruoyi.basic.service.IPsiCustomerCategoryService;

/**
 * 客户分类Service业务层处理
 * 
 * @author smes
 * @date 2023-06-02
 */
@Service
public class PsiCustomerCategoryServiceImpl implements IPsiCustomerCategoryService 
{
    @Autowired
    private PsiCustomerCategoryMapper psiCustomerCategoryMapper;

    /**
     * 查询客户分类
     * 
     * @param id 客户分类主键
     * @return 客户分类
     */
    @Override
    public PsiCustomerCategory selectPsiCustomerCategoryById(Long id)
    {
        return psiCustomerCategoryMapper.selectPsiCustomerCategoryById(id);
    }

    /**
     * 查询客户分类列表
     * 
     * @param psiCustomerCategory 客户分类
     * @return 客户分类
     */
    @Override
    public List<PsiCustomerCategory> selectPsiCustomerCategoryList(PsiCustomerCategory psiCustomerCategory)
    {
        return psiCustomerCategoryMapper.selectPsiCustomerCategoryList(psiCustomerCategory);
    }

    /**
     * 新增客户分类
     * 
     * @param psiCustomerCategory 客户分类
     * @return 结果
     */
    @Override
    public int insertPsiCustomerCategory(PsiCustomerCategory psiCustomerCategory)
    {
        psiCustomerCategory.setCreateTime(DateUtils.getNowDate());
        psiCustomerCategory.setCreateBy(SecurityUtils.getUsername());
        psiCustomerCategory.setDelFlag("0");
        return psiCustomerCategoryMapper.insertPsiCustomerCategory(psiCustomerCategory);
    }

    /**
     * 修改客户分类
     * 
     * @param psiCustomerCategory 客户分类
     * @return 结果
     */
    @Override
    public int updatePsiCustomerCategory(PsiCustomerCategory psiCustomerCategory)
    {
        psiCustomerCategory.setUpdateTime(DateUtils.getNowDate());
        psiCustomerCategory.setUpdateBy(SecurityUtils.getUsername());
        return psiCustomerCategoryMapper.updatePsiCustomerCategory(psiCustomerCategory);
    }

    /**
     * 批量删除客户分类
     * 
     * @param ids 需要删除的客户分类主键
     * @return 结果
     */
    @Override
    public int deletePsiCustomerCategoryByIds(Long[] ids)
    {
        return psiCustomerCategoryMapper.deletePsiCustomerCategoryByIds(ids);
    }

    /**
     * 删除客户分类信息
     * 
     * @param id 客户分类主键
     * @return 结果
     */
    @Override
    public int deletePsiCustomerCategoryById(Long id)
    {
        return psiCustomerCategoryMapper.deletePsiCustomerCategoryById(id);
    }

    /**
     * 查重
     */
    @Override
    public boolean isDuplicate(PsiCustomerCategory entity) {
        List<PsiCustomerCategory> lst = psiCustomerCategoryMapper.findOneByCondition(entity);
        return lst.size()>0;
    }
}
