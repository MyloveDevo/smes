package com.ruoyi.prdmgt.service;

import java.util.List;
import com.ruoyi.prdmgt.domain.PsiProjectBom;

/**
 * 项目BOMService接口
 * 
 * @author smes
 * @date 2023-06-12
 */
public interface IPsiProjectBomService 
{
    /**
     * 查询项目BOM
     * 
     * @param id 项目BOM主键
     * @return 项目BOM
     */
    public PsiProjectBom selectPsiProjectBomById(Long id);

    /**
     * 查询项目BOM列表
     * 
     * @param psiProjectBom 项目BOM
     * @return 项目BOM集合
     */
    public List<PsiProjectBom> selectPsiProjectBomList(PsiProjectBom psiProjectBom);

    /**
     * 新增项目BOM
     * 
     * @param psiProjectBom 项目BOM
     * @return 结果
     */
    public int insertPsiProjectBom(PsiProjectBom psiProjectBom);

    /**
     * 修改项目BOM
     * 
     * @param psiProjectBom 项目BOM
     * @return 结果
     */
    public int updatePsiProjectBom(PsiProjectBom psiProjectBom);

    /**
     * 批量删除项目BOM
     * 
     * @param ids 需要删除的项目BOM主键集合
     * @return 结果
     */
    public int deletePsiProjectBomByIds(Long[] ids);

    /**
     * 删除项目BOM信息
     * 
     * @param id 项目BOM主键
     * @return 结果
     */
    public int deletePsiProjectBomById(Long id);

    /**
     * 新增项目BOM
     *
     * @param psiProjectBom 项目BOM
     * @return 结果 包含PK
     */
    public PsiProjectBom addPsiProjectBom(PsiProjectBom psiProjectBom);
}
