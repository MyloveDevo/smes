package com.ruoyi.basic.domain;

import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import com.ruoyi.common.annotation.Excel;
import com.ruoyi.common.core.domain.BaseEntity;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

/**
 * 客户分类对象 psi_customer_category
 * 
 * @author smes
 * @date 2023-06-02
 */
public class PsiCustomerCategory extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /** PK */
    private Long id;

    /** 名称 */
    @Excel(name = "名称")
    @Size(min = 0, max = 10, message = "名称长度不能超过10个字符")
    //@Email(message = "邮箱格式不正确")
    //@NotBlank(message = "用户账号不能为空")
    private String name;

    /** 编号 */
    @Excel(name = "编号")
    @Size(min = 0, max = 10, message = "编号长度不能超过10个字符")
    private String code;

    /** 父ID */
    @Excel(name = "父ID")
    private String parentId;

    /** 排序码 */
    @Excel(name = "排序码")
    private Long showOrder;

    /** 储备1 */
    @Excel(name = "储备1")
    @Size(min = 0, max = 10, message = "储备1长度不能超过10个字符")
    private String reserve1;

    /** 储备2 */
    @Excel(name = "储备2")
    @Size(min = 0, max = 10, message = "储备2长度不能超过10个字符")
    private String reserve2;

    /** 储备3 */
    @Excel(name = "储备3")
    @Size(min = 0, max = 10, message = "储备3长度不能超过10个字符")
    private String reserve3;

    /** 删除标志（0代表存在 2代表删除） */
    private String delFlag;

    public void setId(Long id) 
    {
        this.id = id;
    }

    public Long getId() 
    {
        return id;
    }
    public void setName(String name) 
    {
        this.name = name;
    }

    public String getName() 
    {
        return name;
    }
    public void setCode(String code) 
    {
        this.code = code;
    }

    public String getCode() 
    {
        return code;
    }
    public void setParentId(String parentId) 
    {
        this.parentId = parentId;
    }

    public String getParentId() 
    {
        return parentId;
    }
    public void setShowOrder(Long showOrder) 
    {
        this.showOrder = showOrder;
    }

    public Long getShowOrder() 
    {
        return showOrder;
    }
    public void setReserve1(String reserve1) 
    {
        this.reserve1 = reserve1;
    }

    public String getReserve1() 
    {
        return reserve1;
    }
    public void setReserve2(String reserve2) 
    {
        this.reserve2 = reserve2;
    }

    public String getReserve2() 
    {
        return reserve2;
    }
    public void setReserve3(String reserve3) 
    {
        this.reserve3 = reserve3;
    }

    public String getReserve3() 
    {
        return reserve3;
    }
    public void setDelFlag(String delFlag) 
    {
        this.delFlag = delFlag;
    }

    public String getDelFlag() 
    {
        return delFlag;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this,ToStringStyle.MULTI_LINE_STYLE)
            .append("id", getId())
            .append("name", getName())
            .append("code", getCode())
            .append("parentId", getParentId())
            .append("showOrder", getShowOrder())
            .append("reserve1", getReserve1())
            .append("reserve2", getReserve2())
            .append("reserve3", getReserve3())
            .append("delFlag", getDelFlag())
            .append("remark", getRemark())
            .append("createBy", getCreateBy())
            .append("createTime", getCreateTime())
            .append("updateBy", getUpdateBy())
            .append("updateTime", getUpdateTime())
            .toString();
    }
}
