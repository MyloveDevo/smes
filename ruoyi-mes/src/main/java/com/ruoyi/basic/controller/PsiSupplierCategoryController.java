package com.ruoyi.basic.controller;

import java.util.List;
import javax.servlet.http.HttpServletResponse;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.ruoyi.common.annotation.Log;
import com.ruoyi.common.core.controller.BaseController;
import com.ruoyi.common.core.domain.AjaxResult;
import com.ruoyi.common.enums.BusinessType;
import com.ruoyi.basic.domain.PsiSupplierCategory;
import com.ruoyi.basic.service.IPsiSupplierCategoryService;
import com.ruoyi.common.utils.poi.ExcelUtil;
import com.ruoyi.common.core.page.TableDataInfo;

/**
 * 供应商分类Controller
 * 
 * @author smes
 * @date 2023-06-04
 */
@RestController
@RequestMapping("/basic/supplierctg")
public class PsiSupplierCategoryController extends BaseController
{
    @Autowired
    private IPsiSupplierCategoryService psiSupplierCategoryService;

    /**
     * 查询供应商分类列表
     */
    @PreAuthorize("@ss.hasPermi('basic:supplierctg:list')")
    @GetMapping("/list")
    public TableDataInfo list(PsiSupplierCategory psiSupplierCategory)
    {
        startPage();
        psiSupplierCategory.setDelFlag("0");
        List<PsiSupplierCategory> list = psiSupplierCategoryService.selectPsiSupplierCategoryList(psiSupplierCategory);
        return getDataTable(list);
    }

    /**
     * 查询供应商分类列表
     * 不分页
     */
    @GetMapping("/dropdownList")
    public AjaxResult dropdownList()
    {
        PsiSupplierCategory psiSupplierCategory = new PsiSupplierCategory();
        psiSupplierCategory.setDelFlag("0");
        List<PsiSupplierCategory> list = psiSupplierCategoryService.selectPsiSupplierCategoryList(psiSupplierCategory);
        return AjaxResult.success(list);
    }

    /**
     * 导出供应商分类列表
     */
    @PreAuthorize("@ss.hasPermi('basic:supplierctg:export')")
    @Log(title = "供应商分类", businessType = BusinessType.EXPORT)
    @PostMapping("/export")
    public void export(HttpServletResponse response, PsiSupplierCategory psiSupplierCategory)
    {
        psiSupplierCategory.setDelFlag("0");
        List<PsiSupplierCategory> list = psiSupplierCategoryService.selectPsiSupplierCategoryList(psiSupplierCategory);
        ExcelUtil<PsiSupplierCategory> util = new ExcelUtil<PsiSupplierCategory>(PsiSupplierCategory.class);
        util.exportExcel(response, list, "供应商分类数据");
    }

    /**
     * 获取供应商分类详细信息
     */
    @PreAuthorize("@ss.hasPermi('basic:supplierctg:query')")
    @GetMapping(value = "/{id}")
    public AjaxResult getInfo(@PathVariable("id") Long id)
    {
        return AjaxResult.success(psiSupplierCategoryService.selectPsiSupplierCategoryById(id));
    }

    /**
     * 新增供应商分类
     */
    @PreAuthorize("@ss.hasPermi('basic:supplierctg:add')")
    @Log(title = "供应商分类", businessType = BusinessType.INSERT)
    @PostMapping
    public AjaxResult add(@Validated @RequestBody PsiSupplierCategory psiSupplierCategory)
    {
        if (psiSupplierCategoryService.isDuplicate(psiSupplierCategory)== false){
            return toAjax(psiSupplierCategoryService.insertPsiSupplierCategory(psiSupplierCategory));
        }else{
            return AjaxResult.error("名称和代码重复。");
        }
    }

    /**
     * 修改供应商分类
     */
    @PreAuthorize("@ss.hasPermi('basic:supplierctg:edit')")
    @Log(title = "供应商分类", businessType = BusinessType.UPDATE)
    @PutMapping
    public AjaxResult edit(@Validated @RequestBody PsiSupplierCategory psiSupplierCategory)
    {
        if (psiSupplierCategoryService.isDuplicate(psiSupplierCategory)== false){
            return toAjax(psiSupplierCategoryService.updatePsiSupplierCategory(psiSupplierCategory));
        }else{
            return AjaxResult.error("名称和代码重复。");
        }
    }

    /**
     * 删除供应商分类
     */
    @PreAuthorize("@ss.hasPermi('basic:supplierctg:remove')")
    @Log(title = "供应商分类", businessType = BusinessType.DELETE)
	@DeleteMapping("/{ids}")
    public AjaxResult remove(@PathVariable Long[] ids)
    {
        return toAjax(psiSupplierCategoryService.deletePsiSupplierCategoryByIds(ids));
    }
}
