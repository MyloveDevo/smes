package com.ruoyi.basic.controller;

import java.util.List;
import javax.servlet.http.HttpServletResponse;

import com.ruoyi.basic.domain.PsiGoods;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.ruoyi.common.annotation.Log;
import com.ruoyi.common.core.controller.BaseController;
import com.ruoyi.common.core.domain.AjaxResult;
import com.ruoyi.common.enums.BusinessType;
import com.ruoyi.basic.domain.PsiGoodsCategory;
import com.ruoyi.basic.service.IPsiGoodsCategoryService;
import com.ruoyi.common.utils.poi.ExcelUtil;
import com.ruoyi.common.core.page.TableDataInfo;

/**
 * 产品物料分类Controller
 * 
 * @author smes
 * @date 2023-06-04
 */
@RestController
@RequestMapping("/basic/goodsctg")
public class PsiGoodsCategoryController extends BaseController
{
    @Autowired
    private IPsiGoodsCategoryService psiGoodsCategoryService;

    /**
     * 查询产品物料分类列表
     */
    @PreAuthorize("@ss.hasPermi('basic:goodsctg:list')")
    @GetMapping("/list")
    public TableDataInfo list(PsiGoodsCategory psiGoodsCategory)
    {
        startPage();
        psiGoodsCategory.setDelFlag("0");
        List<PsiGoodsCategory> list = psiGoodsCategoryService.selectPsiGoodsCategoryList(psiGoodsCategory);
        return getDataTable(list);
    }

    /**
     * 导出产品物料分类列表
     */
    @PreAuthorize("@ss.hasPermi('basic:goodsctg:export')")
    @Log(title = "产品物料分类", businessType = BusinessType.EXPORT)
    @PostMapping("/export")
    public void export(HttpServletResponse response, PsiGoodsCategory psiGoodsCategory)
    {
        psiGoodsCategory.setDelFlag("0");
        List<PsiGoodsCategory> list = psiGoodsCategoryService.selectPsiGoodsCategoryList(psiGoodsCategory);
        ExcelUtil<PsiGoodsCategory> util = new ExcelUtil<PsiGoodsCategory>(PsiGoodsCategory.class);
        util.exportExcel(response, list, "产品物料分类数据");
    }

    /**
     * 获取产品物料分类详细信息
     */
    @PreAuthorize("@ss.hasPermi('basic:goodsctg:query')")
    @GetMapping(value = "/{id}")
    public AjaxResult getInfo(@PathVariable("id") Long id)
    {
        return AjaxResult.success(psiGoodsCategoryService.selectPsiGoodsCategoryById(id));
    }

    /**
     * 新增产品物料分类
     */
    @PreAuthorize("@ss.hasPermi('basic:goodsctg:add')")
    @Log(title = "产品物料分类", businessType = BusinessType.INSERT)
    @PostMapping
    public AjaxResult add(@Validated @RequestBody PsiGoodsCategory psiGoodsCategory)
    {
        if (psiGoodsCategoryService.isDuplicate(psiGoodsCategory)== false){
            return toAjax(psiGoodsCategoryService.insertPsiGoodsCategory(psiGoodsCategory));
        }else{
            return AjaxResult.error("名称和代码重复。");
        }
    }

    /**
     * 修改产品物料分类
     */
    @PreAuthorize("@ss.hasPermi('basic:goodsctg:edit')")
    @Log(title = "产品物料分类", businessType = BusinessType.UPDATE)
    @PutMapping
    public AjaxResult edit(@Validated @RequestBody PsiGoodsCategory psiGoodsCategory)
    {
        if (psiGoodsCategoryService.isDuplicate(psiGoodsCategory)== false){
            return toAjax(psiGoodsCategoryService.updatePsiGoodsCategory(psiGoodsCategory));
        }else{
            return AjaxResult.error("名称和代码重复。");
        }
    }

    /**
     * 删除产品物料分类
     */
    @PreAuthorize("@ss.hasPermi('basic:goodsctg:remove')")
    @Log(title = "产品物料分类", businessType = BusinessType.DELETE)
	@DeleteMapping("/{ids}")
    public AjaxResult remove(@PathVariable Long[] ids)
    {
        return toAjax(psiGoodsCategoryService.deletePsiGoodsCategoryByIds(ids));
    }

    /**
     * 查询产品物料分类列表
     * 不分页
     */
    @GetMapping("/dropdownList")
    public AjaxResult dropdownList()
    {
        PsiGoodsCategory entity = new PsiGoodsCategory();
        entity.setDelFlag("0");
        List<PsiGoodsCategory> list = psiGoodsCategoryService.selectPsiGoodsCategoryList(entity);
        return AjaxResult.success(list);
    }
}
