import request from '@/utils/request'

// 查询BOM列表
export function listBom(query) {
  return request({
    url: '/basic/bom/list',
    method: 'get',
    params: query
  })
}

// 查询BOM详细
export function getBom(id) {
  return request({
    url: '/basic/bom/' + id,
    method: 'get'
  })
}

// 新增BOM
export function addBom(data) {
  return request({
    url: '/basic/bom/insert',
    method: 'post',
    data: data
  })
}

// 修改BOM
export function updateBom(data) {
  return request({
    url: '/basic/bom',
    method: 'put',
    data: data
  })
}

// 删除BOM
export function delBom(id) {
  return request({
    url: '/basic/bom/' + id,
    method: 'delete'
  })
}

// BOM树
export function queryTree(id) {
  return request({
    url: '/basic/bom/tree/' + id,
    method: 'get'
  })
}

