package com.ruoyi.basic.controller;

import java.util.List;
import javax.servlet.http.HttpServletResponse;

import com.github.pagehelper.Page;
import com.github.pagehelper.PageHelper;
import com.ruoyi.basic.domain.PsiGoodsCategory;
import com.ruoyi.common.core.page.PageDomain;
import com.ruoyi.common.core.page.TableSupport;
import com.ruoyi.common.utils.PageUtils;
import com.ruoyi.common.utils.StringUtils;
import com.ruoyi.common.utils.sql.SqlUtil;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.ruoyi.common.annotation.Log;
import com.ruoyi.common.core.controller.BaseController;
import com.ruoyi.common.core.domain.AjaxResult;
import com.ruoyi.common.enums.BusinessType;
import com.ruoyi.basic.domain.PsiCustomerCategory;
import com.ruoyi.basic.service.IPsiCustomerCategoryService;
import com.ruoyi.common.utils.poi.ExcelUtil;
import com.ruoyi.common.core.page.TableDataInfo;

/**
 * 客户分类Controller
 * 
 * @author smes
 * @date 2023-06-02
 */
@RestController
@RequestMapping("/basic/customerctg")
public class PsiCustomerCategoryController extends BaseController
{
    @Autowired
    private IPsiCustomerCategoryService psiCustomerCategoryService;

    /**
     * 查询客户分类列表
     */
    @PreAuthorize("@ss.hasPermi('basic:customerctg:list')")
    @GetMapping("/list")
    public TableDataInfo list(PsiCustomerCategory psiCustomerCategory)
    {
        startPage();
        psiCustomerCategory.setDelFlag("0");
        List<PsiCustomerCategory> list = psiCustomerCategoryService.selectPsiCustomerCategoryList(psiCustomerCategory);
        return getDataTable(list);
    }

    /**
     * 导出客户分类列表
     */
    @PreAuthorize("@ss.hasPermi('basic:customerctg:export')")
    @Log(title = "客户分类", businessType = BusinessType.EXPORT)
    @PostMapping("/export")
    public void export(HttpServletResponse response, PsiCustomerCategory psiCustomerCategory)
    {
        psiCustomerCategory.setDelFlag("0");
        List<PsiCustomerCategory> list = psiCustomerCategoryService.selectPsiCustomerCategoryList(psiCustomerCategory);
        ExcelUtil<PsiCustomerCategory> util = new ExcelUtil<PsiCustomerCategory>(PsiCustomerCategory.class);
        util.exportExcel(response, list, "客户分类数据");
    }

    /**
     * 获取客户分类详细信息
     */
    @PreAuthorize("@ss.hasPermi('basic:customerctg:query')")
    @GetMapping(value = "/{id}")
    public AjaxResult getInfo(@PathVariable("id") Long id)
    {
        return AjaxResult.success(psiCustomerCategoryService.selectPsiCustomerCategoryById(id));
    }

    /**
     * 新增客户分类
     */
    @PreAuthorize("@ss.hasPermi('basic:customerctg:add')")
    @Log(title = "客户分类", businessType = BusinessType.INSERT)
    @PostMapping
    public AjaxResult add(@Validated @RequestBody PsiCustomerCategory psiCustomerCategory)
    {
        if (psiCustomerCategoryService.isDuplicate(psiCustomerCategory)== false){
            return toAjax(psiCustomerCategoryService.insertPsiCustomerCategory(psiCustomerCategory));
        }else{
            return AjaxResult.error("名称和代码重复。");
        }

    }

    /**
     * 修改客户分类
     */
    @PreAuthorize("@ss.hasPermi('basic:customerctg:edit')")
    @Log(title = "客户分类", businessType = BusinessType.UPDATE)
    @PutMapping
    public AjaxResult edit(@Validated @RequestBody PsiCustomerCategory psiCustomerCategory)
    {
        if (psiCustomerCategoryService.isDuplicate(psiCustomerCategory)== false){
            return toAjax(psiCustomerCategoryService.updatePsiCustomerCategory(psiCustomerCategory));
        }else{
            return AjaxResult.error("名称和代码重复。");
        }

    }

    /**
     * 删除客户分类
     */
    @PreAuthorize("@ss.hasPermi('basic:customerctg:remove')")
    @Log(title = "客户分类", businessType = BusinessType.DELETE)
	@DeleteMapping("/{ids}")
    public AjaxResult remove(@PathVariable Long[] ids)
    {
        return toAjax(psiCustomerCategoryService.deletePsiCustomerCategoryByIds(ids));
    }

    /**
     * 查询客户分类列表
     * 不分页
     */
    @GetMapping("/dropdownList")
    public AjaxResult dropdownList()
    {
        PsiCustomerCategory entity = new PsiCustomerCategory();
        entity.setDelFlag("0");
        List<PsiCustomerCategory> list = psiCustomerCategoryService.selectPsiCustomerCategoryList(entity);
        return AjaxResult.success(list);
    }
}
