package com.ruoyi.basic.mapper;

import java.util.List;
import com.ruoyi.basic.domain.PsiCustomerCategory;
import com.ruoyi.basic.domain.PsiGoodsCategory;

/**
 * 客户分类Mapper接口
 * 
 * @author smes
 * @date 2023-06-02
 */
public interface PsiCustomerCategoryMapper 
{
    /**
     * 查询客户分类
     * 
     * @param id 客户分类主键
     * @return 客户分类
     */
    public PsiCustomerCategory selectPsiCustomerCategoryById(Long id);

    /**
     * 查询客户分类列表
     * 
     * @param psiCustomerCategory 客户分类
     * @return 客户分类集合
     */
    public List<PsiCustomerCategory> selectPsiCustomerCategoryList(PsiCustomerCategory psiCustomerCategory);

    /**
     * 新增客户分类
     * 
     * @param psiCustomerCategory 客户分类
     * @return 结果
     */
    public int insertPsiCustomerCategory(PsiCustomerCategory psiCustomerCategory);

    /**
     * 修改客户分类
     * 
     * @param psiCustomerCategory 客户分类
     * @return 结果
     */
    public int updatePsiCustomerCategory(PsiCustomerCategory psiCustomerCategory);

    /**
     * 删除客户分类
     * 
     * @param id 客户分类主键
     * @return 结果
     */
    public int deletePsiCustomerCategoryById(Long id);

    /**
     * 批量删除客户分类
     * 
     * @param ids 需要删除的数据主键集合
     * @return 结果
     */
    public int deletePsiCustomerCategoryByIds(Long[] ids);

    List<PsiCustomerCategory> findOneByCondition(PsiCustomerCategory entiry);
}
