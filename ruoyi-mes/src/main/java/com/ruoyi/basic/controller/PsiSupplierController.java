package com.ruoyi.basic.controller;

import java.util.List;
import javax.servlet.http.HttpServletResponse;

import com.ruoyi.basic.domain.PsiWarehouse;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.ruoyi.common.annotation.Log;
import com.ruoyi.common.core.controller.BaseController;
import com.ruoyi.common.core.domain.AjaxResult;
import com.ruoyi.common.enums.BusinessType;
import com.ruoyi.basic.domain.PsiSupplier;
import com.ruoyi.basic.service.IPsiSupplierService;
import com.ruoyi.common.utils.poi.ExcelUtil;
import com.ruoyi.common.core.page.TableDataInfo;

/**
 * 供应商Controller
 * 
 * @author smes
 * @date 2023-06-06
 */
@RestController
@RequestMapping("/basic/supplier")
public class PsiSupplierController extends BaseController
{
    @Autowired
    private IPsiSupplierService psiSupplierService;

    /**
     * 查询供应商列表
     */
    @PreAuthorize("@ss.hasPermi('basic:supplier:list')")
    @GetMapping("/list")
    public TableDataInfo list(PsiSupplier psiSupplier)
    {
        startPage();
        List<PsiSupplier> list = psiSupplierService.selectPsiSupplierList(psiSupplier);
        return getDataTable(list);
    }

    /**
     * 导出供应商列表
     */
    @PreAuthorize("@ss.hasPermi('basic:supplier:export')")
    @Log(title = "供应商", businessType = BusinessType.EXPORT)
    @PostMapping("/export")
    public void export(HttpServletResponse response, PsiSupplier psiSupplier)
    {
        List<PsiSupplier> list = psiSupplierService.selectPsiSupplierList(psiSupplier);
        ExcelUtil<PsiSupplier> util = new ExcelUtil<PsiSupplier>(PsiSupplier.class);
        util.exportExcel(response, list, "供应商数据");
    }

    /**
     * 获取供应商详细信息
     */
    @PreAuthorize("@ss.hasPermi('basic:supplier:query')")
    @GetMapping(value = "/{id}")
    public AjaxResult getInfo(@PathVariable("id") Long id)
    {
        return AjaxResult.success(psiSupplierService.selectPsiSupplierById(id));
    }

    /**
     * 新增供应商
     */
    @PreAuthorize("@ss.hasPermi('basic:supplier:add')")
    @Log(title = "供应商", businessType = BusinessType.INSERT)
    @PostMapping
    public AjaxResult add(@Validated  @RequestBody PsiSupplier psiSupplier)
    {
        if (psiSupplierService.isDuplicate(psiSupplier)== false){
            return toAjax(psiSupplierService.insertPsiSupplier(psiSupplier));
        }else{
            return AjaxResult.error("名称和编码重复。");
        }
    }

    /**
     * 修改供应商
     */
    @PreAuthorize("@ss.hasPermi('basic:supplier:edit')")
    @Log(title = "供应商", businessType = BusinessType.UPDATE)
    @PutMapping
    public AjaxResult edit(@Validated @RequestBody PsiSupplier psiSupplier)
    {
        if (psiSupplierService.isDuplicate(psiSupplier)== false){
            return toAjax(psiSupplierService.updatePsiSupplier(psiSupplier));
        }else{
            return AjaxResult.error("名称和编码重复。");
        }
    }

    /**
     * 删除供应商
     */
    @PreAuthorize("@ss.hasPermi('basic:supplier:remove')")
    @Log(title = "供应商", businessType = BusinessType.DELETE)
	@DeleteMapping("/{ids}")
    public AjaxResult remove(@PathVariable Long[] ids)
    {
        return toAjax(psiSupplierService.deletePsiSupplierByIds(ids));
    }

    /**
     * 查询供应商列表
     * 不分页
     */
    @GetMapping("/dropdownList")
    public AjaxResult dropdownList()
    {
        PsiSupplier entity = new PsiSupplier();
        entity.setDelFlag("0");
        List<PsiSupplier> list = psiSupplierService.selectPsiSupplierList(entity);
        return AjaxResult.success(list);
    }
}
