package com.ruoyi.salesmgt.controller;

import java.util.List;
import javax.servlet.http.HttpServletResponse;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.ruoyi.common.annotation.Log;
import com.ruoyi.common.core.controller.BaseController;
import com.ruoyi.common.core.domain.AjaxResult;
import com.ruoyi.common.enums.BusinessType;
import com.ruoyi.salesmgt.domain.PsiSalesOrder;
import com.ruoyi.salesmgt.service.IPsiSalesOrderService;
import com.ruoyi.common.utils.poi.ExcelUtil;
import com.ruoyi.common.core.page.TableDataInfo;

/**
 * 销售订单Controller
 * 
 * @author smes
 * @date 2023-06-12
 */
@RestController
@RequestMapping("/salesmgt/order")
public class PsiSalesOrderController extends BaseController
{
    @Autowired
    private IPsiSalesOrderService psiSalesOrderService;

    /**
     * 查询销售订单列表
     */
    @PreAuthorize("@ss.hasPermi('salesmgt:order:list')")
    @GetMapping("/list")
    public TableDataInfo list(PsiSalesOrder psiSalesOrder)
    {
        startPage();
        List<PsiSalesOrder> list = psiSalesOrderService.selectPsiSalesOrderList(psiSalesOrder);
        return getDataTable(list);
    }

    /**
     * 导出销售订单列表
     */
    @PreAuthorize("@ss.hasPermi('salesmgt:order:export')")
    @Log(title = "销售订单", businessType = BusinessType.EXPORT)
    @PostMapping("/export")
    public void export(HttpServletResponse response, PsiSalesOrder psiSalesOrder)
    {
        List<PsiSalesOrder> list = psiSalesOrderService.selectPsiSalesOrderList(psiSalesOrder);
        ExcelUtil<PsiSalesOrder> util = new ExcelUtil<PsiSalesOrder>(PsiSalesOrder.class);
        util.exportExcel(response, list, "销售订单数据");
    }

    /**
     * 获取销售订单详细信息
     */
    @PreAuthorize("@ss.hasPermi('salesmgt:order:query')")
    @GetMapping(value = "/{salesOrderId}")
    public AjaxResult getInfo(@PathVariable("salesOrderId") Long salesOrderId)
    {
        return AjaxResult.success(psiSalesOrderService.selectPsiSalesOrderBySalesOrderId(salesOrderId));
    }

    /**
     * 新增销售订单
     */
    @PreAuthorize("@ss.hasPermi('salesmgt:order:add')")
    @Log(title = "销售订单", businessType = BusinessType.INSERT)
    @PostMapping
    public AjaxResult add(@RequestBody PsiSalesOrder psiSalesOrder)
    {
        return toAjax(psiSalesOrderService.insertPsiSalesOrder(psiSalesOrder));
    }

    /**
     * 修改销售订单
     */
    @PreAuthorize("@ss.hasPermi('salesmgt:order:edit')")
    @Log(title = "销售订单", businessType = BusinessType.UPDATE)
    @PutMapping
    public AjaxResult edit(@RequestBody PsiSalesOrder psiSalesOrder)
    {
        return toAjax(psiSalesOrderService.updatePsiSalesOrder(psiSalesOrder));
    }

    /**
     * 删除销售订单
     */
    @PreAuthorize("@ss.hasPermi('salesmgt:order:remove')")
    @Log(title = "销售订单", businessType = BusinessType.DELETE)
	@DeleteMapping("/{salesOrderIds}")
    public AjaxResult remove(@PathVariable Long[] salesOrderIds)
    {
        return toAjax(psiSalesOrderService.deletePsiSalesOrderBySalesOrderIds(salesOrderIds));
    }
}
