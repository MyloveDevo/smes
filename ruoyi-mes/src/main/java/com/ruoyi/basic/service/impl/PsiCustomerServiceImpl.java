package com.ruoyi.basic.service.impl;

import java.util.List;

import com.ruoyi.basic.domain.PsiCustomerCategory;
import com.ruoyi.common.utils.DateUtils;
import com.ruoyi.common.utils.SecurityUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.ruoyi.basic.mapper.PsiCustomerMapper;
import com.ruoyi.basic.domain.PsiCustomer;
import com.ruoyi.basic.service.IPsiCustomerService;

/**
 * 客户Service业务层处理
 * 
 * @author smes
 * @date 2023-06-07
 */
@Service
public class PsiCustomerServiceImpl implements IPsiCustomerService 
{
    @Autowired
    private PsiCustomerMapper psiCustomerMapper;

    /**
     * 查询客户
     * 
     * @param id 客户主键
     * @return 客户
     */
    @Override
    public PsiCustomer selectPsiCustomerById(Long id)
    {
        return psiCustomerMapper.selectPsiCustomerById(id);
    }

    /**
     * 查询客户列表
     * 
     * @param psiCustomer 客户
     * @return 客户
     */
    @Override
    public List<PsiCustomer> selectPsiCustomerList(PsiCustomer psiCustomer)
    {
        return psiCustomerMapper.selectPsiCustomerList(psiCustomer);
    }

    /**
     * 新增客户
     * 
     * @param psiCustomer 客户
     * @return 结果
     */
    @Override
    public int insertPsiCustomer(PsiCustomer psiCustomer)
    {
        psiCustomer.setDelFlag("0");
        psiCustomer.setCreateBy(SecurityUtils.getUsername());
        psiCustomer.setCreateTime(DateUtils.getNowDate());
        return psiCustomerMapper.insertPsiCustomer(psiCustomer);
    }

    /**
     * 修改客户
     * 
     * @param psiCustomer 客户
     * @return 结果
     */
    @Override
    public int updatePsiCustomer(PsiCustomer psiCustomer)
    {
        psiCustomer.setUpdateBy(SecurityUtils.getUsername());
        psiCustomer.setUpdateTime(DateUtils.getNowDate());
        return psiCustomerMapper.updatePsiCustomer(psiCustomer);
    }

    /**
     * 批量删除客户
     * 
     * @param ids 需要删除的客户主键
     * @return 结果
     */
    @Override
    public int deletePsiCustomerByIds(Long[] ids)
    {
        return psiCustomerMapper.deletePsiCustomerByIds(ids);
    }

    /**
     * 删除客户信息
     * 
     * @param id 客户主键
     * @return 结果
     */
    @Override
    public int deletePsiCustomerById(Long id)
    {
        return psiCustomerMapper.deletePsiCustomerById(id);
    }

    /**
     * 查重
     */
    @Override
    public boolean isDuplicate(PsiCustomer entity) {
        List<PsiCustomer> lst = psiCustomerMapper.findOneByCondition(entity);
        return lst.size()>0;
    }
}
