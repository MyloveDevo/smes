package com.ruoyi.purmgt.domain;

import java.math.BigDecimal;
import java.util.Date;
import com.fasterxml.jackson.annotation.JsonFormat;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import com.ruoyi.common.annotation.Excel;
import com.ruoyi.common.core.domain.BaseEntity;

/**
 * 采购合同对象 psi_purchase_contract
 * 
 * @author smes
 * @date 2023-06-19
 */
public class PsiPurchaseContract extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /** PK */
    private Long purchaseContractId;

    /** 采购订单ID */
    @Excel(name = "采购订单ID")
    private Long purchaseOrderId;

    /** 税率 */
    @Excel(name = "税率")
    private BigDecimal taxRate;

    /** 不含税金额 */
    @Excel(name = "不含税金额")
    private BigDecimal excludingTax;

    /** 含税金额 */
    @Excel(name = "含税金额")
    private BigDecimal taxIncluded;

    /** 结算日期 合同约定日期 */
    @JsonFormat(pattern = "yyyy-MM-dd")
    @Excel(name = "结算日期 合同约定日期", width = 30, dateFormat = "yyyy-MM-dd")
    private Date balanceDate;

    /** 付款日期 时间付款日期 */
    @JsonFormat(pattern = "yyyy-MM-dd")
    @Excel(name = "付款日期 时间付款日期", width = 30, dateFormat = "yyyy-MM-dd")
    private Date paymentDate;

    /** 支付状态（0 ,<合同签订>；1,<开具发票>；2,<已收货款>） */
    @Excel(name = "支付状态", readConverterExp = "0=,,=<合同签订>；1,<开具发票>；2,<已收货款>")
    private String paymentStatu;

    /** 删除标志（0代表存在 1代表删除） */
    private String delFlag;

    public void setPurchaseContractId(Long purchaseContractId) 
    {
        this.purchaseContractId = purchaseContractId;
    }

    public Long getPurchaseContractId() 
    {
        return purchaseContractId;
    }
    public void setPurchaseOrderId(Long purchaseOrderId) 
    {
        this.purchaseOrderId = purchaseOrderId;
    }

    public Long getPurchaseOrderId() 
    {
        return purchaseOrderId;
    }
    public void setTaxRate(BigDecimal taxRate) 
    {
        this.taxRate = taxRate;
    }

    public BigDecimal getTaxRate() 
    {
        return taxRate;
    }
    public void setExcludingTax(BigDecimal excludingTax) 
    {
        this.excludingTax = excludingTax;
    }

    public BigDecimal getExcludingTax() 
    {
        return excludingTax;
    }
    public void setTaxIncluded(BigDecimal taxIncluded) 
    {
        this.taxIncluded = taxIncluded;
    }

    public BigDecimal getTaxIncluded() 
    {
        return taxIncluded;
    }
    public void setBalanceDate(Date balanceDate) 
    {
        this.balanceDate = balanceDate;
    }

    public Date getBalanceDate() 
    {
        return balanceDate;
    }
    public void setPaymentDate(Date paymentDate) 
    {
        this.paymentDate = paymentDate;
    }

    public Date getPaymentDate() 
    {
        return paymentDate;
    }
    public void setPaymentStatu(String paymentStatu) 
    {
        this.paymentStatu = paymentStatu;
    }

    public String getPaymentStatu() 
    {
        return paymentStatu;
    }
    public void setDelFlag(String delFlag) 
    {
        this.delFlag = delFlag;
    }

    public String getDelFlag() 
    {
        return delFlag;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this,ToStringStyle.MULTI_LINE_STYLE)
            .append("purchaseContractId", getPurchaseContractId())
            .append("purchaseOrderId", getPurchaseOrderId())
            .append("taxRate", getTaxRate())
            .append("excludingTax", getExcludingTax())
            .append("taxIncluded", getTaxIncluded())
            .append("balanceDate", getBalanceDate())
            .append("paymentDate", getPaymentDate())
            .append("paymentStatu", getPaymentStatu())
            .append("delFlag", getDelFlag())
            .append("remark", getRemark())
            .append("createBy", getCreateBy())
            .append("createTime", getCreateTime())
            .append("updateBy", getUpdateBy())
            .append("updateTime", getUpdateTime())
            .toString();
    }
}
