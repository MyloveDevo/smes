package com.ruoyi.basic.mapper;

import java.util.List;
import com.ruoyi.basic.domain.PsiSupplier;
import com.ruoyi.basic.domain.PsiSupplierCategory;
import com.ruoyi.basic.domain.PsiWarehouse;

/**
 * 供应商Mapper接口
 * 
 * @author smes
 * @date 2023-06-06
 */
public interface PsiSupplierMapper 
{
    /**
     * 查询供应商
     * 
     * @param id 供应商主键
     * @return 供应商
     */
    public PsiSupplier selectPsiSupplierById(Long id);

    /**
     * 查询供应商列表
     * 
     * @param psiSupplier 供应商
     * @return 供应商集合
     */
    public List<PsiSupplier> selectPsiSupplierList(PsiSupplier psiSupplier);

    /**
     * 新增供应商
     * 
     * @param psiSupplier 供应商
     * @return 结果
     */
    public int insertPsiSupplier(PsiSupplier psiSupplier);

    /**
     * 修改供应商
     * 
     * @param psiSupplier 供应商
     * @return 结果
     */
    public int updatePsiSupplier(PsiSupplier psiSupplier);

    /**
     * 删除供应商
     * 
     * @param id 供应商主键
     * @return 结果
     */
    public int deletePsiSupplierById(Long id);

    /**
     * 批量删除供应商
     * 
     * @param ids 需要删除的数据主键集合
     * @return 结果
     */
    public int deletePsiSupplierByIds(Long[] ids);

    List<PsiSupplier> findOneByCondition(PsiSupplier entity);
}
