package com.ruoyi.purmgt.controller;

import java.util.List;
import javax.servlet.http.HttpServletResponse;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.ruoyi.common.annotation.Log;
import com.ruoyi.common.core.controller.BaseController;
import com.ruoyi.common.core.domain.AjaxResult;
import com.ruoyi.common.enums.BusinessType;
import com.ruoyi.purmgt.domain.PsiPurchaseReq;
import com.ruoyi.purmgt.service.IPsiPurchaseReqService;
import com.ruoyi.common.utils.poi.ExcelUtil;
import com.ruoyi.common.core.page.TableDataInfo;

/**
 * 请购单 Controller
 * 
 * @author smes
 * @date 2023-06-19
 */
@RestController
@RequestMapping("/purmgt/pr")
public class PsiPurchaseReqController extends BaseController
{
    @Autowired
    private IPsiPurchaseReqService psiPurchaseReqService;

    /**
     * 查询请购单 列表
     */
    @PreAuthorize("@ss.hasPermi('purmgt:pr:list')")
    @GetMapping("/list")
    public TableDataInfo list(PsiPurchaseReq psiPurchaseReq)
    {
        startPage();
        List<PsiPurchaseReq> list = psiPurchaseReqService.selectPsiPurchaseReqList(psiPurchaseReq);
        return getDataTable(list);
    }

    /**
     * 导出请购单 列表
     */
    @PreAuthorize("@ss.hasPermi('purmgt:pr:export')")
    @Log(title = "请购单 ", businessType = BusinessType.EXPORT)
    @PostMapping("/export")
    public void export(HttpServletResponse response, PsiPurchaseReq psiPurchaseReq)
    {
        List<PsiPurchaseReq> list = psiPurchaseReqService.selectPsiPurchaseReqList(psiPurchaseReq);
        ExcelUtil<PsiPurchaseReq> util = new ExcelUtil<PsiPurchaseReq>(PsiPurchaseReq.class);
        util.exportExcel(response, list, "请购单 数据");
    }

    /**
     * 获取请购单 详细信息
     */
    @PreAuthorize("@ss.hasPermi('purmgt:pr:query')")
    @GetMapping(value = "/{id}")
    public AjaxResult getInfo(@PathVariable("id") Long id)
    {
        return AjaxResult.success(psiPurchaseReqService.selectPsiPurchaseReqById(id));
    }

    /**
     * 新增请购单 
     */
    @PreAuthorize("@ss.hasPermi('purmgt:pr:add')")
    @Log(title = "请购单 ", businessType = BusinessType.INSERT)
    @PostMapping
    public AjaxResult add(@RequestBody PsiPurchaseReq psiPurchaseReq)
    {
        return toAjax(psiPurchaseReqService.insertPsiPurchaseReq(psiPurchaseReq));
    }

    /**
     * 修改请购单 
     */
    @PreAuthorize("@ss.hasPermi('purmgt:pr:edit')")
    @Log(title = "请购单 ", businessType = BusinessType.UPDATE)
    @PutMapping
    public AjaxResult edit(@RequestBody PsiPurchaseReq psiPurchaseReq)
    {
        return toAjax(psiPurchaseReqService.updatePsiPurchaseReq(psiPurchaseReq));
    }

    /**
     * 删除请购单 
     */
    @PreAuthorize("@ss.hasPermi('purmgt:pr:remove')")
    @Log(title = "请购单 ", businessType = BusinessType.DELETE)
	@DeleteMapping("/{ids}")
    public AjaxResult remove(@PathVariable Long[] ids)
    {
        return toAjax(psiPurchaseReqService.deletePsiPurchaseReqByIds(ids));
    }
}
