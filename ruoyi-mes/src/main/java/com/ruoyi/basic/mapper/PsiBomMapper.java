package com.ruoyi.basic.mapper;

import java.util.List;
import com.ruoyi.basic.domain.PsiBom;
import com.ruoyi.basic.domain.PsiGoods;

/**
 * BOMMapper接口
 * 
 * @author smes
 * @date 2023-06-08
 */
public interface PsiBomMapper 
{
    /**
     * 查询BOM
     * 
     * @param id BOM主键
     * @return BOM
     */
    public PsiBom selectPsiBomById(Long id);

    /**
     * 查询BOM列表
     * 
     * @param psiBom BOM
     * @return BOM集合
     */
    public List<PsiBom> selectPsiBomList(PsiBom psiBom);

    /**
     * 新增BOM
     * 
     * @param psiBom BOM
     * @return 结果
     */
    public int insertPsiBom(PsiBom psiBom);

    /**
     * 修改BOM
     * 
     * @param psiBom BOM
     * @return 结果
     */
    public int updatePsiBom(PsiBom psiBom);

    /**
     * 删除BOM
     * 
     * @param id BOM主键
     * @return 结果
     */
    public int deletePsiBomById(Long id);

    /**
     * 批量删除BOM
     * 
     * @param ids 需要删除的数据主键集合
     * @return 结果
     */
    public int deletePsiBomByIds(Long[] ids);

    /**
     * 查询BOMRoot列表
     *
     * @param psiBom BOM
     * @return BOM集合
     */
    public List<PsiBom> selectPsiRootBomList(PsiBom psiBom);

    /**
     * 查询BOM Tree By root id列表
     *
     * @param psiBom BOM
     * @return BOM集合
     */
    public List<PsiBom> selectPsiBomTreeListById(Long id);

    List<PsiBom> findOneByCondition(PsiBom entiry);


}
