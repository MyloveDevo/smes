package com.ruoyi.inventorymgt.controller;

import java.util.List;
import javax.servlet.http.HttpServletResponse;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.ruoyi.common.annotation.Log;
import com.ruoyi.common.core.controller.BaseController;
import com.ruoyi.common.core.domain.AjaxResult;
import com.ruoyi.common.enums.BusinessType;
import com.ruoyi.inventorymgt.domain.PsiInventory;
import com.ruoyi.inventorymgt.service.IPsiInventoryService;
import com.ruoyi.common.utils.poi.ExcelUtil;
import com.ruoyi.common.core.page.TableDataInfo;

/**
 * 实时库存Controller
 * 
 * @author smes
 * @date 2023-06-20
 */
@RestController
@RequestMapping("/inventorymgt/inventory")
public class PsiInventoryController extends BaseController
{
    @Autowired
    private IPsiInventoryService psiInventoryService;

    /**
     * 查询实时库存列表
     */
    @PreAuthorize("@ss.hasPermi('inventorymgt:inventory:list')")
    @GetMapping("/list")
    public TableDataInfo list(PsiInventory psiInventory)
    {
        startPage();
        List<PsiInventory> list = psiInventoryService.selectPsiInventoryList(psiInventory);
        return getDataTable(list);
    }

    /**
     * 导出实时库存列表
     */
    @PreAuthorize("@ss.hasPermi('inventorymgt:inventory:export')")
    @Log(title = "实时库存", businessType = BusinessType.EXPORT)
    @PostMapping("/export")
    public void export(HttpServletResponse response, PsiInventory psiInventory)
    {
        List<PsiInventory> list = psiInventoryService.selectPsiInventoryList(psiInventory);
        ExcelUtil<PsiInventory> util = new ExcelUtil<PsiInventory>(PsiInventory.class);
        util.exportExcel(response, list, "实时库存数据");
    }

    /**
     * 获取实时库存详细信息
     */
    @PreAuthorize("@ss.hasPermi('inventorymgt:inventory:query')")
    @GetMapping(value = "/{id}")
    public AjaxResult getInfo(@PathVariable("id") Long id)
    {
        return AjaxResult.success(psiInventoryService.selectPsiInventoryById(id));
    }

    /**
     * 新增实时库存
     */
    @PreAuthorize("@ss.hasPermi('inventorymgt:inventory:add')")
    @Log(title = "实时库存", businessType = BusinessType.INSERT)
    @PostMapping
    public AjaxResult add(@RequestBody PsiInventory psiInventory)
    {
        return toAjax(psiInventoryService.insertPsiInventory(psiInventory));
    }

    /**
     * 修改实时库存
     */
    @PreAuthorize("@ss.hasPermi('inventorymgt:inventory:edit')")
    @Log(title = "实时库存", businessType = BusinessType.UPDATE)
    @PutMapping
    public AjaxResult edit(@RequestBody PsiInventory psiInventory)
    {
        return toAjax(psiInventoryService.updatePsiInventory(psiInventory));
    }

    /**
     * 删除实时库存
     */
    @PreAuthorize("@ss.hasPermi('inventorymgt:inventory:remove')")
    @Log(title = "实时库存", businessType = BusinessType.DELETE)
	@DeleteMapping("/{ids}")
    public AjaxResult remove(@PathVariable Long[] ids)
    {
        return toAjax(psiInventoryService.deletePsiInventoryByIds(ids));
    }
}
