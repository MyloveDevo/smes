import request from '@/utils/request'

// 查询销售订单列表
export function listOrder(query) {
  return request({
    url: '/salesmgt/order/list',
    method: 'get',
    params: query
  })
}

// 查询销售订单详细
export function getOrder(salesOrderId) {
  return request({
    url: '/salesmgt/order/' + salesOrderId,
    method: 'get'
  })
}

// 新增销售订单
export function addOrder(data) {
  return request({
    url: '/salesmgt/order',
    method: 'post',
    data: data
  })
}

// 修改销售订单
export function updateOrder(data) {
  return request({
    url: '/salesmgt/order',
    method: 'put',
    data: data
  })
}

// 删除销售订单
export function delOrder(salesOrderId) {
  return request({
    url: '/salesmgt/order/' + salesOrderId,
    method: 'delete'
  })
}
