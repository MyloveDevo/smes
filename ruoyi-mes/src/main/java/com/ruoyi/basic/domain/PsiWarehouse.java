package com.ruoyi.basic.domain;

import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import com.ruoyi.common.annotation.Excel;
import com.ruoyi.common.core.domain.BaseEntity;

import javax.validation.constraints.Size;

/**
 * 仓库对象 psi_warehouse
 * 
 * @author smes
 * @date 2023-06-05
 */
public class PsiWarehouse extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /** PK */
    private Long id;

    /** 仓库名 */
    @Excel(name = "仓库名")
    @Size(min = 0, max = 10, message = "仓库名长度不能超过10个字符")
    private String warehouseName;

    /** 仓库码 */
    @Excel(name = "仓库码")
    @Size(min = 0, max = 10, message = "仓库码长度不能超过10个字符")
    private String warehouseCode;

    /** 岗位 */
    @Excel(name = "岗位")
    @Size(min = 0, max = 10, message = "岗位长度不能超过10个字符")
    private String position;

    /** 联系人 */
    @Excel(name = "联系人")
    @Size(min = 0, max = 10, message = "联系人长度不能超过10个字符")
    private String contacts;

    /** 地址 */
    @Excel(name = "地址")
    @Size(min = 0, max = 10, message = "地址长度不能超过100个字符")
    private String address;

    /** 区域 */
    @Excel(name = "区域")
    private String area;

    /** 删除标志（0代表存在 1代表删除） */
    private String delFlag;

    public void setId(Long id) 
    {
        this.id = id;
    }

    public Long getId() 
    {
        return id;
    }
    public void setWarehouseName(String warehouseName) 
    {
        this.warehouseName = warehouseName;
    }

    public String getWarehouseName() 
    {
        return warehouseName;
    }
    public void setWarehouseCode(String warehouseCode) 
    {
        this.warehouseCode = warehouseCode;
    }

    public String getWarehouseCode() 
    {
        return warehouseCode;
    }
    public void setPosition(String position) 
    {
        this.position = position;
    }

    public String getPosition() 
    {
        return position;
    }
    public void setContacts(String contacts) 
    {
        this.contacts = contacts;
    }

    public String getContacts() 
    {
        return contacts;
    }
    public void setAddress(String address) 
    {
        this.address = address;
    }

    public String getAddress() 
    {
        return address;
    }
    public void setArea(String area) 
    {
        this.area = area;
    }

    public String getArea() 
    {
        return area;
    }
    public void setDelFlag(String delFlag) 
    {
        this.delFlag = delFlag;
    }

    public String getDelFlag() 
    {
        return delFlag;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this,ToStringStyle.MULTI_LINE_STYLE)
            .append("id", getId())
            .append("warehouseName", getWarehouseName())
            .append("warehouseCode", getWarehouseCode())
            .append("position", getPosition())
            .append("contacts", getContacts())
            .append("address", getAddress())
            .append("area", getArea())
            .append("remark", getRemark())
            .append("delFlag", getDelFlag())
            .append("createBy", getCreateBy())
            .append("createTime", getCreateTime())
            .append("updateBy", getUpdateBy())
            .append("updateTime", getUpdateTime())
            .toString();
    }
}
