package com.ruoyi.basic.service.impl;

import com.ruoyi.basic.domain.PsiWarehouse;
import com.ruoyi.basic.mapper.PsiWarehouseMapper;
import com.ruoyi.basic.service.IPsiWarehouseService;
import com.ruoyi.common.utils.DateUtils;
import com.ruoyi.common.utils.SecurityUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * 仓库Service业务层处理
 *
 * @author smes
 * @date 2023-06-05
 */
@Service
public class PsiWarehouseServiceImpl implements IPsiWarehouseService {
    @Autowired
    private PsiWarehouseMapper psiWarehouseMapper;

    /**
     * 查询仓库
     *
     * @param id 仓库主键
     * @return 仓库
     */
    @Override
    public PsiWarehouse selectPsiWarehouseById(Long id) {
        return psiWarehouseMapper.selectPsiWarehouseById(id);
    }

    /**
     * 查询仓库列表
     *
     * @param psiWarehouse 仓库
     * @return 仓库
     */
    @Override
    public List<PsiWarehouse> selectPsiWarehouseList(PsiWarehouse psiWarehouse) {
        return psiWarehouseMapper.selectPsiWarehouseList(psiWarehouse);
    }

    /**
     * 新增仓库
     *
     * @param psiWarehouse 仓库
     * @return 结果
     */
    @Override
    public int insertPsiWarehouse(PsiWarehouse psiWarehouse) {
        psiWarehouse.setCreateTime(DateUtils.getNowDate());
        psiWarehouse.setCreateBy(SecurityUtils.getUsername());
        psiWarehouse.setDelFlag("0");
        return psiWarehouseMapper.insertPsiWarehouse(psiWarehouse);
    }

    /**
     * 修改仓库
     *
     * @param psiWarehouse 仓库
     * @return 结果
     */
    @Override
    public int updatePsiWarehouse(PsiWarehouse psiWarehouse) {
        psiWarehouse.setUpdateTime(DateUtils.getNowDate());
        psiWarehouse.setUpdateBy(SecurityUtils.getUsername());
        return psiWarehouseMapper.updatePsiWarehouse(psiWarehouse);
    }

    /**
     * 批量删除仓库
     *
     * @param ids 需要删除的仓库主键
     * @return 结果
     */
    @Override
    public int deletePsiWarehouseByIds(Long[] ids) {
        return psiWarehouseMapper.deletePsiWarehouseByIds(ids);
    }

    /**
     * 删除仓库信息
     *
     * @param id 仓库主键
     * @return 结果
     */
    @Override
    public int deletePsiWarehouseById(Long id) {
        return psiWarehouseMapper.deletePsiWarehouseById(id);
    }

    /**
     * 查重
     */
    @Override
    public boolean isDuplicate(PsiWarehouse psiWarehouse) {
        List<PsiWarehouse> lst = psiWarehouseMapper.findOneByCondition(psiWarehouse);
        return lst.size()>0;
    }
}