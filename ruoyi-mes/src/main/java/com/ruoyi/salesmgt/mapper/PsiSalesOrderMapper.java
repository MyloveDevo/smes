package com.ruoyi.salesmgt.mapper;

import java.util.List;

import com.ruoyi.salesmgt.domain.PsiSalesContract;
import com.ruoyi.salesmgt.domain.PsiSalesOrder;
import com.ruoyi.salesmgt.domain.PsiSalesOrderDetail;

/**
 * 销售订单Mapper接口
 * 
 * @author smes
 * @date 2023-06-12
 */
public interface PsiSalesOrderMapper 
{
    /**
     * 查询销售订单
     * 
     * @param salesOrderId 销售订单主键
     * @return 销售订单
     */
    public PsiSalesOrder selectPsiSalesOrderBySalesOrderId(Long salesOrderId);

    /**
     * 查询销售订单列表
     * 
     * @param psiSalesOrder 销售订单
     * @return 销售订单集合
     */
    public List<PsiSalesOrder> selectPsiSalesOrderList(PsiSalesOrder psiSalesOrder);

    /**
     * 新增销售订单
     * 
     * @param psiSalesOrder 销售订单
     * @return 结果
     */
    public int insertPsiSalesOrder(PsiSalesOrder psiSalesOrder);

    /**
     * 修改销售订单
     * 
     * @param psiSalesOrder 销售订单
     * @return 结果
     */
    public int updatePsiSalesOrder(PsiSalesOrder psiSalesOrder);

    /**
     * 删除销售订单
     * 
     * @param salesOrderId 销售订单主键
     * @return 结果
     */
    public int deletePsiSalesOrderBySalesOrderId(Long salesOrderId);

    /**
     * 批量删除销售订单
     * 
     * @param salesOrderIds 需要删除的数据主键集合
     * @return 结果
     */
    public int deletePsiSalesOrderBySalesOrderIds(Long[] salesOrderIds);

    /**
     * 批量删除销售订单明细
     * 
     * @param salesOrderIds 需要删除的数据主键集合
     * @return 结果
     */
    public int deletePsiSalesOrderDetailBySalesOrderIds(Long[] salesOrderIds);
    
    /**
     * 批量新增销售订单明细
     * 
     * @param psiSalesOrderDetailList 销售订单明细列表
     * @return 结果
     */
    public int batchPsiSalesOrderDetail(List<PsiSalesOrderDetail> psiSalesOrderDetailList);
    

    /**
     * 通过销售订单主键删除销售订单明细信息
     * 
     * @param salesOrderId 销售订单ID
     * @return 结果
     */
    public int deletePsiSalesOrderDetailBySalesOrderId(Long salesOrderId);

    /**
     * 批量删除销售合同
     *
     * @param salesOrderIds 需要删除的数据主键集合
     * @return 结果
     */
    public int deletePsiSalesContractBySalesOrderIds(Long[] salesOrderIds);

    /**
     * 批量新增销售合同
     *
     * @param psiSalesContractList 销售合同列表
     * @return 结果
     */
    public int batchPsiSalesContract(List<PsiSalesContract> psiSalesContractList);


    /**
     * 通过销售订单主键删除销售合同信息
     *
     * @param salesOrderId 销售订单ID
     * @return 结果
     */
    public int deletePsiSalesContractBySalesOrderId(Long salesOrderId);
}
