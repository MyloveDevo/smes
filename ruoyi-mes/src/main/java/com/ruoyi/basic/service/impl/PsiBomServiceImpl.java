package com.ruoyi.basic.service.impl;

import java.util.List;

import com.ruoyi.basic.domain.PsiSupplier;
import com.ruoyi.common.utils.DateUtils;
import com.ruoyi.common.utils.SecurityUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.ruoyi.basic.mapper.PsiBomMapper;
import com.ruoyi.basic.domain.PsiBom;
import com.ruoyi.basic.service.IPsiBomService;

/**
 * BOMService业务层处理
 * 
 * @author smes
 * @date 2023-06-08
 */
@Service
public class PsiBomServiceImpl implements IPsiBomService 
{
    @Autowired
    private PsiBomMapper psiBomMapper;

    /**
     * 查询BOM
     * 
     * @param id BOM主键
     * @return BOM
     */
    @Override
    public PsiBom selectPsiBomById(Long id)
    {
        return psiBomMapper.selectPsiBomById(id);
    }

    /**
     * 查询BOM列表
     * 
     * @param psiBom BOM
     * @return BOM
     */
    @Override
    public List<PsiBom> selectPsiBomList(PsiBom psiBom)
    {
        return psiBomMapper.selectPsiBomList(psiBom);
    }

    /**
     * 新增BOM
     * 
     * @param psiBom BOM
     * @return 结果
     */
    @Override
    public int insertPsiBom(PsiBom psiBom)
    {
        psiBom.setDeleteFlag("0");
        psiBom.setCreateBy(SecurityUtils.getUsername());
        psiBom.setCreateUserId(SecurityUtils.getUserId());
        psiBom.setCreateTime(DateUtils.getNowDate());
        return psiBomMapper.insertPsiBom(psiBom);
    }

    /**
     * 新增BOM
     *
     * @param psiBom BOM
     * @return PK
     */
    @Override
    public PsiBom addPsiBom(PsiBom psiBom)
    {
        psiBom.setDeleteFlag("0");
        psiBom.setCreateBy(SecurityUtils.getUsername());
        psiBom.setCreateUserId(SecurityUtils.getUserId());
        psiBom.setCreateUserId(SecurityUtils.getUserId());
        psiBom.setCreateTime(DateUtils.getNowDate());
        psiBomMapper.insertPsiBom(psiBom);
        return psiBom;
    }

    /**
     * 修改BOM
     * 
     * @param psiBom BOM
     * @return 结果
     */
    @Override
    public int updatePsiBom(PsiBom psiBom)
    {
        psiBom.setUpdateBy(SecurityUtils.getUsername());
        psiBom.setUpdateUserId(SecurityUtils.getUserId());
        psiBom.setUpdateTime(DateUtils.getNowDate());
        return psiBomMapper.updatePsiBom(psiBom);
    }

    /**
     * 批量删除BOM
     * 
     * @param ids 需要删除的BOM主键
     * @return 结果
     */
    @Override
    public int deletePsiBomByIds(Long[] ids)
    {
        return psiBomMapper.deletePsiBomByIds(ids);
    }

    /**
     * 删除BOM信息
     * 
     * @param id BOM主键
     * @return 结果
     */
    @Override
    public int deletePsiBomById(Long id)
    {
        return psiBomMapper.deletePsiBomById(id);
    }

    /**
     * 查询BOMRoot列表
     *
     * @param psiBom BOM
     * @return BOM
     */
    @Override
    public List<PsiBom> selectPsiRootBomList(PsiBom psiBom)
    {
        return psiBomMapper.selectPsiRootBomList(psiBom);
    }

    /**
     * 查询BOM tree by Root id列表
     *
     * @param psiBom BOM
     * @return BOM
     */
    @Override
    public List<PsiBom> selectPsiBomTreeListById(Long id)
    {
        return psiBomMapper.selectPsiBomTreeListById(id);
    }

    /**
     * 查重
     */
    @Override
    public boolean isDuplicate(PsiBom entity) {
        List<PsiBom> lst = psiBomMapper.findOneByCondition(entity);
        return lst.size()>0;
    }
}
