package com.ruoyi.basic.controller;

import java.util.List;
import javax.servlet.http.HttpServletResponse;

import com.ruoyi.basic.domain.PsiSupplier;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.ruoyi.common.annotation.Log;
import com.ruoyi.common.core.controller.BaseController;
import com.ruoyi.common.core.domain.AjaxResult;
import com.ruoyi.common.enums.BusinessType;
import com.ruoyi.basic.domain.PsiGoods;
import com.ruoyi.basic.service.IPsiGoodsService;
import com.ruoyi.common.utils.poi.ExcelUtil;
import com.ruoyi.common.core.page.TableDataInfo;

/**
 * 产品物料Controller
 * 
 * @author smes
 * @date 2023-06-05
 */
@RestController
@RequestMapping("/basic/goods")
public class PsiGoodsController extends BaseController
{
    @Autowired
    private IPsiGoodsService psiGoodsService;

    /**
     * 查询产品物料列表
     */
    @PreAuthorize("@ss.hasPermi('basic:goods:list')")
    @GetMapping("/list")
    public TableDataInfo list(PsiGoods psiGoods)
    {
        startPage();
        List<PsiGoods> list = psiGoodsService.selectPsiGoodsList(psiGoods);
        return getDataTable(list);
    }

    /**
     * 导出产品物料列表
     */
    @PreAuthorize("@ss.hasPermi('basic:goods:export')")
    @Log(title = "产品物料", businessType = BusinessType.EXPORT)
    @PostMapping("/export")
    public void export(HttpServletResponse response, PsiGoods psiGoods)
    {
        List<PsiGoods> list = psiGoodsService.selectPsiGoodsList(psiGoods);
        ExcelUtil<PsiGoods> util = new ExcelUtil<PsiGoods>(PsiGoods.class);
        util.exportExcel(response, list, "产品物料数据");
    }

    /**
     * 获取产品物料详细信息
     */
    @PreAuthorize("@ss.hasPermi('basic:goods:query')")
    @GetMapping(value = "/{id}")
    public AjaxResult getInfo(@PathVariable("id") Long id)
    {
        return AjaxResult.success(psiGoodsService.selectPsiGoodsById(id));
    }

    /**
     * 新增产品物料
     */
    @PreAuthorize("@ss.hasPermi('basic:goods:add')")
    @Log(title = "产品物料", businessType = BusinessType.INSERT)
    @PostMapping
    public AjaxResult add(@Validated @RequestBody PsiGoods psiGoods)
    {
        if (psiGoodsService.isDuplicate(psiGoods)== false){
            return toAjax(psiGoodsService.insertPsiGoods(psiGoods));
        }else{
            return AjaxResult.error("产品名称和产品编码重复。");
        }
    }

    /**
     * 修改产品物料
     */
    @PreAuthorize("@ss.hasPermi('basic:goods:edit')")
    @Log(title = "产品物料", businessType = BusinessType.UPDATE)
    @PutMapping
    public AjaxResult edit(@Validated @RequestBody PsiGoods psiGoods)
    {
        if (psiGoodsService.isDuplicate(psiGoods)== false){
            return toAjax(psiGoodsService.updatePsiGoods(psiGoods));
        }else{
            return AjaxResult.error("产品名称和产品编码重复。");
        }
    }

    /**
     * 删除产品物料
     */
    @PreAuthorize("@ss.hasPermi('basic:goods:remove')")
    @Log(title = "产品物料", businessType = BusinessType.DELETE)
	@DeleteMapping("/{ids}")
    public AjaxResult remove(@PathVariable Long[] ids)
    {
        return toAjax(psiGoodsService.deletePsiGoodsByIds(ids));
    }

    /**
     * 查询产品物料列表
     * 不分页
     */
    @GetMapping("/dropdownList")
    public AjaxResult dropdownList()
    {
        PsiGoods entity = new PsiGoods();
        entity.setDelFlag("0");
        List<PsiGoods> list = psiGoodsService.selectPsiGoodsList(entity);
        return AjaxResult.success(list);
    }
}
