package com.ruoyi.basic.mapper;

import java.util.List;
import com.ruoyi.basic.domain.PsiCustomer;
import com.ruoyi.basic.domain.PsiCustomerCategory;

/**
 * 客户Mapper接口
 * 
 * @author smes
 * @date 2023-06-07
 */
public interface PsiCustomerMapper 
{
    /**
     * 查询客户
     * 
     * @param id 客户主键
     * @return 客户
     */
    public PsiCustomer selectPsiCustomerById(Long id);

    /**
     * 查询客户列表
     * 
     * @param psiCustomer 客户
     * @return 客户集合
     */
    public List<PsiCustomer> selectPsiCustomerList(PsiCustomer psiCustomer);

    /**
     * 新增客户
     * 
     * @param psiCustomer 客户
     * @return 结果
     */
    public int insertPsiCustomer(PsiCustomer psiCustomer);

    /**
     * 修改客户
     * 
     * @param psiCustomer 客户
     * @return 结果
     */
    public int updatePsiCustomer(PsiCustomer psiCustomer);

    /**
     * 删除客户
     * 
     * @param id 客户主键
     * @return 结果
     */
    public int deletePsiCustomerById(Long id);

    /**
     * 批量删除客户
     * 
     * @param ids 需要删除的数据主键集合
     * @return 结果
     */
    public int deletePsiCustomerByIds(Long[] ids);

    List<PsiCustomer> findOneByCondition(PsiCustomer entiry);
}
