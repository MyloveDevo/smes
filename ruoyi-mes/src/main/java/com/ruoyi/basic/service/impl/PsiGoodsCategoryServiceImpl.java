package com.ruoyi.basic.service.impl;

import java.util.List;

import com.ruoyi.basic.domain.PsiSupplierCategory;
import com.ruoyi.basic.domain.PsiWarehouse;
import com.ruoyi.common.utils.DateUtils;
import com.ruoyi.common.utils.SecurityUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.ruoyi.basic.mapper.PsiGoodsCategoryMapper;
import com.ruoyi.basic.domain.PsiGoodsCategory;
import com.ruoyi.basic.service.IPsiGoodsCategoryService;

/**
 * 产品物料分类Service业务层处理
 * 
 * @author smes
 * @date 2023-06-04
 */
@Service
public class PsiGoodsCategoryServiceImpl implements IPsiGoodsCategoryService 
{
    @Autowired
    private PsiGoodsCategoryMapper psiGoodsCategoryMapper;

    /**
     * 查询产品物料分类
     * 
     * @param id 产品物料分类主键
     * @return 产品物料分类
     */
    @Override
    public PsiGoodsCategory selectPsiGoodsCategoryById(Long id)
    {
        return psiGoodsCategoryMapper.selectPsiGoodsCategoryById(id);
    }

    /**
     * 查询产品物料分类列表
     * 
     * @param psiGoodsCategory 产品物料分类
     * @return 产品物料分类
     */
    @Override
    public List<PsiGoodsCategory> selectPsiGoodsCategoryList(PsiGoodsCategory psiGoodsCategory)
    {
        return psiGoodsCategoryMapper.selectPsiGoodsCategoryList(psiGoodsCategory);
    }

    /**
     * 新增产品物料分类
     * 
     * @param psiGoodsCategory 产品物料分类
     * @return 结果
     */
    @Override
    public int insertPsiGoodsCategory(PsiGoodsCategory psiGoodsCategory)
    {
        psiGoodsCategory.setCreateTime(DateUtils.getNowDate());
        psiGoodsCategory.setDelFlag("0");
        psiGoodsCategory.setCreateBy(SecurityUtils.getUsername());
        return psiGoodsCategoryMapper.insertPsiGoodsCategory(psiGoodsCategory);
    }

    /**
     * 修改产品物料分类
     * 
     * @param psiGoodsCategory 产品物料分类
     * @return 结果
     */
    @Override
    public int updatePsiGoodsCategory(PsiGoodsCategory psiGoodsCategory)
    {
        psiGoodsCategory.setUpdateTime(DateUtils.getNowDate());
        psiGoodsCategory.setUpdateBy(SecurityUtils.getUsername());
        return psiGoodsCategoryMapper.updatePsiGoodsCategory(psiGoodsCategory);
    }

    /**
     * 批量删除产品物料分类
     * 
     * @param ids 需要删除的产品物料分类主键
     * @return 结果
     */
    @Override
    public int deletePsiGoodsCategoryByIds(Long[] ids)
    {
        return psiGoodsCategoryMapper.deletePsiGoodsCategoryByIds(ids);
    }

    /**
     * 删除产品物料分类信息
     * 
     * @param id 产品物料分类主键
     * @return 结果
     */
    @Override
    public int deletePsiGoodsCategoryById(Long id)
    {
        return psiGoodsCategoryMapper.deletePsiGoodsCategoryById(id);
    }

    /**
     * 查重
     */
    @Override
    public boolean isDuplicate(PsiGoodsCategory entity) {
        List<PsiGoodsCategory> lst = psiGoodsCategoryMapper.findOneByCondition(entity);
        return lst.size()>0;
    }
}
