import request from '@/utils/request'

// 查询实时库存列表
export function listInventory(query) {
  return request({
    url: '/inventorymgt/inventory/list',
    method: 'get',
    params: query
  })
}

// 查询实时库存详细
export function getInventory(id) {
  return request({
    url: '/inventorymgt/inventory/' + id,
    method: 'get'
  })
}

// 新增实时库存
export function addInventory(data) {
  return request({
    url: '/inventorymgt/inventory',
    method: 'post',
    data: data
  })
}

// 修改实时库存
export function updateInventory(data) {
  return request({
    url: '/inventorymgt/inventory',
    method: 'put',
    data: data
  })
}

// 删除实时库存
export function delInventory(id) {
  return request({
    url: '/inventorymgt/inventory/' + id,
    method: 'delete'
  })
}
