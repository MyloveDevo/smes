package com.ruoyi.basic.service.impl;

import java.util.List;

import com.ruoyi.basic.domain.PsiGoodsCategory;
import com.ruoyi.common.utils.DateUtils;
import com.ruoyi.common.utils.SecurityUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.ruoyi.basic.mapper.PsiGoodsMapper;
import com.ruoyi.basic.domain.PsiGoods;
import com.ruoyi.basic.service.IPsiGoodsService;

/**
 * 产品物料Service业务层处理
 * 
 * @author smes
 * @date 2023-06-05
 */
@Service
public class PsiGoodsServiceImpl implements IPsiGoodsService 
{
    @Autowired
    private PsiGoodsMapper psiGoodsMapper;

    /**
     * 查询产品物料
     * 
     * @param id 产品物料主键
     * @return 产品物料
     */
    @Override
    public PsiGoods selectPsiGoodsById(Long id)
    {
        return psiGoodsMapper.selectPsiGoodsById(id);
    }

    /**
     * 查询产品物料列表
     * 
     * @param psiGoods 产品物料
     * @return 产品物料
     */
    @Override
    public List<PsiGoods> selectPsiGoodsList(PsiGoods psiGoods)
    {
        return psiGoodsMapper.selectPsiGoodsList(psiGoods);
    }

    /**
     * 新增产品物料
     * 
     * @param psiGoods 产品物料
     * @return 结果
     */
    @Override
    public int insertPsiGoods(PsiGoods psiGoods)
    {
        psiGoods.setCreateBy(SecurityUtils.getUsername());
        psiGoods.setCreateTime(DateUtils.getNowDate());
        psiGoods.setDelFlag("0");
        return psiGoodsMapper.insertPsiGoods(psiGoods);
    }

    /**
     * 修改产品物料
     * 
     * @param psiGoods 产品物料
     * @return 结果
     */
    @Override
    public int updatePsiGoods(PsiGoods psiGoods)
    {
        psiGoods.setUpdateBy(SecurityUtils.getUsername());
        psiGoods.setUpdateTime(DateUtils.getNowDate());
        return psiGoodsMapper.updatePsiGoods(psiGoods);
    }

    /**
     * 批量删除产品物料
     * 
     * @param ids 需要删除的产品物料主键
     * @return 结果
     */
    @Override
    public int deletePsiGoodsByIds(Long[] ids)
    {
        return psiGoodsMapper.deletePsiGoodsByIds(ids);
    }

    /**
     * 删除产品物料信息
     * 
     * @param id 产品物料主键
     * @return 结果
     */
    @Override
    public int deletePsiGoodsById(Long id)
    {
        return psiGoodsMapper.deletePsiGoodsById(id);
    }

    /**
     * 查重
     */
    @Override
    public boolean isDuplicate(PsiGoods entity) {
        List<PsiGoods> lst = psiGoodsMapper.findOneByCondition(entity);
        return lst.size()>0;
    }
}
