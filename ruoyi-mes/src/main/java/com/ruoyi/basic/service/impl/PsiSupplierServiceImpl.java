package com.ruoyi.basic.service.impl;

import java.util.List;

import com.ruoyi.basic.domain.PsiSupplierCategory;
import com.ruoyi.basic.domain.PsiWarehouse;
import com.ruoyi.common.utils.DateUtils;
import com.ruoyi.common.utils.SecurityUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.ruoyi.basic.mapper.PsiSupplierMapper;
import com.ruoyi.basic.domain.PsiSupplier;
import com.ruoyi.basic.service.IPsiSupplierService;

/**
 * 供应商Service业务层处理
 * 
 * @author smes
 * @date 2023-06-06
 */
@Service
public class PsiSupplierServiceImpl implements IPsiSupplierService 
{
    @Autowired
    private PsiSupplierMapper psiSupplierMapper;

    /**
     * 查询供应商
     * 
     * @param id 供应商主键
     * @return 供应商
     */
    @Override
    public PsiSupplier selectPsiSupplierById(Long id)
    {
        return psiSupplierMapper.selectPsiSupplierById(id);
    }

    /**
     * 查询供应商列表
     * 
     * @param psiSupplier 供应商
     * @return 供应商
     */
    @Override
    public List<PsiSupplier> selectPsiSupplierList(PsiSupplier psiSupplier)
    {
        return psiSupplierMapper.selectPsiSupplierList(psiSupplier);
    }

    /**
     * 新增供应商
     * 
     * @param psiSupplier 供应商
     * @return 结果
     */
    @Override
    public int insertPsiSupplier(PsiSupplier psiSupplier)
    {
        psiSupplier.setCreateBy(SecurityUtils.getUsername());
        psiSupplier.setCreateTime(DateUtils.getNowDate());
        psiSupplier.setDelFlag("0");
        return psiSupplierMapper.insertPsiSupplier(psiSupplier);
    }

    /**
     * 修改供应商
     * 
     * @param psiSupplier 供应商
     * @return 结果
     */
    @Override
    public int updatePsiSupplier(PsiSupplier psiSupplier)
    {
        psiSupplier.setUpdateBy(SecurityUtils.getUsername());
        psiSupplier.setUpdateTime(DateUtils.getNowDate());
        return psiSupplierMapper.updatePsiSupplier(psiSupplier);
    }

    /**
     * 批量删除供应商
     * 
     * @param ids 需要删除的供应商主键
     * @return 结果
     */
    @Override
    public int deletePsiSupplierByIds(Long[] ids)
    {
        return psiSupplierMapper.deletePsiSupplierByIds(ids);
    }

    /**
     * 删除供应商信息
     * 
     * @param id 供应商主键
     * @return 结果
     */
    @Override
    public int deletePsiSupplierById(Long id)
    {
        return psiSupplierMapper.deletePsiSupplierById(id);
    }

    /**
     * 查重
     */
    @Override
    public boolean isDuplicate(PsiSupplier entity) {
        List<PsiSupplier> lst = psiSupplierMapper.findOneByCondition(entity);
        return lst.size()>0;
    }
}
