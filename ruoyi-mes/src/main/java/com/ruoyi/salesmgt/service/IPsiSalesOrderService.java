package com.ruoyi.salesmgt.service;

import java.util.List;
import com.ruoyi.salesmgt.domain.PsiSalesOrder;

/**
 * 销售订单Service接口
 * 
 * @author smes
 * @date 2023-06-12
 */
public interface IPsiSalesOrderService 
{
    /**
     * 查询销售订单
     * 
     * @param salesOrderId 销售订单主键
     * @return 销售订单
     */
    public PsiSalesOrder selectPsiSalesOrderBySalesOrderId(Long salesOrderId);

    /**
     * 查询销售订单列表
     * 
     * @param psiSalesOrder 销售订单
     * @return 销售订单集合
     */
    public List<PsiSalesOrder> selectPsiSalesOrderList(PsiSalesOrder psiSalesOrder);

    /**
     * 新增销售订单
     * 
     * @param psiSalesOrder 销售订单
     * @return 结果
     */
    public int insertPsiSalesOrder(PsiSalesOrder psiSalesOrder);

    /**
     * 修改销售订单
     * 
     * @param psiSalesOrder 销售订单
     * @return 结果
     */
    public int updatePsiSalesOrder(PsiSalesOrder psiSalesOrder);

    /**
     * 批量删除销售订单
     * 
     * @param salesOrderIds 需要删除的销售订单主键集合
     * @return 结果
     */
    public int deletePsiSalesOrderBySalesOrderIds(Long[] salesOrderIds);

    /**
     * 删除销售订单信息
     * 
     * @param salesOrderId 销售订单主键
     * @return 结果
     */
    public int deletePsiSalesOrderBySalesOrderId(Long salesOrderId);
}
