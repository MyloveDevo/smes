import request from '@/utils/request'


// 查询项目管理列表
export function getNoPageList(id) {
  return request({
    url: '/prdmgt/bom/noPgList/'+id,
    method: 'get',
  })
}


// 查询项目BOM列表
export function listBom(query) {
  return request({
    url: '/prdmgt/bom/list',
    method: 'get',
    params: query
  })
}

// 查询项目BOM详细
export function getProjectBom(id) {
  return request({
    url: '/prdmgt/bom/' + id,
    method: 'get'
  })
}

// 新增项目BOM
export function addProjectBom(data) {
  return request({
    url: '/prdmgt/bom',
    method: 'post',
    data: data
  })
}

// 修改项目BOM
export function updateProjectBom(data) {
  return request({
    url: '/prdmgt/bom',
    method: 'put',
    data: data
  })
}

// 删除项目BOM
export function delProjectBom(id) {
  return request({
    url: '/prdmgt/bom/' + id,
    method: 'delete'
  })
}
