import request from '@/utils/request'

// 查询销售合同列表
export function listContract(query) {
  return request({
    url: '/prdmgt/contract/list',
    method: 'get',
    params: query
  })
}

// 查询销售合同详细
export function getContract(salesContractId) {
  return request({
    url: '/prdmgt/contract/' + salesContractId,
    method: 'get'
  })
}

// 新增销售合同
export function addContract(data) {
  return request({
    url: '/prdmgt/contract',
    method: 'post',
    data: data
  })
}

// 修改销售合同
export function updateContract(data) {
  return request({
    url: '/prdmgt/contract',
    method: 'put',
    data: data
  })
}

// 删除销售合同
export function delContract(salesContractId) {
  return request({
    url: '/prdmgt/contract/' + salesContractId,
    method: 'delete'
  })
}
