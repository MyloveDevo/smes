package com.ruoyi.prdmgt.service.impl;

import java.util.List;
import com.ruoyi.common.utils.DateUtils;
import com.ruoyi.common.utils.SecurityUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.ruoyi.prdmgt.mapper.PsiProjectBomMapper;
import com.ruoyi.prdmgt.domain.PsiProjectBom;
import com.ruoyi.prdmgt.service.IPsiProjectBomService;

/**
 * 项目BOMService业务层处理
 * 
 * @author smes
 * @date 2023-06-12
 */
@Service
public class PsiProjectBomServiceImpl implements IPsiProjectBomService 
{
    @Autowired
    private PsiProjectBomMapper psiProjectBomMapper;

    /**
     * 查询项目BOM
     * 
     * @param id 项目BOM主键
     * @return 项目BOM
     */
    @Override
    public PsiProjectBom selectPsiProjectBomById(Long id)
    {
        return psiProjectBomMapper.selectPsiProjectBomById(id);
    }

    /**
     * 查询项目BOM列表
     * 
     * @param psiProjectBom 项目BOM
     * @return 项目BOM
     */
    @Override
    public List<PsiProjectBom> selectPsiProjectBomList(PsiProjectBom psiProjectBom)
    {
        return psiProjectBomMapper.selectPsiProjectBomList(psiProjectBom);
    }

    /**
     * 新增项目BOM
     * 
     * @param psiProjectBom 项目BOM
     * @return 结果
     */
    @Override
    public int insertPsiProjectBom(PsiProjectBom psiProjectBom)
    {
        psiProjectBom.setCreateTime(DateUtils.getNowDate());
        return psiProjectBomMapper.insertPsiProjectBom(psiProjectBom);
    }

    /**
     * 修改项目BOM
     * 
     * @param psiProjectBom 项目BOM
     * @return 结果
     */
    @Override
    public int updatePsiProjectBom(PsiProjectBom psiProjectBom)
    {
        psiProjectBom.setUpdateTime(DateUtils.getNowDate());
        return psiProjectBomMapper.updatePsiProjectBom(psiProjectBom);
    }

    /**
     * 批量删除项目BOM
     * 
     * @param ids 需要删除的项目BOM主键
     * @return 结果
     */
    @Override
    public int deletePsiProjectBomByIds(Long[] ids)
    {
        return psiProjectBomMapper.deletePsiProjectBomByIds(ids);
    }

    /**
     * 删除项目BOM信息
     * 
     * @param id 项目BOM主键
     * @return 结果
     */
    @Override
    public int deletePsiProjectBomById(Long id)
    {
        return psiProjectBomMapper.deletePsiProjectBomById(id);
    }

    /**
     * 新增项目BOM
     *
     * @param psiProjectBom 项目BOM
     * @return 结果 包含PK
     */
    @Override
    public PsiProjectBom addPsiProjectBom(PsiProjectBom psiProjectBom)
    {
        psiProjectBom.setDelFlag("0");
        psiProjectBom.setCreateTime(DateUtils.getNowDate());
        psiProjectBom.setCreateBy(SecurityUtils.getUsername());
        psiProjectBom.setCreateId(SecurityUtils.getUserId());
        psiProjectBomMapper.insertPsiProjectBom(psiProjectBom);
        return psiProjectBom;
    }
}
