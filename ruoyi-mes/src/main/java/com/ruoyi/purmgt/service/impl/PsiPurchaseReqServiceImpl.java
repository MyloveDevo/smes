package com.ruoyi.purmgt.service.impl;

import java.util.List;
import com.ruoyi.common.utils.DateUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import java.util.ArrayList;
import com.ruoyi.common.utils.StringUtils;
import org.springframework.transaction.annotation.Transactional;
import com.ruoyi.purmgt.domain.PsiPurchaseReqDetail;
import com.ruoyi.purmgt.mapper.PsiPurchaseReqMapper;
import com.ruoyi.purmgt.domain.PsiPurchaseReq;
import com.ruoyi.purmgt.service.IPsiPurchaseReqService;

/**
 * 请购单 Service业务层处理
 * 
 * @author smes
 * @date 2023-06-19
 */
@Service
public class PsiPurchaseReqServiceImpl implements IPsiPurchaseReqService 
{
    @Autowired
    private PsiPurchaseReqMapper psiPurchaseReqMapper;

    /**
     * 查询请购单 
     * 
     * @param id 请购单 主键
     * @return 请购单 
     */
    @Override
    public PsiPurchaseReq selectPsiPurchaseReqById(Long id)
    {
        return psiPurchaseReqMapper.selectPsiPurchaseReqById(id);
    }

    /**
     * 查询请购单 列表
     * 
     * @param psiPurchaseReq 请购单 
     * @return 请购单 
     */
    @Override
    public List<PsiPurchaseReq> selectPsiPurchaseReqList(PsiPurchaseReq psiPurchaseReq)
    {
        return psiPurchaseReqMapper.selectPsiPurchaseReqList(psiPurchaseReq);
    }

    /**
     * 新增请购单 
     * 
     * @param psiPurchaseReq 请购单 
     * @return 结果
     */
    @Transactional
    @Override
    public int insertPsiPurchaseReq(PsiPurchaseReq psiPurchaseReq)
    {
        psiPurchaseReq.setCreateTime(DateUtils.getNowDate());
        int rows = psiPurchaseReqMapper.insertPsiPurchaseReq(psiPurchaseReq);
        insertPsiPurchaseReqDetail(psiPurchaseReq);
        return rows;
    }

    /**
     * 修改请购单 
     * 
     * @param psiPurchaseReq 请购单 
     * @return 结果
     */
    @Transactional
    @Override
    public int updatePsiPurchaseReq(PsiPurchaseReq psiPurchaseReq)
    {
        psiPurchaseReq.setUpdateTime(DateUtils.getNowDate());
        psiPurchaseReqMapper.deletePsiPurchaseReqDetailByPrId(psiPurchaseReq.getId());
        insertPsiPurchaseReqDetail(psiPurchaseReq);
        return psiPurchaseReqMapper.updatePsiPurchaseReq(psiPurchaseReq);
    }

    /**
     * 批量删除请购单 
     * 
     * @param ids 需要删除的请购单 主键
     * @return 结果
     */
    @Transactional
    @Override
    public int deletePsiPurchaseReqByIds(Long[] ids)
    {
        psiPurchaseReqMapper.deletePsiPurchaseReqDetailByPrIds(ids);
        return psiPurchaseReqMapper.deletePsiPurchaseReqByIds(ids);
    }

    /**
     * 删除请购单 信息
     * 
     * @param id 请购单 主键
     * @return 结果
     */
    @Transactional
    @Override
    public int deletePsiPurchaseReqById(Long id)
    {
        psiPurchaseReqMapper.deletePsiPurchaseReqDetailByPrId(id);
        return psiPurchaseReqMapper.deletePsiPurchaseReqById(id);
    }

    /**
     * 新增请购单明细 信息
     * 
     * @param psiPurchaseReq 请购单 对象
     */
    public void insertPsiPurchaseReqDetail(PsiPurchaseReq psiPurchaseReq)
    {
        List<PsiPurchaseReqDetail> psiPurchaseReqDetailList = psiPurchaseReq.getPsiPurchaseReqDetailList();
        Long id = psiPurchaseReq.getId();
        if (StringUtils.isNotNull(psiPurchaseReqDetailList))
        {
            List<PsiPurchaseReqDetail> list = new ArrayList<PsiPurchaseReqDetail>();
            for (PsiPurchaseReqDetail psiPurchaseReqDetail : psiPurchaseReqDetailList)
            {
                psiPurchaseReqDetail.setPrId(id);
                list.add(psiPurchaseReqDetail);
            }
            if (list.size() > 0)
            {
                psiPurchaseReqMapper.batchPsiPurchaseReqDetail(list);
            }
        }
    }
}
