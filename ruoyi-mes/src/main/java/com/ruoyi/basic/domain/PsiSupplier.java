package com.ruoyi.basic.domain;

import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import com.ruoyi.common.annotation.Excel;
import com.ruoyi.common.core.domain.BaseEntity;

import javax.validation.constraints.Size;

/**
 * 供应商对象 psi_supplier
 * 
 * @author smes
 * @date 2023-06-06
 */
public class PsiSupplier extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /** PK */
    private Long id;

    /** 名称 */
    @Excel(name = "名称")
    @Size(min = 0, max = 10, message = "名称长度不能超过10个字符")
    private String name;

    /** 编号 */
    @Excel(name = "编号")
    @Size(min = 0, max = 10, message = "编号长度不能超过10个字符")
    private String code;

    /** 助记码 */
    @Excel(name = "助记码")
    @Size(min = 0, max = 10, message = "助记码长度不能超过10个字符")
    private String easyCode;

    /** 类型 */
    @Excel(name = "类型")
    private String supplierCategory;

    /** 联系人 */
    @Excel(name = "联系人")
    @Size(min = 0, max = 10, message = "联系人长度不能超过10个字符")
    private String contact;

    /** 电话 */
    @Excel(name = "电话")
    @Size(min = 0, max = 16, message = "电话长度不能超过16个字符")
    private String phone;

    /** 手机 */
    @Excel(name = "手机")
    @Size(min = 0, max = 16, message = "手机长度不能超过16个字符")
    private String mobile;

    /** 地址 */
    @Excel(name = "地址")
    @Size(min = 0, max = 60, message = "地址长度不能超过60个字符")
    private String address;

    /** 区域 */
    @Excel(name = "区域")
    private String area;

    /** E-mail */

    @Excel(name = "E-mail")
    @Size(min = 0, max = 20, message = "E-mail长度不能超过20个字符")
    private String email;

    /** 网址 */
    @Excel(name = "网址")
    @Size(min = 0, max = 60, message = "网址长度不能超过60个字符")
    private String website;

    /** 传真 */
    @Excel(name = "传真")
    @Size(min = 0, max = 10, message = "传真长度不能超过10个字符")
    private String fax;

    /** 开户行 */
    @Excel(name = "开户行")
    @Size(min = 0, max = 60, message = "开户行长度不能超过60个字符")
    private String openingBank;

    /** 开户名 */
    @Excel(name = "开户名")
    @Size(min = 0, max = 30, message = "开户名长度不能超过30个字符")
    private String accountName;

    /** 税号 */
    @Excel(name = "税号")
    @Size(min = 0, max = 10, message = "税号长度不能超过10个字符")
    private String taxIdNumber;

    /** 银行账号 */
    @Excel(name = "银行账号")
    @Size(min = 0, max = 16, message = "银行账号长度不能超过16个字符")
    private String bankAccount;

    /** 审核状态 */
    @Excel(name = "审核状态")
    private String auditStatus;

    /** 删除标志 */
    private String delFlag;

    public void setId(Long id) 
    {
        this.id = id;
    }

    public Long getId() 
    {
        return id;
    }
    public void setName(String name) 
    {
        this.name = name;
    }

    public String getName() 
    {
        return name;
    }
    public void setCode(String code) 
    {
        this.code = code;
    }

    public String getCode() 
    {
        return code;
    }
    public void setEasyCode(String easyCode) 
    {
        this.easyCode = easyCode;
    }

    public String getEasyCode() 
    {
        return easyCode;
    }
    public void setSupplierCategory(String supplierCategory) 
    {
        this.supplierCategory = supplierCategory;
    }

    public String getSupplierCategory() 
    {
        return supplierCategory;
    }
    public void setContact(String contact) 
    {
        this.contact = contact;
    }

    public String getContact() 
    {
        return contact;
    }
    public void setPhone(String phone) 
    {
        this.phone = phone;
    }

    public String getPhone() 
    {
        return phone;
    }
    public void setMobile(String mobile) 
    {
        this.mobile = mobile;
    }

    public String getMobile() 
    {
        return mobile;
    }
    public void setAddress(String address) 
    {
        this.address = address;
    }

    public String getAddress() 
    {
        return address;
    }
    public void setArea(String area) 
    {
        this.area = area;
    }

    public String getArea() 
    {
        return area;
    }
    public void setEmail(String email) 
    {
        this.email = email;
    }

    public String getEmail() 
    {
        return email;
    }
    public void setWebsite(String website) 
    {
        this.website = website;
    }

    public String getWebsite() 
    {
        return website;
    }
    public void setFax(String fax) 
    {
        this.fax = fax;
    }

    public String getFax() 
    {
        return fax;
    }
    public void setOpeningBank(String openingBank) 
    {
        this.openingBank = openingBank;
    }

    public String getOpeningBank() 
    {
        return openingBank;
    }
    public void setAccountName(String accountName) 
    {
        this.accountName = accountName;
    }

    public String getAccountName() 
    {
        return accountName;
    }
    public void setTaxIdNumber(String taxIdNumber) 
    {
        this.taxIdNumber = taxIdNumber;
    }

    public String getTaxIdNumber() 
    {
        return taxIdNumber;
    }
    public void setBankAccount(String bankAccount) 
    {
        this.bankAccount = bankAccount;
    }

    public String getBankAccount() 
    {
        return bankAccount;
    }
    public void setAuditStatus(String auditStatus) 
    {
        this.auditStatus = auditStatus;
    }

    public String getAuditStatus() 
    {
        return auditStatus;
    }
    public void setDelFlag(String delFlag) 
    {
        this.delFlag = delFlag;
    }

    public String getDelFlag() 
    {
        return delFlag;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this,ToStringStyle.MULTI_LINE_STYLE)
            .append("id", getId())
            .append("name", getName())
            .append("code", getCode())
            .append("easyCode", getEasyCode())
            .append("supplierCategory", getSupplierCategory())
            .append("contact", getContact())
            .append("phone", getPhone())
            .append("mobile", getMobile())
            .append("address", getAddress())
            .append("area", getArea())
            .append("email", getEmail())
            .append("website", getWebsite())
            .append("fax", getFax())
            .append("openingBank", getOpeningBank())
            .append("accountName", getAccountName())
            .append("taxIdNumber", getTaxIdNumber())
            .append("bankAccount", getBankAccount())
            .append("auditStatus", getAuditStatus())
            .append("remark", getRemark())
            .append("delFlag", getDelFlag())
            .append("createBy", getCreateBy())
            .append("createTime", getCreateTime())
            .append("updateBy", getUpdateBy())
            .append("updateTime", getUpdateTime())
            .toString();
    }
}
