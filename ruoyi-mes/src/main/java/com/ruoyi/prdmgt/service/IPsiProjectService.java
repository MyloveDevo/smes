package com.ruoyi.prdmgt.service;

import java.util.List;
import com.ruoyi.prdmgt.domain.PsiProject;

/**
 * 项目管理Service接口
 * 
 * @author smes
 * @date 2023-06-11
 */
public interface IPsiProjectService 
{
    /**
     * 查询项目管理
     * 
     * @param id 项目管理主键
     * @return 项目管理
     */
    public PsiProject selectPsiProjectById(String id);

    /**
     * 查询项目管理列表
     * 
     * @param psiProject 项目管理
     * @return 项目管理集合
     */
    public List<PsiProject> selectPsiProjectList(PsiProject psiProject);

    /**
     * 新增项目管理
     * 
     * @param psiProject 项目管理
     * @return 结果
     */
    public PsiProject insertPsiProject(PsiProject psiProject);

    /**
     * 修改项目管理
     * 
     * @param psiProject 项目管理
     * @return 结果
     */
    public int updatePsiProject(PsiProject psiProject);

    /**
     * 批量删除项目管理
     * 
     * @param ids 需要删除的项目管理主键集合
     * @return 结果
     */
    public int deletePsiProjectByIds(String[] ids);

    /**
     * 删除项目管理信息
     * 
     * @param id 项目管理主键
     * @return 结果
     */
    public int deletePsiProjectById(String id);

}
