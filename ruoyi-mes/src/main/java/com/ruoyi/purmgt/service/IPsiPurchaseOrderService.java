package com.ruoyi.purmgt.service;

import java.util.List;
import com.ruoyi.purmgt.domain.PsiPurchaseOrder;

/**
 * 采购订单Service接口
 * 
 * @author smes
 * @date 2023-06-17
 */
public interface IPsiPurchaseOrderService 
{
    /**
     * 查询采购订单
     * 
     * @param purchaseOrderId 采购订单主键
     * @return 采购订单
     */
    public PsiPurchaseOrder selectPsiPurchaseOrderByPurchaseOrderId(Long purchaseOrderId);

    /**
     * 查询采购订单列表
     * 
     * @param psiPurchaseOrder 采购订单
     * @return 采购订单集合
     */
    public List<PsiPurchaseOrder> selectPsiPurchaseOrderList(PsiPurchaseOrder psiPurchaseOrder);

    /**
     * 新增采购订单
     * 
     * @param psiPurchaseOrder 采购订单
     * @return 结果
     */
    public int insertPsiPurchaseOrder(PsiPurchaseOrder psiPurchaseOrder);

    /**
     * 修改采购订单
     * 
     * @param psiPurchaseOrder 采购订单
     * @return 结果
     */
    public int updatePsiPurchaseOrder(PsiPurchaseOrder psiPurchaseOrder);

    /**
     * 批量删除采购订单
     * 
     * @param purchaseOrderIds 需要删除的采购订单主键集合
     * @return 结果
     */
    public int deletePsiPurchaseOrderByPurchaseOrderIds(Long[] purchaseOrderIds);

    /**
     * 删除采购订单信息
     * 
     * @param purchaseOrderId 采购订单主键
     * @return 结果
     */
    public int deletePsiPurchaseOrderByPurchaseOrderId(Long purchaseOrderId);
}
